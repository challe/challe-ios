//
//  AppDelegate.swift
//  Challe
//
//  Created by Rob Dearborn on 2/25/19.
//  Copyright © 2019 Challe. All rights reserved.
//

import UIKit
import UserNotifications

import Auth0
import Firebase
import KeychainSwift
import Lock
import Presentr
import Reachability
import StitchCore

let auth0CredentialsManager = CredentialsManager(authentication: Auth0.authentication())
let keychain = KeychainSwift()
let reachability = Reachability()!
let stitchClient = try! Stitch.initializeAppClient(withClientAppID: C.stitchAppId)


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    private var authPingTimer = Timer()
    private var lambdaPingTimer = Timer()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        // firebase
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        
        // Initialize feed filter settings
        if keychain.get(KeychainKeys.challengesFeedCreatorFilt.rawValue) == nil {
            keychain.set(C.challengesFeedCreatorFiltDefault, forKey: KeychainKeys.challengesFeedCreatorFilt.rawValue)
        }
        if keychain.get(KeychainKeys.challengesFeedSortBy.rawValue) == nil {
            keychain.set(C.challengesFeedSortByDefault, forKey: KeychainKeys.challengesFeedSortBy.rawValue)
        }
        if keychain.get(KeychainKeys.challengesFeedStartingFilt.rawValue) == nil {
            keychain.set(C.challengesFeedStartingFiltDefault, forKey: KeychainKeys.challengesFeedStartingFilt.rawValue)
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        dateFormatter.timeZone = TimeZone.init(secondsFromGMT: 0)
        if keychain.get(KeychainKeys.challengesFeedCustomMinStartDateStr.rawValue) == nil {
            keychain.set(dateFormatter.string(from: Date()), forKey: KeychainKeys.challengesFeedCustomMinStartDateStr.rawValue)
        }
        if keychain.get(KeychainKeys.challengesFeedCustomMaxStartDateStr.rawValue) == nil {
            keychain.set(dateFormatter.string(from: Date()), forKey: KeychainKeys.challengesFeedCustomMaxStartDateStr.rawValue)
        }
        
        // Determine login flow
        if keychain.get(KeychainKeys.auth0InitialScreen.rawValue) == nil {
            keychain.set("signup", forKey: KeychainKeys.auth0InitialScreen.rawValue)
        }
        
        var toReturn = false
        if keychain.getBool(KeychainKeys.nuxComplete.rawValue) == nil {
            let storyboard = UIStoryboard(name: "NUX", bundle: nil)
            if let initialVC = storyboard.instantiateInitialViewController() {
                self.window?.rootViewController = initialVC
                toReturn = false
            }
        } else if userNeedsLogin() {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            if let initialVC = storyboard.instantiateViewController(withIdentifier: "authViewController") as? AuthViewController {
                self.window?.rootViewController = initialVC
                toReturn = false
            }
        } else {
            
            registerForPushNotifications()
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            
            // Check if launched from notification
            let notificationOption = launchOptions?[.remoteNotification]
            let notification = notificationOption as? [String: AnyObject]
            let stitchData = jsonToDictionary(text: notification?["stitch.data"] as? String)
            
            if let initialViewIdentifier = notification?["initial_view_identifier"] as? String ?? stitchData?["initial_view_identifier"] as? String {
                
                Analytics.logEvent("open_notification", parameters: ["initial_view_identifier": initialViewIdentifier])
                
                if initialViewIdentifier == "challengesFeedViewController", let initialVC = storyboard.instantiateViewController(withIdentifier: initialViewIdentifier) as? ChallengesFeedViewController {
                    self.window?.rootViewController = initialVC
                    toReturn = true
                } else if initialViewIdentifier == "challengeViewController", let initialVC = storyboard.instantiateViewController(withIdentifier: initialViewIdentifier) as? ChallengeViewController, let rootVC = storyboard.instantiateViewController(withIdentifier: "challengesFeedViewController") as? ChallengesFeedViewController {
                    
                    self.window?.rootViewController = rootVC
                    
                    if let challengeId = notification?["challenge_id"] as? String ?? stitchData?["challenge_id"] as? String {
                        
                        let presenter = Presentr(presentationType: .fullScreen)
                        
                        initialVC.challengeId = ObjectId(challengeId)!

                        rootVC.customPresentViewController(presenter, viewController: initialVC, animated: true)
                    }
                    
                    toReturn = true
                    
                } else if initialViewIdentifier == "otherProfileViewController", let initialVC = storyboard.instantiateViewController(withIdentifier: initialViewIdentifier) as? OtherProfileViewController, let rootVC = storyboard.instantiateViewController(withIdentifier: "challengesFeedViewController") as? ChallengesFeedViewController {
                    
                    self.window?.rootViewController = rootVC
                    
                    if let challeUserId = notification?["challe_user_id"] as? String ?? stitchData?["challe_user_id"] as? String {
                        
                        let presenter = Presentr(presentationType: .fullScreen)
                        
                        initialVC.challeUserId = challeUserId
                        
                        rootVC.customPresentViewController(presenter, viewController: initialVC, animated: true)
                    }
                    
                    toReturn = true
                    
                } else if initialViewIdentifier == "ownProfileViewController", let initialVC = storyboard.instantiateViewController(withIdentifier: initialViewIdentifier) as? OwnProfileViewController {
                    self.window?.rootViewController = initialVC
                    toReturn = true
                }
                
            } else {
                if let initialVC = storyboard.instantiateInitialViewController() {
                    self.window?.rootViewController = initialVC
                    toReturn = true
                }
            }
        }
        
        // Schedule pings
        reachability.whenReachable = { reachability in
            print("network reachable")
            
            // Auth0 credentials checking
            self.scheduleAuthPings()
            
            // Lambda keepalive pings
            self.lambdaPings()
            self.scheduleLambdaPings()
        }
        
        reachability.whenUnreachable = { _ in
            print("network unreachable")
            self.authPingTimer.invalidate()
            self.lambdaPingTimer.invalidate()
        }
        
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
        
        // fire lambda pings again right after signup / login
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.lambdaPings),
            name: .fireLambdaPings,
            object: nil
        )
        
        // Handle notifications while running
        UNUserNotificationCenter.current().delegate = self
        
        return toReturn
    }
    
    // handle deep links
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        
        if userNeedsLogin() {
            return false
        }
        
        if userActivity.activityType == NSUserActivityTypeBrowsingWeb, let url = userActivity.webpageURL {
            print("Deep link URL: \(url)")
            
            let viewType = url.pathComponents[1]
            let viewId = url.pathComponents[2]
            
            guard viewId.isMongoId() else {
                print("Deep link error: invalid ID")
                return false
            }
            
            guard let topVC = self.window?.rootViewController?.topMostViewController() else {
                print("Deep link error: couldn't form topVC")
                return false
            }
            guard let storyboard = topVC.storyboard else {
                print("Deep link error: couldn't form storyboard")
                return false
            }
            
            let presenter = Presentr(presentationType: .fullScreen)
            
            if viewType == "challenge" {
                if let newVC = storyboard.instantiateViewController(withIdentifier: "challengeViewController") as? ChallengeViewController {
                    Analytics.logEvent("open_deep_link", parameters: ["type": "challenge", "url": "\(url)"])
                    newVC.challengeId = ObjectId(viewId)!
                    topVC.customPresentViewController(presenter, viewController: newVC, animated: true)
                    return true
                }
            } else if viewType == "profile" {
                if let newVC = storyboard.instantiateViewController(withIdentifier: "otherProfileViewController") as? OtherProfileViewController {
                    Analytics.logEvent("open_deep_link", parameters: ["type": "profile", "url": "\(url)"])
                    newVC.challeUserId = viewId
                    topVC.customPresentViewController(presenter, viewController: newVC, animated: true)
                    return true
                }
            }
        }
            
        return false
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        print("resigning active...")
        Analytics.logEvent("app_backgrounded", parameters: nil)
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        print("entering background...")
        reachability.stopNotifier()
        authPingTimer.invalidate()
        lambdaPingTimer.invalidate()
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        print("entering foreground...")
        
        // Auth0 credentials checking
        self.scheduleAuthPings()
        
        // Lambda keepalive pings
        self.lambdaPings()
        self.scheduleLambdaPings()
        
        // Restart reachability
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        print("becoming active...")
        Analytics.logEvent("app_foregrounded", parameters: nil)
        if UIApplication.shared.applicationIconBadgeNumber > 0 {
            UIApplication.shared.applicationIconBadgeNumber = 0
        }
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        print("terminating...")
        Analytics.logEvent("app_terminated", parameters: nil)
    }
    
    // MARK: Auth0 config
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
        return Lock.resumeAuth(url, options: options)
    }
    
    // MARK: Pings config
    @objc func authPings() {
        guard let topVC = self.window?.rootViewController?.topMostViewController() else {
            print("coudln't form topVC; skipping check")
            return
        }
        
        auth0CredentialsManager.credentials { error, credentials in
            DispatchQueue.main.async {
                if error != nil {
                    print("Auth pings error: \(error)")
                }
                guard !userNeedsLogin(), error == nil, let credentials = credentials else {
                    print("new auth0 creds needed")
                    // don't trigger login if nux or login in progress, or on VerifyAccountViewController
                    if keychain.getBool(KeychainKeys.nuxComplete.rawValue) == nil {
                        print("skipping logout flow due to NUX")
                        return
                    } else if let topVC = topVC as? AuthViewController {
                        print("skipping logout flow due to AuthViewController")
                        return
                    } else if let topVC = topVC as? LockViewController {
                        print("skipping logout flow due to LockViewController")
                        return
                    } else if let topVC = topVC as? VerifyAccountViewController {
                        print("skipping logout flow due to VerifyAccountViewController")
                        return
                    } else {
                        logoutFlow(presentFrom: topVC)
                        return
                    }
                }
            }
        }
    }
    
    func scheduleAuthPings(){
        authPingTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.authPings), userInfo: nil, repeats: true)
    }
    
    @objc func lambdaPings(){
        print("beginning lambda pings...")
        
        auth0CredentialsManager.credentials { error, credentials in
            
            guard error == nil, let credentials = credentials else {
                print("Lambda pings error: auth0 credentials error. \(String(describing: error))")
                return
                
            }
            
            guard let token = credentials.accessToken else {
                print("Lambda pings error: coudln't form auth0 token.")
                return
            }
            
            guard let currentUserId = stitchClient.auth.currentUser?.id else{
                print("Lambda pings error: coudln't get stitch user id.")
                return
            }
            
            let getChallengesURL = URL(string: "https://0r6psvjhfi.execute-api.us-east-1.amazonaws.com/prod/challenges/get-challenges")!
            var getChallengesRequest = URLRequest(url: getChallengesURL)
            getChallengesRequest.httpMethod = "GET"
            
            getChallengesRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            getChallengesRequest.setValue(currentUserId, forHTTPHeaderField: "current_user_id")
            getChallengesRequest.setValue("true", forHTTPHeaderField: "is_keepalive_ping")
            getChallengesRequest.setValue("x", forHTTPHeaderField: "n_per_page")
            getChallengesRequest.setValue("x", forHTTPHeaderField: "page_number")
            getChallengesRequest.setValue("x", forHTTPHeaderField: "max_created_at")
            getChallengesRequest.setValue("x", forHTTPHeaderField: "min_start_date")
            getChallengesRequest.setValue("x", forHTTPHeaderField: "max_start_date")
            getChallengesRequest.setValue("x", forHTTPHeaderField: "creator")
            getChallengesRequest.setValue("x", forHTTPHeaderField: "sort_by")
            getChallengesRequest.setValue("x", forHTTPHeaderField: "sort_order")
            getChallengesRequest.setValue("x", forHTTPHeaderField: "query_string")
            getChallengesRequest.setValue("x", forHTTPHeaderField: "follower")
            getChallengesRequest.setValue("x", forHTTPHeaderField: "completed")
            
            let getChallengesTask = URLSession.shared.dataTask(with: getChallengesRequest) {(data, response, error) in
                if let data = data {
                    print("get challenges successfully pinged!")
                }
            }
            print("pinging get challenges...")
            getChallengesTask.resume()
            
            let getUsersURL = URL(string: "https://0r6psvjhfi.execute-api.us-east-1.amazonaws.com/prod/users/get-users")!
            var getUsersRequest = URLRequest(url: getUsersURL)
            getUsersRequest.httpMethod = "GET"
            
            getUsersRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            getUsersRequest.setValue(currentUserId, forHTTPHeaderField: "current_user_id")
            getUsersRequest.setValue("true", forHTTPHeaderField: "is_keepalive_ping")
            getUsersRequest.setValue("x", forHTTPHeaderField: "n_per_page")
            getUsersRequest.setValue("x", forHTTPHeaderField: "page_number")
            getUsersRequest.setValue("x", forHTTPHeaderField: "max_created_at")
            getUsersRequest.setValue("x", forHTTPHeaderField: "query_string")
            
            let getUsersTask = URLSession.shared.dataTask(with: getUsersRequest) {(data, response, error) in
                if let data = data {
                    print("get users successfully pinged!")
                }
            }
            print("pinging get users...")
            getUsersTask.resume()
            
            let getConnectedUsersURL = URL(string: "https://0r6psvjhfi.execute-api.us-east-1.amazonaws.com/prod/users/get-connected-users")!
            var getConnectedUsersRequest = URLRequest(url: getConnectedUsersURL)
            getConnectedUsersRequest.httpMethod = "GET"
            
            getConnectedUsersRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            getConnectedUsersRequest.setValue(currentUserId, forHTTPHeaderField: "current_user_id")
            getConnectedUsersRequest.setValue("true", forHTTPHeaderField: "is_keepalive_ping")
            getConnectedUsersRequest.setValue("x", forHTTPHeaderField: "n_per_page")
            getConnectedUsersRequest.setValue("x", forHTTPHeaderField: "page_number")
            getConnectedUsersRequest.setValue("x", forHTTPHeaderField: "max_created_at")
            getConnectedUsersRequest.setValue("x", forHTTPHeaderField: "connection_collection")
            getConnectedUsersRequest.setValue("x", forHTTPHeaderField: "connection_field")
            getConnectedUsersRequest.setValue("x", forHTTPHeaderField: "connection_approval")
            getConnectedUsersRequest.setValue("x", forHTTPHeaderField: "query_string")
            getConnectedUsersRequest.setValue("x", forHTTPHeaderField: "user_field")
            
            let getConnectedUsersTask = URLSession.shared.dataTask(with: getConnectedUsersRequest) {(data, response, error) in
                if let data = data {
                    print("get connected users successfully pinged!")
                }
            }
            print("pinging get connected users...")
            getConnectedUsersTask.resume()
            
            let updateMongo0UserURL = URL(string: "https://0r6psvjhfi.execute-api.us-east-1.amazonaws.com/prod/users/update-mongo-user")!
            var updateMongoUserRequest = URLRequest(url: updateMongo0UserURL)
            updateMongoUserRequest.httpMethod = "PATCH"
            
            updateMongoUserRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            updateMongoUserRequest.setValue(currentUserId, forHTTPHeaderField: "user_id")
            updateMongoUserRequest.setValue("true", forHTTPHeaderField: "is_keepalive_ping")
            
            let updateMongoUserTask = URLSession.shared.dataTask(with: updateMongoUserRequest) {(data, response, error) in
                if let data = data {
                    print("update mongo user successfully pinged!")
                }
            }
            updateMongoUserTask.resume()
            print("pinging update mongo user...")
        }
        
    }
    
    func scheduleLambdaPings(){
        lambdaPingTimer = Timer.scheduledTimer(timeInterval: 300, target: self, selector: #selector(self.lambdaPings), userInfo: nil, repeats: true)
    }

}

// MARK: Push notifications
extension AppDelegate: UNUserNotificationCenterDelegate, MessagingDelegate {
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let tokenParts = deviceToken.map { data in String(format: "%02.2hhx", data) }
        let token = tokenParts.joined()
        print("APN token: \(token)")
        updateAPNToken(newToken: token)
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register for APN: \(error)")
        let token = ""
        updateAPNToken(newToken: token)
    }
    
    // handle notifications while app running
    // display
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .badge, .sound])
    }
    
    // open
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void
        ) {
        
        let stitchData = jsonToDictionary(text: userInfo["stitch.data"] as? String)
        
        guard let initialViewIdentifier = userInfo["initial_view_identifier"] as? String ?? stitchData?["initial_view_identifier"] as? String else {
            completionHandler(.failed)
            return
        }
        
        Analytics.logEvent("open_notification", parameters: ["initial_view_identifier": initialViewIdentifier])
        
        guard let topVC = self.window?.rootViewController?.topMostViewController() else {
            print("Notification opening error: couldn't form topVC")
            completionHandler(.failed)
            return
        }
        
        guard let storyboard = topVC.storyboard else {
            print("Notification opening error: couldn't form storyboard")
            completionHandler(.failed)
            return
        }
        
        let presenter = Presentr(presentationType: .fullScreen)
        
        if initialViewIdentifier == "challengesFeedViewController", let newVC = storyboard.instantiateViewController(withIdentifier: initialViewIdentifier) as? ChallengesFeedViewController {
            topVC.customPresentViewController(presenter, viewController: newVC, animated: true)
        } else if initialViewIdentifier == "challengeViewController", let newVC = storyboard.instantiateViewController(withIdentifier: initialViewIdentifier) as? ChallengeViewController {
            
            guard let challengeId = userInfo["challenge_id"] as? String ?? stitchData?["challenge_id"] as? String else {
                completionHandler(.failed)
                return
            }
            
            newVC.challengeId = ObjectId(challengeId)!
            topVC.customPresentViewController(presenter, viewController: newVC, animated: true)
            
        } else if initialViewIdentifier == "otherProfileViewController", let newVC = storyboard.instantiateViewController(withIdentifier: initialViewIdentifier) as? OtherProfileViewController {
            
            guard let challeUserId = userInfo["challe_user_id"] as? String ?? stitchData?["challe_user_id"] as? String else {
                completionHandler(.failed)
                return
            }
            
            newVC.challeUserId = challeUserId
            topVC.customPresentViewController(presenter, viewController: newVC, animated: true)
            
        } else if initialViewIdentifier == "ownProfileViewController", let newVC = storyboard.instantiateViewController(withIdentifier: initialViewIdentifier) as? OwnProfileViewController {
            topVC.customPresentViewController(presenter, viewController: newVC, animated: true)
        }
        
        completionHandler(.newData)
        return
    }
    
    // FCM token refresh
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        updateFCMToken(newToken: fcmToken)
    }
}

func registerForPushNotifications() {
    UNUserNotificationCenter.current()
        .requestAuthorization(options: [.alert, .sound, .badge]) {
            granted, error in
            print("Push notification granted: \(granted)")
            guard granted else {
                let token = ""
                updateAPNToken(newToken: token)
                return
            }
            getNotificationSettings()
    }
}

func getNotificationSettings() {
    UNUserNotificationCenter.current().getNotificationSettings { settings in
        print("Notification settings: \(settings)")
        guard settings.authorizationStatus == .authorized else { return }
        DispatchQueue.main.async {
            UIApplication.shared.registerForRemoteNotifications()
        }
    }
}

func updateAPNToken(newToken: String){
    // update global var
    V.apnToken = newToken
    
    // update mongo
    if !userNeedsLogin(), let userId = stitchClient.auth.currentUser?.id {
        let tokenDoc: Document = [
            "apn_token": newToken
        ]
        stitchClient.callFunction(withName: "updateUserNotificationSettings", withArgs: [userId, tokenDoc]) {
            (result: StitchResult<String>) in
            switch result {
            case .failure(let error):
                print("Stitch update user notification settings error: \(error)")
            case .success(let user_id):
                print("Successfully updated user notification settings to Stitch: \(user_id)")
            }
        }
    }
}

func updateFCMToken(newToken: String){
    // update global var
    V.fcmToken = newToken
    
    // update mongo
    if !userNeedsLogin(), let userId = stitchClient.auth.currentUser?.id {
        let tokenDoc: Document = [
            "fcm_token": newToken
        ]
        stitchClient.callFunction(withName: "updateUserNotificationSettings", withArgs: [userId, tokenDoc]) {
            (result: StitchResult<String>) in
            switch result {
            case .failure(let error):
                print("Stitch update user notification settings error: \(error)")
            case .success(let user_id):
                print("Successfully updated user notification settings to Stitch: \(user_id)")
            }
        }
    }
}
