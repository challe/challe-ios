//
//  Challenge.swift
//  Challe
//
//  Created by Rob Dearborn on 3/12/19.
//  Copyright © 2019 Challe. All rights reserved.
//

import UIKit

import StitchCore


class Challenge {
    
    var id: ObjectId
    var creatorId: String
    var userIsCreator: Bool
    var userIsFollower: Bool
    var userFollowsCreator: Bool
    var userHasCompleted: Bool
    var title: String
    var creator: String
    var visibility: String
    var description: String
    var dateFormatter: DateFormatter
    var startDate: Date
    var endDate: Date
    var nFollowers: Int64
    
    init?(challengeDoc: Document) {
        if let idStr = challengeDoc["_id"] as? String {
            self.id = ObjectId(idStr)!
        } else if let id = challengeDoc["_id"] as? ObjectId {
            self.id = id
        } else {
            self.id = ObjectId("000000000000000000000000")!
        }
        
        self.creatorId = challengeDoc["creator_id"] as? String ?? "000000000000000000000000"
        self.userIsCreator = stitchClient.auth.currentUser!.id == self.creatorId
        self.userIsFollower = challengeDoc["user_is_follower"] as? Bool ?? false
        self.userFollowsCreator = challengeDoc["user_follows_creator"] as? Bool ?? false
        self.userHasCompleted = challengeDoc["user_has_completed"] as? Bool ?? false
        
        let encodedTitle = challengeDoc["title"] as? String ?? "title"
        self.title = encodedTitle.utf8DecodedString()
        
        let encodedCreator = challengeDoc["creator"] as? String ?? "creator"
        self.creator = encodedCreator.utf8EncodedString()
        
        self.visibility = challengeDoc["visibility"] as? String ?? "Link sharing only"
        
        let encodedDescription = challengeDoc["description"] as? String ?? "description"
        self.description = encodedDescription.utf8DecodedString()
        
        self.dateFormatter = DateFormatter()
        self.dateFormatter.timeZone = TimeZone.init(secondsFromGMT: 0)
        self.dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        
        if let startDateStr = challengeDoc["start_date"] as? String {
            self.startDate = self.dateFormatter.date(from: startDateStr)!
        } else if let startDate = challengeDoc["start_date"] as? Date {
            self.startDate = startDate
        } else {
            self.startDate = Date()
        }
        
        if let endDateStr = challengeDoc["end_date"] as? String {
            self.endDate = self.dateFormatter.date(from: endDateStr)!
        } else if let endDate = challengeDoc["end_date"] as? Date {
            self.endDate = endDate
        } else {
            self.endDate = Date()
        }
        
        if let followersInt = challengeDoc["n_followers"] as? Int {
            self.nFollowers = Int64(exactly: followersInt)!
        } else if let nFollowers = challengeDoc["n_followers"] as? Int64 {
            self.nFollowers = nFollowers
        } else {
            self.nFollowers = Int64(exactly: 0)!
        }
    }
    
    func getAttributedCreatorStr() -> NSMutableAttributedString {
        let creatorText = ("by " + self.creator) as NSString
        let attributedCreatorText = NSMutableAttributedString(
            string: creatorText as String
        )
        let boldFontAttribute = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 14.0)]
        attributedCreatorText.addAttributes(boldFontAttribute, range: creatorText.range(of: creator))
        
        return attributedCreatorText
    }
    
    func getDatesStr() -> String {
        self.dateFormatter.dateFormat = "E MM/dd/yyyy"
        
        let datesText = (
            self.dateFormatter.string(from: self.startDate)
                + " - "
                + self.dateFormatter.string(from: self.endDate)
        )
        
        return datesText
    }
    
    func getFollowersStr() -> String {
        return toFriendlyStr(int: self.nFollowers)
    }

}
