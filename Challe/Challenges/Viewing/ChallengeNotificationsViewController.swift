//
//  ChallengeNotificationsViewController.swift
//  Challe
//
//  Created by Rob Dearborn on 5/28/19.
//  Copyright © 2019 Challe. All rights reserved.
//

import UIKit

import Firebase
import StitchCore


class ChallengeNotificationsViewController: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {   
    
    // MARK: Properties
    @IBOutlet weak var dailyChallengeRemindersSwitch: UISwitch!
    @IBOutlet weak var dailyChallengeRemindersReceiveAtField: UITextField!
    
    var challengeId: ObjectId?
    private var hourPicker = UIPickerView()
    private var userTimezone: String!

    // MARK: Overrides
    override func viewDidLoad() {
        super.viewDidLoad()

        dailyChallengeRemindersSwitch.isEnabled = false
        
        dailyChallengeRemindersReceiveAtField.isEnabled = false
        dailyChallengeRemindersReceiveAtField.delegate = self
        dailyChallengeRemindersReceiveAtField.inputView = hourPicker
        hourPicker.delegate = self
        
        // Load User Info
        guard let ownUserId = stitchClient.auth.currentUser?.id else {
            print("ChallengeNotificationsViewController error: user must be authenticated")
            return
        }
        
        guard let challengeId = challengeId else {
            print("ChallengeNotificationsViewController error: challengeId not set")
            return
        }
        
        stitchClient.callFunction(withName: "getUserChallengeNotificationSettings", withArgs: [challengeId, ownUserId]) { (result: StitchResult<Document>) in
            switch result {
            case .failure(let error):
                print("Stitch challenge notification settings collection error: \(String(describing: error))")
            case .success(let cnsDoc):
                
                if let receiveDailyChallengeReminders = cnsDoc["receive_challenge_reminders"] as? Bool, let receiveDailyChallengeRemindersAtUTCHour = cnsDoc["receive_challenge_reminders_at_utc_hour"] as? Int, let timezone = cnsDoc["timezone"] as? String {
                    DispatchQueue.main.async {
                        self.dailyChallengeRemindersSwitch.isOn = receiveDailyChallengeReminders
                        self.dailyChallengeRemindersReceiveAtField.isEnabled = receiveDailyChallengeReminders
                        self.dailyChallengeRemindersSwitch.isEnabled = true
                        
                        self.userTimezone = timezone
                        
                        let receiveDailyChallengeRemindersAtLocalHour = hourTimezoneConversion(hourOfDay: Int(receiveDailyChallengeRemindersAtUTCHour), firstTimezone: TimeZone(abbreviation: "UTC")!, secondTimezone: TimeZone(identifier: self.userTimezone)!)
                        self.dailyChallengeRemindersReceiveAtField.text = C.hourOptions[receiveDailyChallengeRemindersAtLocalHour]
                        
                        let row = C.hourOptions.firstIndex(of: self.dailyChallengeRemindersReceiveAtField.text!)!
                        self.hourPicker.selectRow(row, inComponent: 0, animated: true)
                    }
                }
            }
        }
    }
    
    // MARK: Picker methods
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView( _ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return C.hourOptions.count
    }
    
    func pickerView( _ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return C.hourOptions[row]
    }
    
    func pickerView( _ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        let hourOption = C.hourOptions[row]
        let receiveDailyChallengeRemindersAtLocalHour = C.hourOptions.firstIndex(of: hourOption)!
        
        let receiveDailyChallengeRemindersAtUTCHour = hourTimezoneConversion(hourOfDay: receiveDailyChallengeRemindersAtLocalHour, firstTimezone: TimeZone(identifier: self.userTimezone)!, secondTimezone: TimeZone(abbreviation: "UTC")!)
        
        // update user notification settings
        let updateDoc: Document = [
            "receive_challenge_reminders_at_utc_hour": Int64(exactly: receiveDailyChallengeRemindersAtUTCHour)!
        ]
        updateMongoUcns(updateDoc: updateDoc)
        
        return dailyChallengeRemindersReceiveAtField.text = hourOption
        
    }
    
    // MARK: Button Actions
    @IBAction func dismissVC(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
        
    @IBAction func dailyChallengeRemindersSwitchAction(_ sender: UISwitch) {
        dailyChallengeRemindersReceiveAtField.isEnabled = dailyChallengeRemindersSwitch.isOn
        
        let updateDoc: Document = [
            "receive_challenge_reminders": dailyChallengeRemindersSwitch.isOn
        ]
        updateMongoUcns(updateDoc: updateDoc)
    }
    
    private func updateMongoUcns(updateDoc: Document) {
        let userId = stitchClient.auth.currentUser!.id
        
        stitchClient.callFunction(withName: "updateUserChallengeNotificationSettings", withArgs: [challengeId!, userId, updateDoc]) {
            (result: StitchResult<String>) in
            switch result {
            case .failure(let error):
                print("Stitch update user challenge notification settings error: \(error)")
            case .success(let user_id):
                print("Successfully updated user challenge notification settings to Stitch: \(user_id)")
            }
        }
    }

}
