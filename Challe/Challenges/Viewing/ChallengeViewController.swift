//
//  ChallengeViewController.swift
//  Challe
//
//  Created by Rob Dearborn on 3/6/19.
//  Copyright © 2019 Challe. All rights reserved.
//

import UIKit

import Firebase
import Presentr
import StitchCore


class ChallengeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    // MARK: Properties
    @IBOutlet weak var titleTextView: UITextView!
    @IBOutlet weak var creatorTextButton: UIButton!
    @IBOutlet weak var datesTextLabel: UILabel!
    @IBOutlet weak var followersButton: UIButton!
    @IBOutlet weak var followersTextLabel: UILabel!
    @IBOutlet weak var joinButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var moreButton: UIButton!
    @IBOutlet weak var descriptionTextLabel: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var subChallengesTextLabel: UILabel!
    @IBOutlet weak var subChallengesTableView: UITableView!
    @IBOutlet weak var privateChallengeTextLabel: UILabel!
    @IBOutlet weak var privateChallengeTextView: UITextView!
    @IBOutlet weak var privateChallengeCreatorButton: UIButton!
    @IBOutlet weak var privateChallengeView: UIView!
    @IBOutlet weak var commentsTextLabel: UILabel!
    @IBOutlet weak var viewAllCommentsButton: UIButton!
    @IBOutlet weak var commentsView: UIView!
    
    var challengeId: ObjectId?
    private var challenge: Challenge!
    private var subChallenges = [SubChallenge]()
    private var userIsCreator = false
    private var vSpinner: UIView?
    
    // MARK: Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // initially hides for security
        initialHides()
        
        // Initialize description delegate and borders
        descriptionTextView.layer.borderColor = UIColor.lightGray.cgColor
        descriptionTextView.layer.borderWidth = 0.25
        descriptionTextView.layer.cornerRadius = 5.0
        
        // Initialize subchallenges table view
        subChallengesTableView.delegate = self
        subChallengesTableView.dataSource = self
        subChallengesTableView.isEditing = false
        subChallengesTableView.showsVerticalScrollIndicator = false
        subChallengesTableView.showsHorizontalScrollIndicator = false
        
        // Initialize comments view
        if let vc = storyboard?.instantiateViewController(withIdentifier: "commentsViewController") as? CommentsViewController {
            vc.threadType = "challenge"
            vc.threadId = challengeId
            vc.commentsPerPage = C.challengeRecentCommentsPerPage
            vc.enableContinuedLoading = false
            vc.view.translatesAutoresizingMaskIntoConstraints = false
            
            addChild(vc)
            commentsView.addSubview(vc.view)
            
            NSLayoutConstraint.activate([
                vc.view.leadingAnchor.constraint(equalTo: commentsView.leadingAnchor),
                vc.view.trailingAnchor.constraint(equalTo: commentsView.trailingAnchor),
                vc.view.topAnchor.constraint(equalTo: commentsView.topAnchor),
                vc.view.bottomAnchor.constraint(equalTo: commentsView.bottomAnchor)
            ])
            
            vc.didMove(toParent: self)
        }
        
        // Listeners
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(onChallengeUpdate(_:)),
            name: .didUpdateChallenge,
            object: nil
        )
        
        // Relevant when a user follows a private challenge's creator
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(onUpdateOwnFollowing(_:)),
            name: .didUpdateOwnFollowing,
            object: nil
        )
        
        // Swipe to dismiss
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(dismissVC(_:)))
        swipeDown.direction = UISwipeGestureRecognizer.Direction.down
        self.view.addGestureRecognizer(swipeDown)
        
        loadChallenge()
    }
    
    // MARK: data loading
    func initialHides() {
        titleTextView.isHidden = true
        creatorTextButton.isHidden = true
        datesTextLabel.isHidden = true
        followersButton.isHidden = true
        followersTextLabel.isHidden = true
        joinButton.isHidden = true
        shareButton.isHidden = true
        moreButton.isHidden = true
        descriptionTextLabel.isHidden = true
        descriptionTextView.isHidden = true
        subChallengesTextLabel.isHidden = true
        subChallengesTableView.isHidden = true
        privateChallengeTextLabel.isHidden = true
        privateChallengeTextView.isHidden = true
        privateChallengeCreatorButton.isHidden = true
        privateChallengeView.isHidden = true
        commentsTextLabel.isHidden = true
        viewAllCommentsButton.isHidden = true
        commentsView.isHidden = true
    }
    
    func loadChallenge() {
        guard let challengeId = challengeId else {
            print("error: ChallengeViewController created w/o setting challengeId")
            return
        }
        
        if vSpinner == nil || vSpinner?.superview == nil {
            vSpinner = showSpinner(onView: self.view)
        }
        
        stitchClient.callFunction(withName: "getChallenge", withArgs: [challengeId]) { (result: StitchResult<Document>) in
            switch result {
            case .failure(let error):
                print("Stitch challenge collection error: \(String(describing: error))")
                if self.vSpinner != nil { self.removeSpinner(vSpinner: self.vSpinner!) }
                if C.stitchAuthErrorDescriptions.contains(error.description) {
                    DispatchQueue.main.async {
                        logoutFlow(presentFrom: self)
                    }
                }
            case .success(let challengeDoc):
                DispatchQueue.main.async {
                    
                    self.challenge = Challenge(challengeDoc: challengeDoc)
                    self.subChallenges.removeAll()
                    
                    if (self.challenge.visibility == "Everyone" || self.challenge.userIsFollower || self.challenge.userFollowsCreator) {
                        
                        self.titleTextView.text = self.challenge.title
                        self.userIsCreator = self.challenge.userIsCreator
                        self.creatorTextButton.setAttributedTitle(
                            self.challenge.getAttributedCreatorStr(),
                            for: .normal
                        )
                        self.datesTextLabel.text = self.challenge.getDatesStr()
                        self.followersTextLabel.text = self.challenge.getFollowersStr()
                        self.descriptionTextView.text = self.challenge.description
                        self.updateJoinButton(postNotification: false)
                        
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "MM/dd/yyyy"
                        dateFormatter.timeZone = TimeZone.init(secondsFromGMT: 0)
                        guard let subChallengesDoc = challengeDoc["sub_challenges"] as? Document else {
                            print("error unwrapping subChallengesDoc")
                            return
                        }
                        for idx in 0..<subChallengesDoc.count {
                            guard let subChallengeDoc = subChallengesDoc[String(idx)] as? Document,
                                  let subChallengeDate = subChallengeDoc["date"] as? Date,
                                  let subChallengeDateStr = dateFormatter.string(from: subChallengeDate) as? String,
                                  let encodedSubChallengeDesc = subChallengeDoc["description"] as? String,
                                  let subChallengeIsComplete = subChallengeDoc["is_complete"] as? Bool,
                                  let newSubChallenge = SubChallenge(
                                    is_complete: subChallengeIsComplete,
                                    dateStr: subChallengeDateStr,
                                    description: encodedSubChallengeDesc.utf8DecodedString()
                                  )
                            else {
                                print("error unwrapping subChallengesDoc")
                                return
                            }
                            self.subChallenges.append(newSubChallenge)
                        }
                        self.subChallengesTableView.reloadData()
                        
                        self.titleTextView.isHidden = false
                        self.creatorTextButton.isHidden = false
                        self.datesTextLabel.isHidden = false
                        self.followersButton.isHidden = false
                        self.followersTextLabel.isHidden = false
                        self.joinButton.isHidden = false
                        self.shareButton.isHidden = false
                        self.moreButton.isHidden = false
                        self.descriptionTextLabel.isHidden = false
                        self.descriptionTextView.isHidden = false
                        self.subChallengesTextLabel.isHidden = false
                        self.subChallengesTableView.isHidden = false
                        self.commentsTextLabel.isHidden = false
                        self.viewAllCommentsButton.isHidden = false
                        self.commentsView.isHidden = false
                        
                        // populate subchallenges progress
                        // nested here to avoid race conditions on subChallenges
                        stitchClient.callFunction(withName: "getUserChallenge",
                                                  withArgs: [challengeId,  stitchClient.auth.currentUser!.id]) { (result: StitchResult<[Document]>) in
                                                    switch result {
                                                    case .failure(let error):
                                                        print("Stitch user challenge collection error: \(String(describing: error))")
                                                        if self.vSpinner != nil { self.removeSpinner(vSpinner: self.vSpinner!) }
                                                        if C.stitchAuthErrorDescriptions.contains(error.description) {
                                                            DispatchQueue.main.async {
                                                                DispatchQueue.main.async {
                                                                    logoutFlow(presentFrom: self)
                                                                }
                                                            }
                                                        }
                                                    case .success(let userChallengeArray):
                                                        if userChallengeArray.count > 0 {
                                                            let userChallengeDoc = userChallengeArray[0]
                                                            DispatchQueue.main.async {
                                                                guard let userProgressionDoc = userChallengeDoc["user_progression"] as? Document else {
                                                                    print("error unpacking userProgressionDoc")
                                                                    return
                                                                }
                                                                for idx in 0..<userProgressionDoc.count {
                                                                    guard let subChallengeDoc = userProgressionDoc[String(idx)] as? Document,
                                                                          let subChallengeIsComplete = subChallengeDoc["is_complete"] as? Bool
                                                                    else {
                                                                        print("error unpacking userProgressionDoc")
                                                                        return
                                                                    }
                                                                    
                                                                    self.subChallenges[idx].is_complete = subChallengeIsComplete
                                                                }
                                                                self.subChallengesTableView.reloadData()
                                                            }
                                                        }
                                                        Analytics.logEvent("view_challenge", parameters: ["id": "\(self.challengeId)"])
                                                        if self.vSpinner != nil { self.removeSpinner(vSpinner: self.vSpinner!) }
                                                    }
                        }
                    } else {
                        self.privateChallengeTextLabel.isHidden = false
                        self.privateChallengeTextView.isHidden = false
                        self.privateChallengeCreatorButton.setTitle(self.challenge.creator, for: .normal)
                        self.privateChallengeCreatorButton.isHidden = false
                        self.privateChallengeView.isHidden = false
                        Analytics.logEvent("view_challenge", parameters: ["id": "\(self.challengeId)"])
                        if self.vSpinner != nil { self.removeSpinner(vSpinner: self.vSpinner!) }
                    }
                }
            }
        }
    }
    
    // MARK: keyboard methods
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    // MARK: Table view methods
    // initialize # of rows
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return subChallenges.count
    }
    
    // form cells
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
        let cellIdentifier = "SubChallengeTableViewCell"
    
        // create cell
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? SubChallengeTableViewCell else {
            fatalError("The dequeued cell is not an instance of SubChallengeTableViewCell.")
        }
        cell.selectionStyle = .none
    
        // load cell data
        let subChallenge = subChallenges[indexPath.row]
        cell.challengeId = challengeId!
        cell.subChallengeIdx = indexPath.row
        cell.subChallengeCheckbox.isEnabled = challenge.userIsFollower
        cell.subChallengeCheckbox.isSelected = subChallenge.is_complete
        cell.subChallengeDateField.text = subChallenge.dateStr
        cell.subChallengeDescField.text = subChallenge.description
        
        // allow for persistant text editing
        cell.setSyncCheckboxChange {[weak self] checkBoxIsSelected in
            Analytics.logEvent("toggle_subchallenge_complete", parameters: ["is_complete": checkBoxIsSelected])
            self?.subChallenges[indexPath.row].is_complete = checkBoxIsSelected
            self?.updateJoinButton(postNotification: true) // toggling is propogate to background challenges in here
        }
    
        return cell
    }
    
    // MARK: Update join button
    private func updateJoinButton(postNotification: Bool) {
        
        joinButton.isSelected = challenge.userIsFollower
        
        if challenge.userIsFollower {
            
            if (subChallenges.count > 0) {
                var userHasCompleted = true
                for subChallenge in subChallenges {
                    userHasCompleted = userHasCompleted && subChallenge.is_complete
                }
                
                if userHasCompleted != challenge.userHasCompleted {
                    challenge.userHasCompleted = userHasCompleted
                    Analytics.logEvent("toggle_challenge_complete", parameters: ["is_complete": userHasCompleted])
                }
            }
            
            if challenge.userHasCompleted {
                joinButton.setImage(UIImage(named: "check_green"), for: .selected)
            } else {
                joinButton.setImage(UIImage(named: "calendar"), for: .selected)
            }
            
        } else {
            joinButton.setImage(UIImage(named: "plus"), for: .normal)
        }
        
        if postNotification {
            NotificationCenter.default.post(
                name: .didUpdateChallenge,
                object: nil,
                userInfo: ["challenge": challenge]
            )
        }
    }
    
    // MARK: Listeners
    @objc private func onChallengeUpdate(_ notification:Notification) {
        if self.topMostViewController() != self { // only reload if in background
            DispatchQueue.main.async {
                self.initialHides()
                self.loadChallenge()
            }
        }
    }
    
    @objc private func onUpdateOwnFollowing(_ notification:Notification) {
        DispatchQueue.main.async {
            self.initialHides()
            self.loadChallenge()
        }
    }
    
    // MARK: Button Actions
    @IBAction func dismissVC(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func creatorTextButtonAction(_ sender: UIButton) {
        let presenter = Presentr(presentationType: .fullScreen)
        
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "otherProfileViewController") as? OtherProfileViewController {
            vc.challeUserId = challenge.creatorId
            customPresentViewController(presenter, viewController: vc, animated: true)
        }
    }
    
    @IBAction func joinButtonAction(_ sender: UIButton) {
        guard let challengeId = challengeId else {
            print("error: ChallengeViewController created w/o setting challengeId")
            return
        }
        
        if challenge.userIsFollower {
            // load actionSheet menu
            let alertController = UIAlertController(
                title: nil,
                message: nil,
                preferredStyle: .actionSheet
            )
            
            // cancel button
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
                print("Challenge deletion canceled.")
            }
            alertController.addAction(cancelAction)
            
            // edit notifications button --> half panel
            let notificationsAction = UIAlertAction(title: "Add/Edit Reminders", style: .default) { (action) in
                
                let presenter = Presentr(presentationType: .bottomHalf)
                presenter.keyboardTranslationType = .moveUp
                
                if let vc = self.storyboard?.instantiateViewController(withIdentifier: "challengeNotificationsViewController") as? ChallengeNotificationsViewController {
                    
                    vc.challengeId = self.challengeId
                    
                    self.customPresentViewController(presenter, viewController: vc, animated: true)
                }
                
            }
            alertController.addAction(notificationsAction)
            
            // add to calendar button --> half panel
            let calendarAction = UIAlertAction(title: "Add to Calendar", style: .default) { (action) in
                print("Adding to calendar...")
            }
            alertController.addAction(calendarAction)
            
            // leave button --> alert
            let destroyAction = UIAlertAction(title: "Leave Challenge", style: .destructive) { (action) in
                
                let alertController = UIAlertController(
                    title: "Leave this challenge?",
                    message: "You can re-join at any time, but saved progress will be lost.",
                    preferredStyle: .alert
                )
                
                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
                    print("User challenge deletion canceled.")
                }
                alertController.addAction(cancelAction)
                
                let destroyAction = UIAlertAction(title: "Leave", style: .destructive) { (action) in
                    if self.vSpinner == nil || self.vSpinner?.superview == nil {
                        self.vSpinner = self.showSpinner(onView: self.view)
                    }
                    
                    stitchClient.callFunction(withName: "deleteUserChallenge", withArgs: [challengeId, stitchClient.auth.currentUser!.id]) { (result: StitchResult<String>) in
                        switch result {
                        case .failure(let error):
                            print("Stitch user challenge deletion error: \(String(describing: error))")
                            if self.vSpinner != nil { self.removeSpinner(vSpinner: self.vSpinner!) }
                            if C.stitchAuthErrorDescriptions.contains(error.description) {
                                DispatchQueue.main.async {
                                    logoutFlow(presentFrom: self)
                                }
                            }
                        case .success(_):
                            DispatchQueue.main.async {
                                for (n, _) in self.subChallenges.enumerated() {
                                    self.subChallenges[n].is_complete = false
                                }
                                self.challenge.userIsFollower = false
                                self.challenge.userHasCompleted = false
                                self.challenge.nFollowers -= 1
                                self.followersTextLabel.text = self.challenge.getFollowersStr()
                                self.updateJoinButton(postNotification: true)
                                self.subChallengesTableView.reloadData()
                                Analytics.logEvent("leave_challenge", parameters: ["id": "\(self.challengeId)"])
                                if self.vSpinner != nil { self.removeSpinner(vSpinner: self.vSpinner!) }
                            }
                        }
                    }
                }
                alertController.addAction(destroyAction)
                
                self.present(alertController, animated: true) {
                    // ...
                }
            }
            alertController.addAction(destroyAction)
            
            self.present(alertController, animated: true) {
                // ...
            }
        } else {
            if vSpinner == nil || vSpinner?.superview == nil {
                vSpinner = showSpinner(onView: self.view)
            }
            
            var subChallengesProgressionDoc: Document = []
            for (n, subChallenge) in subChallenges.enumerated() {
                // for user_challenge record initialization
                let subChallengeProgressionDoc: Document = [
                    "is_complete": subChallenge.is_complete // always false
                ]
                subChallengesProgressionDoc[String(n)] = subChallengeProgressionDoc
            }
            let newUserChallenge: Document = [
                "creator_id": stitchClient.auth.currentUser!.id,
                "created_at": Date(),
                "is_deleted": false,
                "challenge_id": challengeId,
                "user_progression": subChallengesProgressionDoc,
                "n_subchallenges": Int64(exactly: subChallengesProgressionDoc.count)!,
                "n_completed_subchallenges": 0 as Int64
            ]
            stitchClient.callFunction(withName: "createUserChallenge", withArgs: [newUserChallenge]) { (result: StitchResult<String>) in
                switch result {
                case .failure(let error):
                    print("Stitch user_challenge creation error: \(String(describing: error))")
                    if self.vSpinner != nil { self.removeSpinner(vSpinner: self.vSpinner!) }
                    if C.stitchAuthErrorDescriptions.contains(error.description) {
                        DispatchQueue.main.async {
                            logoutFlow(presentFrom: self)
                        }
                    }
                case .success(let userChallengeId):
                    print("Stitch created user challenge with ID: \(userChallengeId)")
                    DispatchQueue.main.async {
                        self.challenge.userIsFollower = true
                        self.challenge.nFollowers += 1
                        self.followersTextLabel.text = self.challenge.getFollowersStr()
                        self.updateJoinButton(postNotification: true)
                        self.subChallengesTableView.reloadData()
                        Analytics.logEvent("join_challenge", parameters: ["id": "\(self.challengeId)"])
                        if self.vSpinner != nil { self.removeSpinner(vSpinner: self.vSpinner!) }
                    }
                }
            }
        }
    }
    
    @IBAction func shareButtonAction(_ sender: UIButton) {
        guard let challengeId = challengeId else {
            print("error: ChallengeViewController created w/o setting challengeId")
            return
        }
        
        Analytics.logEvent("share_challenge", parameters: ["id": "\(self.challengeId)"])
        
        var text = "https://challe.co/challenge/\(challengeId)"
        
        // set up activity view controller
        let textToShare = [text]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
        
    }
    
    
    @IBAction func moreButtonAction(_ sender: UIButton) {
        moreButton.isSelected = true
        let alertController = UIAlertController(
            title: nil,
            message: nil,
            preferredStyle: .actionSheet
        )
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            print("Challenge deletion canceled.")
            self.moreButton.isSelected = false
        }
        alertController.addAction(cancelAction)
        
        let destroyAction = setDestroyAction()
        alertController.addAction(destroyAction)
        
        self.present(alertController, animated: true) {
            // ...
        }
    }
    
    private func setDestroyAction() -> UIAlertAction {
        
        var destroyAction: UIAlertAction
        if userIsCreator {
            destroyAction = UIAlertAction(title: "Delete Challenge", style: .destructive) { (action) in
                let alertController = UIAlertController(
                    title: "Delete this challenge?",
                    message: "Deleted challenges cannot be recovered.",
                    preferredStyle: .alert
                )
                
                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
                    print("Challenge deletion canceled.")
                    self.moreButton.isSelected = false
                }
                alertController.addAction(cancelAction)
                
                let destroyAction = UIAlertAction(title: "Delete", style: .destructive) { (action) in
                    stitchClient.callFunction(withName: "deleteChallenge", withArgs: [self.challengeId!]) { (result: StitchResult<String>) in
                        switch result {
                        case .failure(let error):
                            print("Stitch challenge deletion error: \(String(describing: error))")
                            if C.stitchAuthErrorDescriptions.contains(error.description) {
                                DispatchQueue.main.async {
                                    logoutFlow(presentFrom: self)
                                }
                            }
                        case .success(_):
                            DispatchQueue.main.async {
                                Analytics.logEvent("delete_challenge", parameters: ["id": "\(self.challengeId)"])
                                NotificationCenter.default.post(
                                    name: .didDeleteChallenge,
                                    object: nil,
                                    userInfo: ["id": self.challengeId!]
                                )
                                self.dismiss(animated: true, completion: nil)
                            }
                        }
                    }
                    self.moreButton.isSelected = false
                }
                alertController.addAction(destroyAction)
                
                self.present(alertController, animated: true) {
                    // ...
                }
            }
        } else {
            destroyAction = UIAlertAction(title: "Report as Inappropriate", style: .destructive) { (action) in
                let alertController = UIAlertController(
                    title: "Report this challenge?",
                    message: "Reported challenges may be removed if inappropriate.",
                    preferredStyle: .alert
                )
                
                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
                    print("Challenge flagging canceled.")
                    self.moreButton.isSelected = false
                }
                alertController.addAction(cancelAction)
                
                let destroyAction = UIAlertAction(title: "Report", style: .destructive) { (action) in
                    let flagDoc: Document = [
                        "creator_id": stitchClient.auth.currentUser!.id,
                        "created_at": Date(),
                        "target_type": "challenge",
                        "target_id": self.challengeId!
                    ]
                    stitchClient.callFunction(withName: "createFlag", withArgs: [flagDoc]) { (result: StitchResult<String>) in
                        switch result {
                        case .failure(let error):
                            print("Stitch flag creation error: \(String(describing: error))")
                            if C.stitchAuthErrorDescriptions.contains(error.description) {
                                DispatchQueue.main.async {
                                    logoutFlow(presentFrom: self)
                                }
                            }
                        case .success(let successMessage):
                            Analytics.logEvent("flag_challenge", parameters: ["id": "\(self.challengeId)"])
                            print(successMessage)
                        }
                    }
                    self.moreButton.isSelected = false
                }
                alertController.addAction(destroyAction)
                
                self.present(alertController, animated: true) {
                    // ...
                }
            }
        }
        
        return destroyAction
    }
    
    
    @IBAction func followersButtonAction(_ sender: Any) {
        guard let challengeId = challengeId else {
            print("error: ChallengeViewController created w/o setting challengeId")
            return
        }
        
        let presenter = Presentr(presentationType: .fullScreen)
        
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "usersFeedViewController") as? UsersFeedViewController {
            Analytics.logEvent("view_challenge_followers", parameters: ["id": self.challengeId])
            vc.queryType = .challengeFollowers
            vc.queryString = "\(challengeId)"
            self.customPresentViewController(presenter, viewController: vc, animated: true)
        }
    }
    
    @IBAction func privateChallengeCreatorButtonAction(_ sender: UIButton) {
        let presenter = Presentr(presentationType: .fullScreen)
        
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "otherProfileViewController") as? OtherProfileViewController {
            vc.challeUserId = challenge.creatorId
            customPresentViewController(presenter, viewController: vc, animated: true)
        }
    }
    
    
    @IBAction func viewAllCommentsButtonAction(_ sender: UIButton) {
        guard let challengeId = challengeId else {
            print("error: ChallengeViewController created w/o setting challengeId")
            return
        }
        
        let presenter = Presentr(presentationType: .fullScreen)
        
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "allCommentsViewController") as? AllCommentsViewController {
            vc.threadType = "challenge"
            vc.threadId = challengeId
            customPresentViewController(presenter, viewController: vc, animated: true)
        }
    }
    

}
