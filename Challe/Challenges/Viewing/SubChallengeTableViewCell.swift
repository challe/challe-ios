//
//  SubChallengeTableViewCell.swift
//  Challe
//
//  Created by Rob Dearborn on 3/6/19.
//  Copyright © 2019 Challe. All rights reserved.
//

import UIKit

import StitchCore


class SubChallengeTableViewCell: UITableViewCell {
    
    // MARK: Properties
    @IBOutlet weak var subChallengeCheckbox: UIButton!
    @IBOutlet weak var subChallengeDateField: UITextView!
    @IBOutlet weak var subChallengeDescField: UITextView!
    
    var challengeId: ObjectId?
    var subChallengeIdx: Int?
    private var syncCheckboxChange: ((Bool) -> Void)?
    
    // MARK: Overrides
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    // MARK: persist checkbox edits
    func setSyncCheckboxChange(action: @escaping (Bool) -> Void) {
        self.syncCheckboxChange = action
    }
    
    @IBAction func subChallengeCheckboxAction(_ sender: UIButton) {
        guard let challengeId = challengeId else {
            print("SubChallengeTableViewCell error: challengeId not set")
            return
        }
        
        guard let subChallengeIdx = subChallengeIdx else {
            print("SubChallengeTableViewCell error: subChallengeIdx not set")
            return
        }
        
        if subChallengeCheckbox.isEnabled {
            let newCheckboxIsSelected = !self.subChallengeCheckbox.isSelected
            let updatedSubChallengeProgression: Document = [
                "is_complete": newCheckboxIsSelected
            ]
            stitchClient.callFunction(withName: "updateUserChallenge", withArgs: [challengeId, stitchClient.auth.currentUser!.id, subChallengeIdx, updatedSubChallengeProgression]) { (result: StitchResult<String>) in
                switch result {
                case .failure(let error):
                    print("Stitch user challenge updating error: \(String(describing: error))")
                case .success(_):
                    DispatchQueue.main.async {
                        self.subChallengeCheckbox.isSelected = newCheckboxIsSelected
                        self.syncCheckboxChange?(newCheckboxIsSelected)
                    }
                }
            }
        }
    }
    
    
}
