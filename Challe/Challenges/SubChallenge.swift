//
//  SubChallenge.swift
//  Challe
//
//  Created by Rob Dearborn on 2/28/19.
//  Copyright © 2019 Challe. All rights reserved.
//

import UIKit


class SubChallenge {
    
    var is_complete: Bool
    var dateStr: String
    var description: String
    
    init?(is_complete: Bool, dateStr: String, description: String) {
        self.is_complete = is_complete
        self.dateStr = dateStr
        self.description = description
        
    }
    
}
