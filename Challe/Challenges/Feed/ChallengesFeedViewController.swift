//
//  ChallengesFeedViewController.swift
//  Challe
//
//  Created by Rob Dearborn on 3/12/19.
//  Copyright © 2019 Challe. All rights reserved.
//

import UIKit

import Firebase
import Presentr


class ChallengesFeedViewController: UIViewController, UITextFieldDelegate {
    
    //MARK: Properties
    @IBOutlet weak var challeLogoImage: UIImageView!
    @IBOutlet weak var filterButton: UIButton!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var searchField: UITextField!
    @IBOutlet weak var searchCloseButton: UIButton!
    @IBOutlet weak var composeButton: UIButton!
    @IBOutlet weak var searchSegmentedControl: UISegmentedControl!
    @IBOutlet weak var feedViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var feedView: UIView!
    @IBOutlet weak var feedButton: UIButton!
    @IBOutlet weak var profileButton: UIButton!
    
    var challengesTableViewController: ChallengesTableViewController!
    var usersTableViewController: UsersTableViewController!
    
    // MARK: Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchField.delegate = self
        searchField.isHidden = true
        searchSegmentedControl.isHidden = true
        searchCloseButton.isHidden = true
        
        // Initialize challenges tableview
        challengesTableViewController = storyboard!.instantiateViewController(withIdentifier: "challengesTableViewController") as! ChallengesTableViewController
        challengesTableViewController.view.translatesAutoresizingMaskIntoConstraints = false
        addChild(challengesTableViewController)
        
        feedView.addSubview(challengesTableViewController.view)
        
        NSLayoutConstraint.activate([
            challengesTableViewController.view.leadingAnchor.constraint(equalTo: feedView.leadingAnchor),
            challengesTableViewController.view.trailingAnchor.constraint(equalTo: feedView.trailingAnchor),
            challengesTableViewController.view.topAnchor.constraint(equalTo: feedView.topAnchor),
            challengesTableViewController.view.bottomAnchor.constraint(equalTo: feedView.bottomAnchor)
            ])
        
        challengesTableViewController.didMove(toParent: self)
        
        // Intialize users tableview for use in search
        usersTableViewController = storyboard!.instantiateViewController(withIdentifier: "usersTableViewController") as! UsersTableViewController
        usersTableViewController.view.translatesAutoresizingMaskIntoConstraints = false
        addChild(usersTableViewController)
        
        feedView.addSubview(usersTableViewController.view)
        
        NSLayoutConstraint.activate([
            usersTableViewController.view.leadingAnchor.constraint(equalTo: feedView.leadingAnchor),
            usersTableViewController.view.trailingAnchor.constraint(equalTo: feedView.trailingAnchor),
            usersTableViewController.view.topAnchor.constraint(equalTo: feedView.topAnchor),
            usersTableViewController.view.bottomAnchor.constraint(equalTo: feedView.bottomAnchor)
            ])
        
        usersTableViewController.didMove(toParent: self)
        
        
        // Display, load challenges table
        feedView.bringSubviewToFront(challengesTableViewController.view)
        challengesTableViewController.updateFiltsAndReload(
            creatorQueryArg: C.challengesFeedCreatorFiltOptionsToMongoArgs[keychain.get(KeychainKeys.challengesFeedCreatorFilt.rawValue)!]!,
            sortByQueryArg: C.challengesFeedSortByOptionsToQueryArgs[keychain.get(KeychainKeys.challengesFeedSortBy.rawValue)!]!,
            sortByQueryOrder: C.challengesFeedSortByOptionsToQueryOrders[keychain.get(KeychainKeys.challengesFeedSortBy.rawValue)!]!,
            startingFilt: keychain.get(KeychainKeys.challengesFeedStartingFilt.rawValue)!,
            queryString: searchField.text!
        )
        
        // challenge filter listener
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(onFilterChange(_:)),
            name: .didChangeFeedFilter,
            object: nil
        )
    }
    
    // exit keyboards / pickers
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    // MARK: Search
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == searchField {
            challengesTableViewController.updateFiltsAndReload(
                creatorQueryArg: C.challengesFeedCreatorFiltOptionsToMongoArgs[keychain.get(KeychainKeys.challengesFeedCreatorFilt.rawValue)!]!,
                sortByQueryArg: C.challengesFeedSortByOptionsToQueryArgs[keychain.get(KeychainKeys.challengesFeedSortBy.rawValue)!]!,
                sortByQueryOrder: C.challengesFeedSortByOptionsToQueryOrders[keychain.get(KeychainKeys.challengesFeedSortBy.rawValue)!]!,
                startingFilt: keychain.get(KeychainKeys.challengesFeedStartingFilt.rawValue)!,
                queryString: searchField.text!
            )
            
            var searchType = "challenges"
            if searchSegmentedControl.selectedSegmentIndex == 1 {
                searchType = "users"
            }
            Analytics.logEvent(AnalyticsEventSearch, parameters: ["search_term": "\(searchType) - \(searchField.text!)"])
            
            usersTableViewController.updateRequestAndReload(queryType: .search, queryString: searchField.text!)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        
        return newString.length <= C.feedQueryMaxChars
    }
    
    // MARK: Listeners
    // Filter change listener
    @objc func onFilterChange(_ notification:Notification) {
        challengesTableViewController.updateFiltsAndReload(
            creatorQueryArg: C.challengesFeedCreatorFiltOptionsToMongoArgs[keychain.get(KeychainKeys.challengesFeedCreatorFilt.rawValue)!]!,
            sortByQueryArg: C.challengesFeedSortByOptionsToQueryArgs[keychain.get(KeychainKeys.challengesFeedSortBy.rawValue)!]!,
            sortByQueryOrder: C.challengesFeedSortByOptionsToQueryOrders[keychain.get(KeychainKeys.challengesFeedSortBy.rawValue)!]!,
            startingFilt: keychain.get(KeychainKeys.challengesFeedStartingFilt.rawValue)!,
            queryString: searchField.text!
        )
    }

    // MARK: Button actions
    @IBAction func filterButtonAction(_ sender: UIButton) {
        let presenter = Presentr(presentationType: .bottomHalf)
        presenter.keyboardTranslationType = .moveUp
        
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "challengesFeedFilterViewController") as? ChallengesFeedFilterViewController {
            customPresentViewController(presenter, viewController: vc, animated: true)
        }
    }
    
    @IBAction func searchButtonAction(_ sender: UIButton) {
        // make way for UISegmentedControl
        feedViewTopConstraint.constant = 16.0 + 30.0
        
        searchField.isHidden = false
        searchCloseButton.isHidden = false
        searchSegmentedControl.isHidden = false
    }
    
    @IBAction func searchCloseButtonAction(_ sender: UIButton) {
        // move back now that UISegmentedControl is gone
        feedViewTopConstraint.constant = 16.0
        
        searchField.text = ""
        
        searchField.isHidden = true
        searchCloseButton.isHidden = true
        searchSegmentedControl.isHidden = true
        searchSegmentedControl.selectedSegmentIndex = 0
        
        challengesTableViewController.updateFiltsAndReload(
            creatorQueryArg: C.challengesFeedCreatorFiltOptionsToMongoArgs[keychain.get(KeychainKeys.challengesFeedCreatorFilt.rawValue)!]!,
            sortByQueryArg: C.challengesFeedSortByOptionsToQueryArgs[keychain.get(KeychainKeys.challengesFeedSortBy.rawValue)!]!,
            sortByQueryOrder: C.challengesFeedSortByOptionsToQueryOrders[keychain.get(KeychainKeys.challengesFeedSortBy.rawValue)!]!,
            startingFilt: keychain.get(KeychainKeys.challengesFeedStartingFilt.rawValue)!,
            queryString: searchField.text!
        )
        
        usersTableViewController.challeUsers.removeAll()
        usersTableViewController.tableView.reloadData()
        feedView.bringSubviewToFront(challengesTableViewController.view)
    }
    
    @IBAction func composeButtonAction(_ sender: UIButton) {
        let presenter = Presentr(presentationType: .fullScreen)
        
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "createChallengeViewController") as? CreateChallengeViewController {
            customPresentViewController(presenter, viewController: vc, animated: true)
        }
    }
    
    @IBAction func searchSegmentedControlAction(_ sender: UISegmentedControl) {
        let selectedIndex = self.searchSegmentedControl.selectedSegmentIndex
        var feedType: String
        if selectedIndex == 0 {
            feedView.bringSubviewToFront(challengesTableViewController.view)
            feedType = "challenges"
        } else {
            feedView.bringSubviewToFront(usersTableViewController.view)
            feedType = "users"
        }
        Analytics.logEvent("toggle_search_feed", parameters: ["type": feedType])
    }
    
    @IBAction func feedButtonAction(_ sender: UIButton) {
        feedViewTopConstraint.constant = 16.0
        
        searchField.text = ""
        
        searchField.isHidden = true
        searchCloseButton.isHidden = true
        searchSegmentedControl.isHidden = true
        searchSegmentedControl.selectedSegmentIndex = 0
        
        challengesTableViewController.updateFiltsAndReload(
            creatorQueryArg: C.challengesFeedCreatorFiltOptionsToMongoArgs[keychain.get(KeychainKeys.challengesFeedCreatorFilt.rawValue)!]!,
            sortByQueryArg: C.challengesFeedSortByOptionsToQueryArgs[keychain.get(KeychainKeys.challengesFeedSortBy.rawValue)!]!,
            sortByQueryOrder: C.challengesFeedSortByOptionsToQueryOrders[keychain.get(KeychainKeys.challengesFeedSortBy.rawValue)!]!,
            startingFilt: keychain.get(KeychainKeys.challengesFeedStartingFilt.rawValue)!,
            queryString: searchField.text!
        )
        
        usersTableViewController.challeUsers.removeAll()
        usersTableViewController.tableView.reloadData()
        feedView.bringSubviewToFront(challengesTableViewController.view)
    }
    
    @IBAction func profileActionButton(_ sender: UIButton) {
        self.performSegue(withIdentifier: "feedOwnProfileSegue", sender: self)
    }
    
}
