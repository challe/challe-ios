//
//  ChallengesFeedFilterViewController.swift
//  Challe
//
//  Created by Rob Dearborn on 3/13/19.
//  Copyright © 2019 Challe. All rights reserved.
//

import UIKit

import Firebase


class ChallengesFeedFilterViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    // MARK: Properties
    @IBOutlet weak var creatorField: UITextField!
    @IBOutlet weak var sortByField: UITextField!
    @IBOutlet weak var startingField: UITextField!
    @IBOutlet weak var minStartDateField: UITextField!
    @IBOutlet weak var maxStartDateField: UITextField!
    @IBOutlet weak var warningTextLabel: UILabel!
    
    private var creatorPicker = UIPickerView()
    private var sortByPicker = UIPickerView()
    private var dateFormatter = DateFormatter()
    private var startingPicker = UIPickerView()
    private var minStartDatePicker = UIDatePicker()
    private var maxStartDatePicker = UIDatePicker()
    
    // MARK: Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Creator selection
        creatorField.text = keychain.get(KeychainKeys.challengesFeedCreatorFilt.rawValue)!
        creatorField.inputView = creatorPicker
        creatorPicker.delegate = self
        var row = C.challengesFeedCreatorFiltOptions.firstIndex(of: creatorField.text!)!
        creatorPicker.selectRow(row, inComponent: 0, animated: true)
        
        // Sort by selection
        sortByField.text = keychain.get(KeychainKeys.challengesFeedSortBy.rawValue)!
        sortByField.inputView = sortByPicker
        sortByPicker.delegate = self
        row = C.challengesFeedSortByOptions.firstIndex(of: sortByField.text!)!
        sortByPicker.selectRow(row, inComponent: 0, animated: true)

        // Starting selection
        startingField.text = keychain.get(KeychainKeys.challengesFeedStartingFilt.rawValue)!
        startingField.inputView = startingPicker
        startingPicker.delegate = self
        row = C.challengesFeedStartingFiltOptions.firstIndex(of: startingField.text!)!
        startingPicker.selectRow(row, inComponent: 0, animated: true)
        
        // Custom start date
        dateFormatter.dateFormat = "MM/dd/yyyy"
        dateFormatter.timeZone = TimeZone.init(secondsFromGMT: 0)
        minStartDatePicker.datePickerMode = .date
        minStartDatePicker.timeZone = TimeZone.init(secondsFromGMT: 0)
        minStartDatePicker.addTarget(
            self,
            action: #selector(self.minStartDateChanged(datePicker:)),
            for: .valueChanged
        )
        minStartDateField.inputView = minStartDatePicker
        if startingField.text == "Custom" {
            minStartDateField.text = keychain.get(KeychainKeys.challengesFeedCustomMinStartDateStr.rawValue)!
        } else {
            minStartDateField.isHidden = true
        }
        
        // Custom end date
        maxStartDatePicker.datePickerMode = .date
        maxStartDatePicker.timeZone = TimeZone.init(secondsFromGMT: 0)
        maxStartDatePicker.addTarget(
            self,
            action: #selector(self.maxStartDateChanged(datePicker:)),
            for: .valueChanged
        )
        maxStartDateField.inputView = maxStartDatePicker
        if startingField.text == "Custom" {
            maxStartDateField.text = keychain.get(KeychainKeys.challengesFeedCustomMaxStartDateStr.rawValue)!
        } else {
            maxStartDateField.isHidden = true
        }
        
    }
    
    // exit keyboards / pickers
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    // MARK: picker methods
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView( _ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == creatorPicker {
            return C.challengesFeedCreatorFiltOptionsToMongoArgs.count
        } else if pickerView == sortByPicker {
            return C.challengesFeedSortByOptionsToQueryArgs.count
        } else {
            return C.challengesFeedStartingFiltOptions.count
        }
    }
    
    func pickerView( _ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == creatorPicker {
            return C.challengesFeedCreatorFiltOptions[row]
        } else if pickerView == sortByPicker {
            return C.challengesFeedSortByOptions[row]
        } else {
            return C.challengesFeedStartingFiltOptions[row]
        }
    }
    
    func pickerView( _ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == creatorPicker {
            return creatorField.text = C.challengesFeedCreatorFiltOptions[row]
        } else if pickerView == sortByPicker {
            return sortByField.text = C.challengesFeedSortByOptions[row]
        } else {
            let selection = C.challengesFeedStartingFiltOptions[row]
            if selection == "Custom" {
                minStartDateField.isHidden = false
                maxStartDateField.isHidden = false
            } else {
                minStartDateField.isHidden = true
                maxStartDateField.isHidden = true
            }
            return startingField.text = selection
        }
    }
    
    @objc private func minStartDateChanged(datePicker: UIDatePicker) {
        minStartDateField.text = dateFormatter.string(from: datePicker.date)
    }
    
    @objc private func maxStartDateChanged(datePicker: UIDatePicker) {
        maxStartDateField.text = dateFormatter.string(from: datePicker.date)
    }
    
    // MARK: Actions
    @IBAction func dismissVC(_ sender: Any) {
        var dateError = false
        
        let minStartDateStr = minStartDateField.text!
        dateError = dateError || (minStartDateStr.count == 0)
        let minStartDate: Date
        if minStartDateStr.count > 0 {
            minStartDate = dateFormatter.date(from: minStartDateStr)!
        } else {
            minStartDate = dateFormatter.date(from: "01/01/2000")!
        }
        
        let maxStartDateStr = maxStartDateField.text!
        dateError = dateError || (maxStartDateStr.count == 0)
        let maxStartDate: Date
        if maxStartDateStr.count > 0 {
            maxStartDate = dateFormatter.date(from: maxStartDateStr)!
        } else {
            maxStartDate = dateFormatter.date(from: "01/01/2000")!
        }
        
        dateError = dateError || (minStartDate > maxStartDate)
        
        if startingField.text == "Custom" && dateError {
            warningTextLabel.text = "Please correct dates errors 😊"
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3.0) {
                self.warningTextLabel.text = ""
            }
        } else {
            keychain.set(creatorField.text!, forKey: KeychainKeys.challengesFeedCreatorFilt.rawValue)
            keychain.set(sortByField.text!, forKey: KeychainKeys.challengesFeedSortBy.rawValue)
            keychain.set(startingField.text!, forKey: KeychainKeys.challengesFeedStartingFilt.rawValue)
            if startingField.text! == "Custom" {
                keychain.set(minStartDateStr, forKey: KeychainKeys.challengesFeedCustomMinStartDateStr.rawValue)
                keychain.set(maxStartDateStr, forKey: KeychainKeys.challengesFeedCustomMaxStartDateStr.rawValue)
            }
            
            Analytics.logEvent("challenges_filter_change", parameters: [
                "creator_field": creatorField.text!,
                "sort_by_field": sortByField.text!,
                "starting_field": startingField.text!,
                "custom_min_start_date": dateFormatter.string(from: minStartDate),
                "custom_max_start_date": dateFormatter.string(from: maxStartDate)
            ])
            
            DispatchQueue.main.async {
                NotificationCenter.default.post(
                    name: .didChangeFeedFilter ,
                    object: nil
                )
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
}
