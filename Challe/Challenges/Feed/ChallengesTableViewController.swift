//
//  ChallengesTableViewController.swift
//  Challe
//
//  Created by Rob Dearborn on 3/18/19.
//  Copyright © 2019 Challe. All rights reserved.
//

import UIKit

import AWSLambda
import Firebase
import Presentr
import StitchCore


class ChallengesTableViewController: UITableViewController, ChallengesTableViewCellDelegator {
    
    // MARK: Properties
    private var challenges = [Challenge]()
    
    private var creatorQueryArg = C.challengesTableDefaultCreatorQueryArg
    private var sortByQueryArg = C.challengesTableDefaultSortByQueryArg
    private var sortByQueryOrder = C.challengesTableDefaultSortByQueryOrder
    private var startingFilt = C.challengesTableDefaultStartingFilt
    private var minStartDate, maxStartDate: Date!
    private var queryString = ""
    private var followerQueryArg: String?
    private var completedQueryArg: Bool?
    
    private var vSpinner: UIView?
    private let tableQueue = DispatchQueue(label: "tableQueue")
    private var initialTableLoadComplete = false
    private var pauseLoading = true // flipped after initial load
    private var maxCreatedAt = Date()
    private var challengesPerPage = C.challengesPerPage
    private var onPage = 1

    // MARK: Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.showsVerticalScrollIndicator = false
        self.tableView.showsHorizontalScrollIndicator = false
        
        // challenge change listeners
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(onChallengeCreation(_:)),
            name: .didCreateChallenge,
            object: nil
        )
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(onChallengeDeletion(_:)),
            name: .didDeleteChallenge,
            object: nil
        )
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(onChallengeUpdate(_:)),
            name: .didUpdateChallenge,
            object: nil
        )
        
    }
    
    // initialize # of rows
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return challenges.count
    }
    
    // form cells
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "ChallengesTableViewCell"
        
        // create cell
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? ChallengesTableViewCell else {
            fatalError("The dequeued cell is not an instance of ChallengesTableViewCell.")
        }
        cell.delegate = self
        cell.selectionStyle = .none
        
        // load cell data
        let challenge = challenges[indexPath.row]
        cell.challenge = challenge
        
        cell.titleTextView.text = challenge.title
        cell.creatorTextButton.setAttributedTitle(challenge.getAttributedCreatorStr(), for: .normal)
        cell.descriptionTextLabel.text = challenge.description
        cell.followersTextLabel.text = challenge.getFollowersStr()
        cell.datesTextLabel.text = challenge.getDatesStr()
        cell.userIsFollowingButton.isHidden = !challenge.userIsFollower
        if challenge.userHasCompleted {
            cell.userIsFollowingButton.setImage(UIImage(named: "check_green"), for: .normal)
        } else {
            cell.userIsFollowingButton.setImage(UIImage(named: "check_light_grey"), for: .normal)
        }
        
        return cell
    }
    
    // reload and pagination
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let frameHeight = scrollView.frame.size.height
        let contentYoffset = scrollView.contentOffset.y
        let rowsHeight = scrollView.contentSize.height
        let distanceFromBottom = rowsHeight - contentYoffset
        
        // Reload when pulled down
        if !pauseLoading && contentYoffset < -50 {
            pauseLoading = true
            loadChallenges(reload: true)
        }
        
        // Load more when bottom of table reached
        if !pauseLoading && (rowsHeight > frameHeight) && (distanceFromBottom < frameHeight) {
            pauseLoading = true
            loadChallenges()
        }
    }
    
    // Selection
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewChallenge(challengeId: challenges[indexPath.row].id)
    }
    
    func updateFiltsAndReload(creatorQueryArg: String, sortByQueryArg: String, sortByQueryOrder: Int, startingFilt: String, queryString: String = "", followerQueryArg: String? = nil, completedQueryArg: Bool? = nil) {
        self.creatorQueryArg = creatorQueryArg
        self.sortByQueryArg = sortByQueryArg
        self.sortByQueryOrder = sortByQueryOrder
        self.startingFilt = startingFilt
        self.queryString = queryString
        self.followerQueryArg = followerQueryArg
        self.completedQueryArg = completedQueryArg
        
        pauseLoading = true
        loadChallenges(reload: true)
    }
    
    // MARK: Data loading
    private func loadChallenges(reload: Bool = false) {
        if vSpinner == nil || vSpinner?.superview == nil {
            vSpinner = showSpinner(onView: self.view)
        }
        
        setDates()
        
        if reload {
            self.onPage = 1
            self.maxCreatedAt = Date()
        }
        
        if queryString.count == 0 {
            var argsDoc: Document = [
                "maxCreatedAt": maxCreatedAt,
                "creator": creatorQueryArg,
                "sortBy": sortByQueryArg,
                "sortOrder": sortByQueryOrder,
                "minStartDate": minStartDate,
                "maxStartDate": maxStartDate,
            ]
            
            if let followerQueryArg = followerQueryArg {
                argsDoc["follower"] = followerQueryArg
                
                if let completedQueryArg = completedQueryArg {
                    argsDoc["completed"] = completedQueryArg
                }
                
            }
            
            stitchClient.callFunction(withName: "getChallenges", withArgs: [argsDoc, self.challengesPerPage, self.onPage]) { (result: StitchResult<[Document]>) in
                switch result {
                case .failure(let error):
                    print("Stitch challenges collection error: \(String(describing: error))")
                    if self.vSpinner != nil { self.removeSpinner(vSpinner: self.vSpinner!) }
                    if C.stitchAuthErrorDescriptions.contains(error.description) {
                        DispatchQueue.main.async {
                            logoutFlow(presentFrom: self)
                        }
                    }
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.0) {
                        self.pauseLoading = false
                    }
                case .success(let challengeDocs):
                    DispatchQueue.main.async {
                        let serialQueue = DispatchQueue(label: "serialQueue")
                        serialQueue.sync {
                            if reload {
                                self.challenges.removeAll()
                            }
                            
                            for challengeDoc in challengeDocs {
                                let challenge = Challenge(challengeDoc: challengeDoc)
                                self.challenges.append(challenge!)
                            }
                            self.tableView.reloadData()
                            if self.challenges.count == 0 {
                                self.tableView.setEmptyView(title: "No challenges to show 😢", message: "(yet!)")
                            }
                            else {
                                self.tableView.restore()
                            }
                            Analytics.logEvent("load_challenges", parameters: ["reload": reload, "on_page": self.onPage])
                            self.onPage += 1
                            if self.vSpinner != nil { self.removeSpinner(vSpinner: self.vSpinner!) }
                            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.0) {
                                self.pauseLoading = false
                            }
                        }
                    }
                }
            }
        } else { // stitch doesn't support text indices yet, so have to do via lambda
            let getChallengesURL = URL(string: "https://0r6psvjhfi.execute-api.us-east-1.amazonaws.com/prod/challenges/get-challenges")!
            var getChallengesRequest = URLRequest(url: getChallengesURL)
            getChallengesRequest.httpMethod = "GET"
            
            auth0CredentialsManager.credentials { error, credentials in
                guard error == nil, let credentials = credentials else {
                    print("Auth0 credentials error: \(String(describing: error))")
                    if self.vSpinner != nil { self.removeSpinner(vSpinner: self.vSpinner!) }
                    DispatchQueue.main.async {
                        logoutFlow(presentFrom: self)
                    }
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.0) {
                        self.pauseLoading = false
                    }
                    return
                }
                
                guard let token = credentials.accessToken else {
                    print("Auth0 token formation error")
                    if self.vSpinner != nil { self.removeSpinner(vSpinner: self.vSpinner!) }
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.0) {
                        self.pauseLoading = false
                    }
                    return
                }
                
                getChallengesRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
                
                let currentUserId = stitchClient.auth.currentUser!.id
                getChallengesRequest.setValue(currentUserId, forHTTPHeaderField: "current_user_id")
                
                getChallengesRequest.setValue("\(self.challengesPerPage)", forHTTPHeaderField: "n_per_page")
                getChallengesRequest.setValue("\(self.onPage)", forHTTPHeaderField: "page_number")
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                dateFormatter.timeZone = TimeZone.init(secondsFromGMT: 0)
                getChallengesRequest.setValue(dateFormatter.string(from: self.maxCreatedAt), forHTTPHeaderField: "max_created_at")
                getChallengesRequest.setValue(dateFormatter.string(from: self.minStartDate), forHTTPHeaderField: "min_start_date")
                getChallengesRequest.setValue(dateFormatter.string(from: self.maxStartDate), forHTTPHeaderField: "max_start_date")
                
                getChallengesRequest.setValue(self.creatorQueryArg, forHTTPHeaderField: "creator")
                getChallengesRequest.setValue(self.sortByQueryArg, forHTTPHeaderField: "sort_by")
                getChallengesRequest.setValue("\(self.sortByQueryOrder)", forHTTPHeaderField: "sort_order")
                getChallengesRequest.setValue(self.queryString, forHTTPHeaderField: "query_string")
                
                // NOTE: simultaneous follower and query constraints not yet supported by Mongo
                // since can't compound indexes cannot contain both text and id.
                // Leaving this here in case that's someday not the case
                if let followerQueryArg = self.followerQueryArg {
                    getChallengesRequest.setValue(followerQueryArg, forHTTPHeaderField: "follower")
                    
                    if let completedQueryArg = self.completedQueryArg {
                        getChallengesRequest.setValue("\(completedQueryArg)", forHTTPHeaderField: "completed")
                    }
                    
                }
                
                let getChallengesTask = URLSession.shared.dataTask(with: getChallengesRequest) {(data, response, error) in
                    guard let data = data else {
                        print("get-challenges-for-search error")
                        if self.vSpinner != nil { self.removeSpinner(vSpinner: self.vSpinner!) }
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.0) {
                            self.pauseLoading = false
                        }
                        return
                    }
                    
                    guard let challengeBSONs = try? Document(fromJSON: data) else {
                        print("error: get-challenges data couldn't be deserialized")
                        if self.vSpinner != nil { self.removeSpinner(vSpinner: self.vSpinner!) }
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.0) {
                            self.pauseLoading = false
                        }
                        return
                    }
                    
                    DispatchQueue.main.async {
                        let serialQueue = DispatchQueue(label: "serialQueue")
                        serialQueue.sync {
                            if reload {
                                self.challenges.removeAll()
                            }
                            for challengeBSON in challengeBSONs {
                                guard let challengeDoc = try? Document(fromJSON: "\(challengeBSON.value)") else {
                                    print("error: get-challenges data couldn't be formed into document")
                                    continue
                                }
                                guard let challenge = Challenge(challengeDoc: challengeDoc) else {
                                    ("error: get-challenges document couldn't be formed into challenge")
                                    continue
                                }
                                self.challenges.append(challenge)
                            }
                            self.tableView.reloadData()
                            if self.challenges.count == 0 {
                                self.tableView.setEmptyView(title: "No challenges to show 😢", message: "(yet!)")
                            }
                            else {
                                self.tableView.restore()
                            }
                            Analytics.logEvent("load_challenges", parameters: ["reload": reload, "on_page": self.onPage])
                            self.onPage += 1
                            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2.0) {
                                self.pauseLoading = false
                            }
                            if self.vSpinner != nil { self.removeSpinner(vSpinner: self.vSpinner!) }
                        }
                    }
                    
                }
                getChallengesTask.resume()
            }
        }
        
    }
    
    private func setDates() {
        var calendar = Calendar(identifier: .gregorian)
        calendar.timeZone = TimeZone.init(secondsFromGMT: 0)!
        let now = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        dateFormatter.timeZone = TimeZone.init(secondsFromGMT: 0)!
        
        switch startingFilt {
        case "All":
            minStartDate = dateFormatter.date(from: "01/01/2000")
            maxStartDate = dateFormatter.date(from: "01/01/3000")
        case "This month":
            let components: DateComponents = calendar.dateComponents([.year, .month], from: now)
            minStartDate = calendar.date(from: components)!
            
            var componentsToAdd = DateComponents()
            componentsToAdd.month = 1
            componentsToAdd.second = -1
            maxStartDate = calendar.date(byAdding: componentsToAdd, to: minStartDate)
        case "This week":
            let components: DateComponents = calendar.dateComponents([.yearForWeekOfYear, .weekOfYear], from: now)
            minStartDate = calendar.date(from: components)!
            
            var componentsToAdd = DateComponents()
            componentsToAdd.day = 7
            componentsToAdd.second = -1
            maxStartDate = calendar.date(byAdding: componentsToAdd, to: minStartDate)
        case "Today":
            let components: DateComponents = calendar.dateComponents([.year, .month, .day], from: now)
            minStartDate = calendar.date(from: components)
            
            var componentsToAdd = DateComponents()
            componentsToAdd.day = 1
            componentsToAdd.second = -1
            maxStartDate = calendar.date(byAdding: componentsToAdd, to: minStartDate)
        case "Tomorrow":
            let components: DateComponents = calendar.dateComponents([.year, .month, .day], from: now)
            let today = calendar.date(from: components)!
            
            minStartDate = calendar.date(byAdding: .day, value: 1, to: today)
            
            var componentsToAdd = DateComponents()
            componentsToAdd.day = 1
            componentsToAdd.second = -1
            maxStartDate = calendar.date(byAdding: componentsToAdd, to: minStartDate)
        case "Next 7 days":
            minStartDate = now
            maxStartDate = calendar.date(byAdding: .day, value: 7, to: now)
        case "Next week":
            let components: DateComponents = calendar.dateComponents([.yearForWeekOfYear, .weekOfYear], from: now)
            let startOfThisWeek = calendar.date(from: components)!
            
            minStartDate = calendar.date(byAdding: .day, value: 7, to: startOfThisWeek)
            
            var componentsToAdd = DateComponents()
            componentsToAdd.day = 7
            componentsToAdd.second = -1
            maxStartDate = calendar.date(byAdding: componentsToAdd, to: minStartDate)
        case "Next month":
            let components: DateComponents = calendar.dateComponents([.year, .month], from: now)
            let startOfThisMonth = calendar.date(from: components)!
            
            minStartDate = calendar.date(byAdding: .month, value: 1, to: startOfThisMonth)
            
            var componentsToAdd = DateComponents()
            componentsToAdd.month = 1
            componentsToAdd.second = -1
            maxStartDate = calendar.date(byAdding: componentsToAdd, to: minStartDate)
        default: // custom
            minStartDate = dateFormatter.date(from: keychain.get(KeychainKeys.challengesFeedCustomMinStartDateStr.rawValue)!)
            maxStartDate = dateFormatter.date(from: keychain.get(KeychainKeys.challengesFeedCustomMaxStartDateStr.rawValue)!)
            
            var componentsToAdd = DateComponents()
            componentsToAdd.day = 1
            componentsToAdd.second = -1
            maxStartDate = calendar.date(byAdding: componentsToAdd, to: maxStartDate)
        }
    }
    
    func viewChallenge(challengeId: ObjectId) {
        if !pauseLoading { // avoid tapping on changing indices
            let presenter = Presentr(presentationType: .fullScreen)
            
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "challengeViewController") as? ChallengeViewController {
                vc.challengeId = challengeId
                customPresentViewController(presenter, viewController: vc, animated: true)
            }
        }
    }
    
    func viewUserProfile(challeUserId: String) {
        if !pauseLoading {
            let presenter = Presentr(presentationType: .fullScreen)
            
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "otherProfileViewController") as? OtherProfileViewController {
                vc.challeUserId = challeUserId
                customPresentViewController(presenter, viewController: vc, animated: true)
            }
        }
    }
    
    // MARK: Listeners
    @objc private func onChallengeCreation(_ notification:Notification) {
        guard let ownUserId = stitchClient.auth.currentUser?.id else {
            print("challengesTableViewController error: user not authenticated")
            return
        }
        
        if ownUserId == creatorQueryArg || ownUserId == followerQueryArg {
            return // challenges table is part of own profile and will be updated there
        }
        
        DispatchQueue.main.async {
            self.pauseLoading = true
            self.loadChallenges(reload: true)
        }
    }
    
    @objc private func onChallengeDeletion(_ notification:Notification) {
        DispatchQueue.main.async {
            let userInfo = notification.userInfo as! [String: ObjectId]
            let idToRemove = userInfo["id"]
            
            for (idx, challenge) in self.challenges.enumerated() {
                if challenge.id == idToRemove {
                    self.challenges.remove(at: idx)
                    let indexPath = IndexPath(row: idx, section: 0)
                    self.tableView.deleteRows(at: [indexPath], with: .fade)
                }
            }
            if self.challenges.count == 0 {
                self.tableView.setEmptyView(title: "No challenges to show 😢", message: "(yet!)")
            }
            else {
                self.tableView.restore()
            }
        }
    }
    
    // Unfollow listener
    @objc private func onChallengeUpdate(_ notification:Notification) {
        guard let ownUserId = stitchClient.auth.currentUser?.id else {
            print("challengesTableViewController error: user not authenticated")
            return
        }
        
        if ownUserId == creatorQueryArg || ownUserId == followerQueryArg {
            return // challenges table is part of own profile and will be updated there            
        }
        
        DispatchQueue.main.async {
            let userInfo = notification.userInfo as! [String: Challenge]
            let newChallenge = userInfo["challenge"]
            let idToEdit = newChallenge?.id
            
            for (idx, challenge) in self.challenges.enumerated() {
                if challenge.id == idToEdit {
                    self.challenges[idx] = newChallenge!
                    let indexPath = IndexPath(row: idx, section: 0)
                    self.tableView.reloadRows(at: [indexPath], with: UITableView.RowAnimation.none)
                }
            }
        }
    }

}
