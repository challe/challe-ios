//
//  ChallengesTableViewCell.swift
//  Challe
//
//  Created by Rob Dearborn on 3/12/19.
//  Copyright © 2019 Challe. All rights reserved.
//

import UIKit

import StitchCore


protocol ChallengesTableViewCellDelegator {
    func viewChallenge(challengeId: ObjectId)
    
    func viewUserProfile(challeUserId: String)
}


class ChallengesTableViewCell: UITableViewCell {
    
    // MARK: Properties
    @IBOutlet weak var titleTextView: UITextView!
    @IBOutlet weak var titleTextButton: UIButton!
    @IBOutlet weak var creatorTextButton: UIButton!
    @IBOutlet weak var descriptionTextLabel: UILabel!
    @IBOutlet weak var followersTextLabel: UILabel!
    @IBOutlet weak var datesTextLabel: UILabel!
    @IBOutlet weak var userIsFollowingButton: UIButton!
    
    var delegate: ChallengesTableViewController!
    var challenge: Challenge!
    
    // MARK: Overrides
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: Button actions
    @IBAction func titleTextButtonAction(_ sender: UIButton) {
        self.delegate.viewChallenge(challengeId: challenge.id)
    }
    
    
    @IBAction func creatorTextButtonAction(_ sender: UIButton) {
        self.delegate.viewUserProfile(challeUserId: challenge.creatorId)
    }
}
