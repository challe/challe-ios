//
//  NUXViewController.swift
//  Challe
//
//  Created by Rob Dearborn on 3/3/19.
//  Copyright © 2019 Challe. All rights reserved.
//
// Copied from:
// https://stackoverflow.com/questions/18398796/uipageviewcontroller-and-storyboard
// https://stackoverflow.com/questions/28014852/transition-pagecurl-to-scroll-with-uipageviewcontroller

import UIKit

import Firebase


class NUXViewController: UIPageViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    // MARK: Properties
    private var pages = [UIViewController]()
    
    // MARK: Disable page flip swip scroll animation
    required init?(coder: NSCoder){
        super.init(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
    }
    
    // MARK: Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        self.dataSource = self
        
        let p1: UIViewController! = storyboard?.instantiateViewController(withIdentifier: "nuxPage1")
        let p2: UIViewController! = storyboard?.instantiateViewController(withIdentifier: "nuxPage2")
        let p3: UIViewController! = storyboard?.instantiateViewController(withIdentifier: "nuxPage3")
        
        pages.append(p1)
        pages.append(p2)
        pages.append(p3)
        
        setViewControllers([p1], direction: UIPageViewController.NavigationDirection.forward, animated: false, completion: nil)
        
        Analytics.logEvent("begin_nux", parameters: nil)
    }
    
    // MARK: Swipe scrolling methods
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController)-> UIViewController? {
        
        let cur = pages.firstIndex(of: viewController)!
        
        // disable circular scrolling
        if cur == 0 { return nil }
        
        let prev = abs((cur - 1) % pages.count)
        return pages[prev]
        
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController)-> UIViewController? {
        
        let cur = pages.firstIndex(of: viewController)!
        var nxt: Int
        
        // if swiping from last page
        if cur == (pages.count - 1) {
            // mark nux complete
            keychain.set(true, forKey: KeychainKeys.nuxComplete.rawValue)
            Analytics.logEvent("complete_nux", parameters: nil)
            // redirect to app, launch login
            let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
            if let authVC = mainStoryboard.instantiateViewController(withIdentifier: "authViewController") as? AuthViewController {
                self.present(authVC, animated: false, completion: nil)
            }
            return nil
        } else {
            nxt = abs((cur + 1) % pages.count)
        }
        
        return pages[nxt]
    }
    
    func presentationIndex(for pageViewController: UIPageViewController)-> Int {
        return pages.count
    }
}
