//
//  NotificationNames.swift
//  Challe
//
//  Created by Rob Dearborn on 3/12/19.
//  Copyright © 2019 Challe. All rights reserved.
//

import Foundation


extension Notification.Name {
    static let fireLambdaPings = Notification.Name("fireLambdaPings")
    
    // Feed Filter - Feed Comms
    static let didChangeFeedFilter = Notification.Name("didChangeFeedFilter")
    
    // View Challenge - Feed Comms
    static let didCreateChallenge = Notification.Name("didCreateChallenge")
    static let didDeleteChallenge = Notification.Name("didDeleteChallenge")
    static let didUpdateChallenge = Notification.Name("didUpdateChallenge")
    
    // User profile
    static let didUpdateOwnProfile = Notification.Name("didChangeOwnProfile")
    static let didUpdateOwnFollowers = Notification.Name("didUpdateOwnFollowers")
    static let didUpdateOwnFollowing = Notification.Name("didUpdateOwnFollowing")
    static let didHandleFollowRequest = Notification.Name("didHandleFollowRequest")
    static let didUpdateUser = Notification.Name("didUpdateUser")
}
