//
//  KeychainKeys.swift
//  Challe
//
//  Created by Rob Dearborn on 3/13/19.
//  Copyright © 2019 Challe. All rights reserved.
//

import Foundation


enum KeychainKeys: String {
    
    // Login
    case auth0InitialScreen
    
    // NUX
    case nuxComplete
    
    // User profile
    case username
    case userFirstName
    case userLastName
    case userImageURL
    
    // Feed filtering
    case challengesFeedStartingFilt
    case challengesFeedCustomMinStartDateStr
    case challengesFeedCustomMaxStartDateStr
    case challengesFeedCreatorFilt
    case challengesFeedSortBy

}
