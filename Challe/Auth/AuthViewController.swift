//
//  AuthViewController.swift
//  Challe
//
//  Created by Rob Dearborn on 3/20/19.
//  Copyright © 2019 Challe. All rights reserved.
//

import SimpleKeychain
import UIKit

import Auth0
import Firebase
import JWTDecode
import Lock
import Presentr
import StitchCore


class AuthViewController: UIViewController {
    
    // MARK: Properties
    enum CustomFieldError: Error {
        case validationError(String)
    }
    
    // MARK: Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        // handle auth0's dismissing self and reloading
        // this view before executing onAuth block
        if !V.authInProgress {
            registerForPushNotifications()
            V.authInProgress = true
            loginFlow()
            
        } else {
            V.authInProgress = false
        }
    }

    func loginFlow() -> Void {
        // Auth0 login and signup
        let initialScreenStr = keychain.get(KeychainKeys.auth0InitialScreen.rawValue) ?? "signup"
        
        var initialScreen = DatabaseScreen.login
        if initialScreenStr == "signup" {
            initialScreen = DatabaseScreen.signup
        } else if initialScreenStr == "resetPassword" {
            initialScreen = DatabaseScreen.resetPassword
        }
        
        Lock
            .classic()
            .withOptions {
                $0.initialScreen = initialScreen
                $0.oidcConformant = true
                $0.audience = C.auth0Audience
                $0.scope = C.auth0Scope
                $0.customSignupFields = [
                    CustomTextField(
                        name: "first_name",
                        placeholder: "your first name",
                        icon: LazyImage(name: "ic_person",
                                        bundle: Lock.bundle),
                        validation: { (firstName: String?) -> Error? in
                            var err: CustomFieldError?
                            if let firstName = firstName {
                                if !firstName.isLetter(additionalChars: " \\-’'") {
                                    err = CustomFieldError.validationError("First name should contain only letters, spaces, apostrophes, and hyphens.")
                                }
                            } else {
                                err = CustomFieldError.validationError("Please enter a first name.")
                            }
                            
                            return err
                        }
                    ),
                    CustomTextField(
                        name: "last_name",
                        placeholder: "your last name",
                        icon: LazyImage(name: "ic_person",
                                        bundle: Lock.bundle),
                        validation: { (firstName: String?) -> Error? in
                            var err: CustomFieldError?
                            if let firstName = firstName {
                                if !firstName.isLetter(additionalChars: " \\-’'") {
                                    err = CustomFieldError.validationError("Last name should contain only letters, spaces, apostrophes, and hyphens.")
                                }
                            } else {
                                err = CustomFieldError.validationError("Please enter a last name.")
                            }
                            
                            return err
                        }
                    ),
                    CustomTextField(
                        name: "phone_number",
                        placeholder: "your phone number",
                        icon: LazyImage(name: "ic_phone",
                                        bundle: Lock.bundle),
                        keyboardType: UIKeyboardType.phonePad,
                        validation: { (firstName: String?) -> Error? in
                            var err: CustomFieldError?
                            if let firstName = firstName {
                                if !firstName.isNumeric(additionalChars: "+") {
                                    err = CustomFieldError.validationError("Phone number should contain only digits.")
                                }
                            } else {
                                err = CustomFieldError.validationError("Please enter a phone number.")
                            }
                            
                            return err
                        }
                    )
                ]
                $0.termsOfService = "http://challe.co/terms/"
                $0.privacyPolicy = "http://challe.co/privacy-policy/"
            }
            .withStyle {
                $0.title = "Challe"
                $0.logo = LazyImage(name: "AuthLogo")
                $0.primaryColor = UIColor(named: "ChalleRed") ?? UIColor.red
                $0.hideTitle = true
                $0.hideButtonTitle = false
                $0.headerColor = UIColor(named: "ChalleWhite") ?? UIColor.red
            }
            .onError {
                print("Auth0 failed with \($0)")
            }
            .onAuth { credentials in
                // Henceforth default to Auth0 login screen, not sign up
                keychain.set("login", forKey: KeychainKeys.auth0InitialScreen.rawValue)
                
                // store credentials
                print("Secured credentials: \(credentials)")
                _ = auth0CredentialsManager.store(credentials: credentials)
                
                // populate user profile
                guard let jwt = try? decode(jwt: credentials.idToken ?? "") else {
                    print("Auth0 jwt decoding error")
                    // force new login
                    logout()
                    self.loginFlow()
                    return
                }
                
                guard let auth0_id = jwt.claim(name: "sub").string, let email = jwt.claim(name: "email").string, let username = jwt.claim(name: "https://www.challe.co/username").string, let phoneNumber = jwt.claim(name: "https://www.challe.co/phone_number").string, let firstName = jwt.claim(name: "https://www.challe.co/first_name").string, let lastName = jwt.claim(name: "https://www.challe.co/last_name").string, let country = jwt.claim(name: "https://www.challe.co/country").string, let timezone = jwt.claim(name: "https://www.challe.co/timezone").string, let imageURL = jwt.claim(name: "https://www.challe.co/picture").string ?? jwt.claim(name: "picture").string, let emailVerified = jwt.claim(name: "email_verified").integer else {
                    print("Auth0 jwt string conversion error")
                    // force new login
                    logout()
                    self.loginFlow()
                    return
                }
                
                // Stitch login
                guard let stitchCred = credentials.accessToken else {
                    print("Stitch credential formation error")
                    // force new login
                    logout()
                    self.loginFlow()
                    return
                }
                stitchClient.auth.login(withCredential: CustomCredential(withToken: stitchCred)) { result in
                    switch result {
                    case .failure(let error):
                        print("Stitch auth error: \(error)")
                        // force new login
                        logout()
                        self.loginFlow()
                    case .success(let user):
                        print("Logged into Stitch as user: \(user.id)")
                        
                        // Set user defaults
                        
                        keychain.set(username, forKey: KeychainKeys.username.rawValue)
                        keychain.set(firstName.utf8EncodedString(), forKey: KeychainKeys.userFirstName.rawValue)
                        keychain.set(lastName.utf8EncodedString(), forKey: KeychainKeys.userLastName.rawValue)
                        keychain.set(imageURL, forKey: KeychainKeys.userImageURL.rawValue)
                        
                        // Create user notification settings record in MongoDB
                        // Deduplication logic is in Mongo function
                        
                        let receiveDailyChallengeDigestsAtUTCHour = hourTimezoneConversion(hourOfDay: C.receiveDailyDigestsAtLocalHourDefault, firstTimezone: TimeZone(identifier: timezone)!, secondTimezone: TimeZone(abbreviation: "UTC")!)
                        let receiveChallengeRemindersAtUTCHour = hourTimezoneConversion(hourOfDay: C.receiveChallengeRemindersAtLocalHourDefault, firstTimezone: TimeZone(identifier: timezone)!, secondTimezone: TimeZone(abbreviation: "UTC")!)
                        
                        let newUNSDoc: Document = [
                            "created_at": Date(),
                            "creator_id": user.id,
                            "apn_token": V.apnToken,
                            "fcm_token": V.fcmToken,
                            "timezone": timezone,
                            "receive_daily_challenge_digests": C.receiveDailyDigestsDefault,
                            "receive_daily_challenge_digests_at_utc_hour": Int64(exactly: receiveDailyChallengeDigestsAtUTCHour)!,
                            "receive_challenge_reminders_default": C.receiveChallengeRemindersDefault,
                            "receive_challenge_reminders_at_utc_hour_default": Int64(exactly: receiveChallengeRemindersAtUTCHour)!,
                            "receive_new_challenge_joiner_notifs": C.receiveNewChallengeJoinerNotifsDefault,
                            "receive_new_challenge_comment_notifs": C.receiveNewChallengeCommentNotifsDefault,
                            "receive_new_profile_follower_notifs": C.receiveNewProfileFollowerNotifsDefault,
                            "receive_profile_follow_request_notifs": C.receiveProfileFollowRequestNotifsDefault,
                            "receive_profile_follow_request_approved_notifs": C.receiveProfileFollowRequestApprovedNotifsDefault,
                            "receive_followed_user_created_challenge_notifs": C.receiveFollowedUserCreatedChallengeNotifsDefault,
                            "receive_followed_user_commented_on_joined_challenge_notifs": C.receiveFollowedUserCommentedOnJoinedChallengeNotifsDefault,
                            "is_deleted": false
                        ]
                        stitchClient.callFunction(withName: "createUserNotificationSettings", withArgs: [newUNSDoc]) {
                            (result: StitchResult<String>) in
                            switch result {
                            case .failure(let error):
                                print("Stitch create user notification settings error: \(error)")
                            case .success(let uns_id):
                                print("Successfully created user notification settings in Stitch: \(uns_id)")
                            }
                        }
                        
                        // Create User Record in MongoDB if doesn't exist
                        // Deduplication logic is in Mongo function
                        let newUserDoc: Document = [
                            "_id": ObjectId.init(user.id)!,
                            "created_at": Date(),
                            "updated_at": Date(),
                            "is_deleted": false,
                            "creator_id": user.id,
                            "auth0_id": auth0_id,
                            "email": email,
                            "username": username,
                            "phone_number": phoneNumber,
                            "first_name": firstName.utf8EncodedString(),
                            "last_name": lastName.utf8EncodedString(),
                            "country": country,
                            "timezone": timezone,
                            "image_url": imageURL,
                            "is_private": C.profilePrivacyDefault,
                            "bio": "",
                            "n_followers": 0 as Int64,
                            "n_following": 0 as Int64,
                            "n_flags": 0 as Int64
                        ]
                        
                        stitchClient.callFunction(withName: "createUser", withArgs: [newUserDoc]) { (result: StitchResult<String>) in
                            switch result {
                            case .failure(let error):
                                print("Stitch user creation error: \(error)")
                                // force new login
                                logout()
                                self.loginFlow()
                            case .success(let user_id):
                                print("Stitch created user with ID: \(user_id)")
                                DispatchQueue.main.async {
                                    if user_id.isMongoId() {
                                        Analytics.logEvent(AnalyticsEventSignUp, parameters: ["method": "auth0"])
                                    }
                                    
                                    let presenter = Presentr(presentationType: .fullScreen)
                                    
                                    NotificationCenter.default.post(
                                        name: .fireLambdaPings,
                                        object: nil
                                    )
                                    
                                    if emailVerified == 1 {
                                        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "challengesFeedViewController") as? ChallengesFeedViewController {
                                            Analytics.logEvent(AnalyticsEventLogin, parameters: ["method": "auth0"])
                                            self.customPresentViewController(presenter, viewController: vc, animated: false)
                                        }
                                    } else {
                                        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "verifyAccountViewController") as? VerifyAccountViewController {
                                            DispatchQueue.main.async {
                                                vc.titleTextLabel.text = "Thanks for joining Challe! 🙌"
                                            }
                                            self.customPresentViewController(presenter, viewController: vc, animated: false)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            .present(from: self)
    }

}

func userNeedsLogin() -> Bool {
    // will return false in case of expired access token amd revoked (but existing) refresh token
    // this case will be caught by appdelegate pings
    return !auth0CredentialsManager.hasValid() || !stitchClient.auth.isLoggedIn
}


func logoutFlow(presentFrom: UIViewController) -> Void {
    print("logout flow triggered! Presenting from \(presentFrom)")
    Analytics.logEvent("logout_flow", parameters: nil)
    logout()
    
    let presenter = Presentr(presentationType: .fullScreen)
    
    if let vc = presentFrom.storyboard?.instantiateViewController(withIdentifier: "authViewController") as? AuthViewController {
        presentFrom.customPresentViewController(presenter, viewController: vc, animated: true)
    }
}


func logout() {
    // Send firebase event
    Analytics.logEvent("logout", parameters: nil)
    
    // Clear APN token
    updateAPNToken(newToken: "")
    updateFCMToken(newToken: "")
    
    // Clear Keychain
    keychain.delete(KeychainKeys.username.rawValue)
    keychain.delete(KeychainKeys.userFirstName.rawValue)
    keychain.delete(KeychainKeys.userLastName.rawValue)
    keychain.delete(KeychainKeys.userImageURL.rawValue)
    
    // clear Auth0 credentials
    _ = auth0CredentialsManager.clear()
    
    // Stitch logout
    stitchClient.auth.logout{ result in
        switch result {
        case .failure(let error):
            print("Stitch logout error: \(error)")
        case .success(_):
            print("Successful Stitch logout")
        }
    }
}


func forceLogin(presentFrom: UIViewController, completion: (() -> Void)? = nil) {
    Analytics.logEvent("force_login", parameters: nil)
    Lock
        .classic()
        .withOptions {
            $0.initialScreen = .login
            $0.oidcConformant = true
            $0.audience = C.auth0Audience
            $0.scope = C.auth0Scope
            $0.allow = [.Login]
            $0.closable = true
        }
        .withStyle {
            $0.title = "Challe"
            $0.logo = LazyImage(name: "AuthLogo")
            $0.primaryColor = UIColor(named: "ChalleRed") ?? UIColor.red
            $0.hideTitle = true
            $0.hideButtonTitle = false
            $0.headerColor = UIColor(named: "ChalleWhite") ?? UIColor.red
        }
        .onError {
            print("Auth0 failed with \($0)")
        }
        .onAuth { credentials in
            if let completion = completion {
                completion()
            }
        }
        .present(from: presentFrom)
}


func resendVerificationEmail(auth0AccessToken: String, auth0UserId: String) {
    Analytics.logEvent("request_resend_verification_email", parameters: nil)
    
    let resendVerificationEmailUrl = URL(string: "https://0r6psvjhfi.execute-api.us-east-1.amazonaws.com/prod/users/resend-verification-email")!
    var resendVerificationEmailRequest = URLRequest(url: resendVerificationEmailUrl)
    resendVerificationEmailRequest.httpMethod = "POST"
        
    resendVerificationEmailRequest.setValue("Bearer \(auth0AccessToken)", forHTTPHeaderField: "Authorization")
    resendVerificationEmailRequest.setValue(auth0UserId, forHTTPHeaderField: "auth0_user_id")
    
    let resendVerificationEmailTask = URLSession.shared.dataTask(with: resendVerificationEmailRequest) {(data, response, error) in
        guard let data = data else {
            print("resend-verification-email error")
            return
        }
        
        guard let jsonData = try? JSONSerialization.jsonObject(with: data, options: []) else {
            print("error: resend-verification-email data couldn't be deserialized")
            return
        }
        
        guard let dataDict = jsonData as? [String: Any] else {
            print("error: deserialized resend-verification-email data couldn't be formed into dict")
            return
        }
        
        guard let emailTriggered = dataDict["email_triggered"] as? Bool else {
            print("error: bool email_triggered not returned by resend-verification-email")
            return
        }
    }
    print("issuing resend verification email request...")
    resendVerificationEmailTask.resume()
}
