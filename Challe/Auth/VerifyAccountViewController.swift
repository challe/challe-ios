//
//  VerifyAccountViewController.swift
//  Challe
//
//  Created by Rob Dearborn on 3/20/19.
//  Copyright © 2019 Challe. All rights reserved.
//

import UIKit

import JWTDecode


class VerifyAccountViewController: UIViewController {
    
    // MARK: Properties
    @IBOutlet weak var titleTextLabel: UILabel!
    @IBOutlet weak var instructionsTextView: UITextView!
    @IBOutlet weak var loginButton: UIButton!
    
    
    private var auth0AccessToken: String?
    private var auth0UserId: String?
    
    // MARK: Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        
        auth0CredentialsManager.credentials { error, credentials in
            guard let credentials = credentials else {
                print("Auth0 credentials error: \(String(describing: error))")
                logout()
                return
            }
            
            guard let jwt = try? decode(jwt: credentials.idToken ?? "") else {
                print("Auth0 jwt decoding error")
                logout()
                return
            }
            
            self.auth0AccessToken = credentials.accessToken
            self.auth0UserId = jwt.claim(name: "sub").string
            logout() // prevent re-open to skip verification
        }
    }
    
    // MARK: Button actions
    @IBAction func loginButtonAction(_ sender: UIButton) {
        logoutFlow(presentFrom: self)
    }
    
    @IBAction func resendVerificationButton(_ sender: Any) {
        if let auth0AccessToken = auth0AccessToken, let auth0UserId = auth0UserId {
            resendVerificationEmail(auth0AccessToken: auth0AccessToken, auth0UserId: auth0UserId)
        }
    }
    
    
}
