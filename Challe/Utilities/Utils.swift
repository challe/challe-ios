//
//  Utils.swift
//  Challe
//
//  Created by Rob Dearborn on 3/12/19.
//  Copyright © 2019 Challe. All rights reserved.
//

import UIKit

// https://stackoverflow.com/questions/27182023/getting-the-difference-between-two-nsdates-in-months-days-hours-minutes-seconds
extension Date {
    /// Returns the amount of years from another date
    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
    /// Returns the amount of months from another date
    func months(from date: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }
    /// Returns the amount of weeks from another date
    func weeks(from date: Date) -> Int {
        return Calendar.current.dateComponents([.weekOfMonth], from: date, to: self).weekOfMonth ?? 0
    }
    /// Returns the amount of days from another date
    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }
    /// Returns the amount of hours from another date
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    /// Returns the amount of minutes from another date
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    /// Returns the amount of seconds from another date
    func seconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
    }
    /// Returns the a custom time interval description from another date
    func offset(from date: Date) -> String {
        if years(from: date)   > 0 { return "\(years(from: date))y"   }
        if months(from: date)  > 0 { return "\(months(from: date))M"  }
        if weeks(from: date)   > 0 { return "\(weeks(from: date))w"   }
        if days(from: date)    > 0 { return "\(days(from: date))d"    }
        if hours(from: date)   > 0 { return "\(hours(from: date))h"   }
        if minutes(from: date) > 0 { return "\(minutes(from: date))m" }
        if seconds(from: date) > 0 { return "\(seconds(from: date))s" }
        return ""
    }
}


// https://stackoverflow.com/questions/27338573/rounding-a-double-value-to-x-number-of-decimal-places-in-swift
extension Double {
    
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
    
}

// https://stackoverflow.com/questions/37087325/how-to-convert-string-to-unicodeutf-8-string-in-swift
extension String {
    
    func utf8DecodedString()-> String {
        let data = self.data(using: .utf8)
        if let message = String(data: data!, encoding: .nonLossyASCII){
            return message
        }
        return ""
    }
    
    func utf8EncodedString()-> String {
        let messageData = self.data(using: .nonLossyASCII)
        let text = String(data: messageData!, encoding: .utf8)
        return text!
    }
    
    func isAlphanumeric(additionalChars: String = "") -> Bool {
        let regexStr = "[^a-zA-Z0-9\(additionalChars)]"
        return self.range(of: regexStr, options: .regularExpression) == nil && self != ""
    }
    
    func isNumeric(additionalChars: String = "") -> Bool {
        let regexStr = "[^0-9\(additionalChars)]"
        return self.range(of: regexStr, options: .regularExpression) == nil && self != ""
    }
    
    func isLetter(additionalChars: String = "") -> Bool {
        let regexStr = "[^a-zA-Z\(additionalChars)]"
        return self.range(of: regexStr, options: .regularExpression) == nil && self != ""
    }
    
    func isLowercaseLetter(additionalChars: String = "") -> Bool {
        let regexStr = "[^a-z\(additionalChars)]"
        return self.range(of: regexStr, options: .regularExpression) == nil && self != ""
    }
    
    func isUppercaseLetter(additionalChars: String = "") -> Bool {
        let regexStr = "[^A-Z\(additionalChars)]"
        return self.range(of: regexStr, options: .regularExpression) == nil && self != ""
    }
    
    func isMongoId() -> Bool {
        let regexStr = "[^0123456789abcdef]"
        return self.count == 24 && self.range(of: regexStr, options: .regularExpression) == nil && self != ""
    }
    
}

// https://stackoverflow.com/questions/43256005/swift-ios-reduce-image-size-before-upload
extension UIImage {
    
    func resizeWithPercent(percentage: CGFloat) -> UIImage? {
        let imageView = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: size.width * percentage, height: size.height * percentage)))
        imageView.contentMode = .scaleAspectFit
        imageView.image = self
        
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
        
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        
        imageView.layer.render(in: context)
        
        guard let result = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
        
        UIGraphicsEndImageContext()
        
        return result
    }
    
    func resizeWithWidth(width: CGFloat) -> UIImage? {
        let imageView = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: width, height: CGFloat(ceil(width/size.width * size.height)))))
        imageView.contentMode = .scaleAspectFit
        imageView.image = self
        
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
        
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        
        imageView.layer.render(in: context)
        
        guard let result = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
        
        UIGraphicsEndImageContext()
        
        return result
    }
    
}


// https://stackoverflow.com/questions/24231680/loading-downloading-image-from-url-on-swift
extension UIImageView {
    
    func setImage(from url: URL, withPlaceholder placeholder: UIImage? = nil) {
        self.image = placeholder
        URLSession.shared.dataTask(with: url) {(data, _, _) in
            if let data = data {
                let image = UIImage(data: data)
                DispatchQueue.main.async {
                    self.image = image
                }
            }
            }.resume()
    }
    
}

// https://medium.com/@mtssonmez/handle-empty-tableview-in-swift-4-ios-11-23635d108409
extension UITableView {
    func setEmptyView(title: String, message: String) {
        let emptyView = UIView(frame: CGRect(x: self.center.x, y: self.center.y, width: self.bounds.size.width, height: self.bounds.size.height))
        let titleLabel = UILabel()
        let messageLabel = UILabel()
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        messageLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.textColor = UIColor(named: "ChalleBlack") ?? UIColor.darkGray
        titleLabel.font = UIFont(name: "HelveticaNeue-Regular", size: 17)
        messageLabel.textColor = UIColor.lightGray
        messageLabel.font = UIFont(name: "HelveticaNeue-Regular", size: 16)
        emptyView.addSubview(titleLabel)
        emptyView.addSubview(messageLabel)
        titleLabel.centerYAnchor.constraint(equalTo: emptyView.centerYAnchor).isActive = true
        titleLabel.centerXAnchor.constraint(equalTo: emptyView.centerXAnchor).isActive = true
        messageLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 20).isActive = true
        messageLabel.leftAnchor.constraint(equalTo: emptyView.leftAnchor, constant: 20).isActive = true
        messageLabel.rightAnchor.constraint(equalTo: emptyView.rightAnchor, constant: -20).isActive = true
        titleLabel.text = title
        messageLabel.text = message
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        // The only tricky part is here:
        self.backgroundView = emptyView
        self.separatorStyle = .none
    }
    
    func restore() {
        self.backgroundView = nil
        self.separatorStyle = .singleLine
    }
}

extension UIView {
    var firstResponder: UIView? {
        guard !isFirstResponder else { return self }
        
        for subview in subviews {
            if let firstResponder = subview.firstResponder {
                return firstResponder
            }
        }
        
        return nil
    }
}


extension UIViewController {
    // https://gist.github.com/db0company/369bfa43cb84b145dfd8
    func topMostViewController() -> UIViewController {
        
        if let presented = self.presentedViewController {
            return presented.topMostViewController()
        }
        
        if let navigation = self as? UINavigationController {
            return navigation.visibleViewController?.topMostViewController() ?? navigation
        }
        
        if let tab = self as? UITabBarController {
            return tab.selectedViewController?.topMostViewController() ?? tab
        }
        
        return self
    }
    
    // http://brainwashinc.com/2017/07/21/loading-activity-indicator-ios-swift/
    func showSpinner(onView: UIView) -> UIView {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = nil
        let ai = UIActivityIndicatorView.init(style: .gray)
        ai.startAnimating()
        ai.center = spinnerView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
        }
        
        return spinnerView
    }
    
    func removeSpinner(vSpinner: UIView) {
        DispatchQueue.main.async {
            vSpinner.removeFromSuperview()
        }
    }
}

// https://stackoverflow.com/questions/30480672/how-to-convert-a-json-string-to-a-dictionary
func jsonToDictionary(text: String?) -> [String: Any]? {
    if let text = text, let data = text.data(using: .utf8) {
        do {
            return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
        } catch {
            print(error.localizedDescription)
        }
    }
    return nil
}

// https://stackoverflow.com/questions/26913799/remove-println-for-release-version-ios-swift
func print(_ items: Any..., separator: String = " ", terminator: String = "\n") {
    #if DEBUG
    items.forEach {
        Swift.print($0, separator: separator, terminator: terminator)
    }
    #endif
}


func toFriendlyStr(int: Int64) -> String {
    if int < 1000 {
        return "\(int)"
    } else if int < 999500 {
        var inThousands = Double(int) / 1e3
        inThousands = inThousands.rounded(toPlaces: 1)
        return "\(inThousands)k"
    } else if int < 999500000 {
        var inMillions = Double(int) / 1e6
        inMillions = inMillions.rounded(toPlaces: 1)
        return "\(inMillions)M"
    } else {
        var inBillions = Double(int) / 1e9
        inBillions = inBillions.rounded(toPlaces: 1)
        return "\(inBillions)B"
    }
}

func hourTimezoneConversion(hourOfDay: Int, firstTimezone: TimeZone, secondTimezone: TimeZone) -> Int {
    
    var firstCal = Calendar(identifier: .gregorian)
    firstCal.timeZone = firstTimezone
    
    var secondCal = Calendar(identifier: .gregorian)
    secondCal.timeZone = secondTimezone
    
    let now = Date() // this should account for DST in real time
    var components: DateComponents = firstCal.dateComponents([.year, .month, .day], from: now)
    components.hour = hourOfDay
    let date = firstCal.date(from: components)!
    
    return secondCal.component(.hour, from: date)
    
}
