//
//  CreateSubChallengeTableViewCell
//  Challe
//
//  Created by Rob Dearborn on 2/27/19.
//  Copyright © 2019 Challe. All rights reserved.
//

import UIKit


class CreateSubChallengeTableViewCell: UITableViewCell, UITextFieldDelegate, UITextViewDelegate {
    
    // MARK: Properties
    @IBOutlet weak var subChallengeDateField: KeyboardResponsiveTextField!
    @IBOutlet weak var subChallengeDescField: KeyboardResponsiveTextView!
    
    private let dateFormatter = DateFormatter()
    private var datePicker: UIDatePicker?
    private var syncDateChange: ((String) -> Void)?
    private var syncDescChange: ((String) -> Void)?
    private var onInputDone: (() -> Void)?
    
    // MARK: Overrides
    override func awakeFromNib() {
        super.awakeFromNib()
        
        dateFormatter.dateFormat = "MM/dd/yyyy"
        dateFormatter.timeZone = TimeZone.init(secondsFromGMT: 0)
        
        datePicker = UIDatePicker()
        datePicker?.timeZone = TimeZone.init(secondsFromGMT: 0)
        datePicker?.datePickerMode = .date
        datePicker?.addTarget(
            self,
            action: #selector(self.dateChanged(datePicker:)),
            for: .valueChanged
        )
        subChallengeDateField.delegate = self
        subChallengeDateField.inputView = datePicker
        
        subChallengeDescField.delegate = self
        subChallengeDescField.layer.borderColor = UIColor.lightGray.cgColor
        subChallengeDescField.layer.borderWidth = 0.25
        subChallengeDescField.layer.cornerRadius = 5.0
    }
    
    // MARK: handle date picker
    @objc func dateChanged(datePicker: UIDatePicker) {
        subChallengeDateField.text = dateFormatter.string(from: datePicker.date)
    }
    
    // MARK: limit description length
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        let currentString: NSString = textView.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: text) as NSString
        
        return newString.length <= C.subChallengeDescMaxChars
        
    }
    
    // MARK: persist text through row edits
    func setSyncDateChange(action: @escaping (String) -> Void) {
        self.syncDateChange = action
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        syncDateChange?(textField.text!)
    }
    
    func setSyncDescChange(action: @escaping (String) -> Void) {
        self.syncDescChange = action
    }
    
    func textViewDidChange(_ textView: UITextView) {
        syncDescChange?(textView.text)
    }
    

}
