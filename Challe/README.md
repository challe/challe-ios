#  README

Gotchas:
- Challenge, sub-challenge dates are recorded as 00:00 UTC on their selected date
- User-inputted strings (e.g., titles, descriptions, user's names, comment texts) are UTF8 encoded before written to MongoDB and must be decoded when read
