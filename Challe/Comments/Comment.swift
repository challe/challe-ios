//
//  Comment.swift
//  Challe
//
//  Created by Rob Dearborn on 3/7/19.
//  Copyright © 2019 Challe. All rights reserved.
//

import UIKit

import StitchCore


class Comment {
    
    var id: ObjectId
    var creatorImageURL: URL
    var creator: String
    var creatorId: String
    var userIsCreator: Bool
    var text: String
    
    init?(id: ObjectId, creatorImageURL: URL, creator: String, creatorId: String, userIsCreator: Bool, text: String) {
        
        self.id = id
        self.creatorImageURL = creatorImageURL
        self.creator = creator
        self.creatorId = creatorId
        self.userIsCreator = userIsCreator
        self.text = text
        
    }
    
}
