//
//  AllCommentsViewController.swift
//  Challe
//
//  Created by Rob Dearborn on 3/8/19.
//  Copyright © 2019 Challe. All rights reserved.
//

import UIKit

import StitchCore


class AllCommentsViewController: UIViewController {
    
    // MARK: Properties
    @IBOutlet weak var commentsView: UIView!
    
    var threadType: String?
    var threadId: ObjectId?

    // MARK: Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let threadType = threadType else {
            print("CommentsViewController error: threadType not set")
            return
        }
        
        guard let threadId = threadId else {
            print("CommentsViewController error: threadId not set")
            return
        }
        
        // Initialize comments view
        if let vc = storyboard?.instantiateViewController(withIdentifier: "commentsViewController") as? CommentsViewController {
            vc.threadType = threadType
            vc.threadId = threadId
            vc.enableContinuedLoading = false
            vc.view.translatesAutoresizingMaskIntoConstraints = false
            
            addChild(vc)
            commentsView.addSubview(vc.view)
            
            NSLayoutConstraint.activate([
                vc.view.leadingAnchor.constraint(equalTo: commentsView.leadingAnchor),
                vc.view.trailingAnchor.constraint(equalTo: commentsView.trailingAnchor),
                vc.view.topAnchor.constraint(equalTo: commentsView.topAnchor),
                vc.view.bottomAnchor.constraint(equalTo: commentsView.bottomAnchor)
                ])
            
            vc.didMove(toParent: self)
        }
        
        // Swipe to dismiss
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(dismissVC(_:)))
        swipeDown.direction = UISwipeGestureRecognizer.Direction.down
        self.view.addGestureRecognizer(swipeDown)
        
    }
    
    // MARK: keyboard methods
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

    // MARK: Button actions
    @IBAction func dismissVC(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}
