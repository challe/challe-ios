//
//  CommentsTableViewCell
//  Challe
//
//  Created by Rob Dearborn on 3/7/19.
//  Copyright © 2019 Challe. All rights reserved.
//

import UIKit

protocol CommentsTableViewCellDelegator {
    func viewUserProfile(challeUserId: String)
}

class CommentsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var commentImageView: UIImageView!
    @IBOutlet weak var commentTextView: UITextView!
    
    var delegate: CommentsViewController!
    var comment: Comment!
    
    override func awakeFromNib() {
        super.awakeFromNib()     
                
    }
    
    @IBAction func commentTextButton(_ sender: Any) {
        self.delegate.viewUserProfile(challeUserId: comment.creatorId)
    }
}
