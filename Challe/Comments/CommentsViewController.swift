//
//  CommentsViewController.swift
//  Challe
//
//  Created by Rob Dearborn on 3/8/19.
//  Copyright © 2019 Challe. All rights reserved.
//

import UIKit

import Firebase
import Presentr
import StitchCore


class CommentsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, CommentsTableViewCellDelegator {
    
    // MARK: Properties
    @IBOutlet weak var commentsTableView: UITableView!
    @IBOutlet weak var addCommentImageView: UIImageView!
    @IBOutlet weak var addCommentTextField: KeyboardResponsiveTextField!
    @IBOutlet weak var addCommentButton: UIButton!
    
    var threadType: String?
    var threadId: ObjectId?
    private let username = keychain.get(KeychainKeys.username.rawValue) ?? "username"
    private let userImageURL = keychain.get(KeychainKeys.userImageURL.rawValue) ?? "userImageURL"
    private var comments = [Comment]()
    var enableContinuedLoading = true
    private var pauseLoading = false
    private var maxCreatedAt = Date()
    var commentsPerPage = C.commentsViewControllerDefaultCommentsPerPage
    private var onPage = 1
    private var vSpinner: UIView?

    // MARK: Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let threadType = threadType else {
            print("CommentsViewController error: threadType not set")
            return
        }
        
        guard let threadId = threadId else {
            print("CommentsViewController error: threadId not set")
            return
        }

        // Initialize comments table view
        commentsTableView.delegate = self
        commentsTableView.dataSource = self
        commentsTableView.isEditing = false
        commentsTableView.showsVerticalScrollIndicator = false
        commentsTableView.showsHorizontalScrollIndicator = false
        
        addCommentImageView.setImage(from: URL(string: userImageURL)!)
        addCommentTextField.delegate = self
        
        // populate comments
        loadComments()
    }

    // MARK: Table view methods
    // initialize # of rows
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return comments.count
    }
    
    // form cells
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "CommentsTableViewCell"
        
        // create cell
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? CommentsTableViewCell else {
            fatalError("The dequeued cell is not an instance of CommentsTableViewCell.")
        }
        cell.delegate = self
        cell.selectionStyle = .none
        
        // load cell data
        let comment = comments[indexPath.row]
        cell.comment = comment
        
        cell.commentImageView.setImage(from: comment.creatorImageURL)
        
        let creator = comment.creator
        let commentText = (
            creator + "  " + comment.text
        ) as NSString
            
        let attributedCommentText = NSMutableAttributedString(
            string: commentText as String,
            attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize: 12.0)]
        )
        let boldFontAttribute = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 12.0)]
        attributedCommentText.addAttributes(boldFontAttribute, range: commentText.range(of: creator))
        
        cell.commentTextView.attributedText = attributedCommentText
        
        return cell
    }
    
    // reload and pagination
    private func loadComments(reload: Bool = false) {
        if vSpinner == nil || vSpinner?.superview == nil {
            vSpinner = showSpinner(onView: self.view)
        }
        
        if reload {
            self.onPage = 1
            self.maxCreatedAt = Date()
        }
        
        stitchClient.callFunction(withName: "getComments",
                                  withArgs: [threadType!, threadId!, maxCreatedAt, commentsPerPage, onPage]) { (result: StitchResult<[Document]>) in
            switch result {
            case .failure(let error):
                print("Stitch challenge comments collection error: \(String(describing: error))")
                if self.vSpinner != nil { self.removeSpinner(vSpinner: self.vSpinner!) }
                if C.stitchAuthErrorDescriptions.contains(error.description) {
                    DispatchQueue.main.async {
                        logoutFlow(presentFrom: self)
                    }
                }
            case .success(let challengeComments):
                DispatchQueue.main.async {
                    if reload {
                        self.comments.removeAll()
                    }
                    
                    for comment in challengeComments {
                        guard let id = comment["_id"] as? ObjectId,
                              let urlStr = comment["creator_image_url"] as? String,
                              let url = URL(string: urlStr) as? URL,
                              let encodedCreator = comment["creator"] as? String,
                              let creatorId = comment["creator_id"] as? String,
                              let encodedText = comment["text"] as? String,
                              let newComment = Comment(
                                  id: id,
                                  creatorImageURL: url,
                                  creator: encodedCreator.utf8DecodedString(),
                                  creatorId: creatorId,
                                  userIsCreator: creatorId == stitchClient.auth.currentUser!.id,
                                  text: encodedText.utf8DecodedString()
                             )
                        else {
                            continue
                        }
                        self.comments.append(newComment)
                    }
                    self.commentsTableView.reloadData()
                    if self.comments.count == 0 {
                        self.commentsTableView.backgroundView = nil
                    }
                    Analytics.logEvent("load_comments", parameters: ["reload": reload, "on_page": self.onPage])
                    self.onPage += 1
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.0) {
                        self.pauseLoading = false
                    }
                    if self.vSpinner != nil { self.removeSpinner(vSpinner: self.vSpinner!) }
                }
            }
        }
    }
    
    internal func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let frameHeight = scrollView.frame.size.height
        let contentYoffset = scrollView.contentOffset.y
        let rowsHeight = scrollView.contentSize.height
        let distanceFromBottom = rowsHeight - contentYoffset
        
        // Reload when pulled down
        if enableContinuedLoading && !pauseLoading && contentYoffset < -50 {
            pauseLoading = true
            loadComments(reload: true)
        }
        // Load more when bottom of table reached
        if enableContinuedLoading && !pauseLoading && (rowsHeight > frameHeight) && (distanceFromBottom < frameHeight) {
            pauseLoading = true
            loadComments()
        }
    }
    
    // Selection
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewUserProfile(challeUserId: comments[indexPath.row].creatorId)
    }
    
    func viewUserProfile(challeUserId: String) {
        if !pauseLoading {
            let presenter = Presentr(presentationType: .fullScreen)
            
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "otherProfileViewController") as? OtherProfileViewController {
                vc.challeUserId = challeUserId
                customPresentViewController(presenter, viewController: vc, animated: true)
            }
        }
    }
    
    // Allow deleting and flagging comments
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let comment = comments[indexPath.row]
        var swipeConfig: UISwipeActionsConfiguration?
        if comment.userIsCreator {
            let deleteAction = self.contextualDeleteAction(forRowAt: indexPath)
            swipeConfig = UISwipeActionsConfiguration(actions: [deleteAction])
        } else {
            let reportAction = self.contextualReportAction(forRowAt: indexPath)
            swipeConfig = UISwipeActionsConfiguration(actions: [reportAction])
        }
        return swipeConfig
    }
    
    private func contextualDeleteAction(forRowAt indexPath: IndexPath) -> UIContextualAction {
        let comment = comments[indexPath.row]
        let action = UIContextualAction(style: .normal, title: "Delete") {
            (contextAction: UIContextualAction, sourceView: UIView, completionHandler: (Bool) -> Void) in
            
            let alertController = UIAlertController(
                title: "Delete this comment?",
                message: "Once deleted, comments cannot be recovered.",
                preferredStyle: .alert
            )
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
                print("Comment deletion canceled.")
            }
            alertController.addAction(cancelAction)
            
            let destroyAction = UIAlertAction(title: "Delete", style: .destructive) { (action) in
                stitchClient.callFunction(withName: "deleteComment", withArgs: [comment.id]) { (result: StitchResult<String>) in
                    switch result {
                    case .failure(let error):
                        print("Stitch comment deletion error: \(String(describing: error))")
                        if C.stitchAuthErrorDescriptions.contains(error.description) {
                            DispatchQueue.main.async {
                                logoutFlow(presentFrom: self)
                            }
                        }
                    case .success(_):
                        DispatchQueue.main.async {
                            Analytics.logEvent("delete_comment", parameters: nil)
                            self.comments.remove(at: indexPath.row)
                            self.commentsTableView.deleteRows(at: [indexPath], with: .fade)
                        }
                    }
                }
            }
            alertController.addAction(destroyAction)
            
            self.present(alertController, animated: true) {
                // ...
            }
            completionHandler(true)
        }
        action.image = UIImage(named: "trash")
        action.backgroundColor = UIColor.red
        return action
    }
    
    private func contextualReportAction(forRowAt indexPath: IndexPath) -> UIContextualAction {
        let comment = comments[indexPath.row]
        let action = UIContextualAction(style: .normal, title: "Report") {
            (contextAction: UIContextualAction, sourceView: UIView, completionHandler: (Bool) -> Void) in
            
            let alertController = UIAlertController(
                title: "Report this comment?",
                message: "Reported comments may be removed if inappropriate.",
                preferredStyle: .alert
            )
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
                print("Comment flagging canceled.")
            }
            alertController.addAction(cancelAction)
            
            let destroyAction = UIAlertAction(title: "Report", style: .destructive) { (action) in
                let flagDoc: Document = [
                    "creator_id": stitchClient.auth.currentUser!.id,
                    "created_at": Date(),
                    "target_type": "comment",
                    "target_id": comment.id
                ]
                stitchClient.callFunction(withName: "createFlag", withArgs: [flagDoc]) { (result: StitchResult<String>) in
                    switch result {
                    case .failure(let error):
                        print("Stitch flag creation error: \(String(describing: error))")
                        if C.stitchAuthErrorDescriptions.contains(error.description) {
                            DispatchQueue.main.async {
                                logoutFlow(presentFrom: self)
                            }
                        }
                    case .success(let successMessage):
                        Analytics.logEvent("flag_comment", parameters: nil)
                        print(successMessage)
                    }
                }
            }
            alertController.addAction(destroyAction)
            
            self.present(alertController, animated: true) {
                // ...
            }
            completionHandler(true)
        }
        action.image = UIImage(named: "alert")
        action.backgroundColor = UIColor.orange
        return action
    }
    // Add a comment
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let maxLength = 150
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        
        return newString.length <= maxLength
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    // MARK: Button actions
    @IBAction func addCommentButtonAction(_ sender: UIButton) {
        guard let threadId = threadId else {
            print("CommentsViewController error: threadId not set")
            return
        }
        
        let text = addCommentTextField.text!.utf8EncodedString()
        
        if  text.count > 0 {
            let creatorId = stitchClient.auth.currentUser!.id
            
            let newCommentDoc: Document = [
                "creator_id": creatorId,
                "creator_image_url": userImageURL,
                "thread_type": "challenge",
                "thread_id": threadId,
                "created_at": Date(),
                "is_deleted": false,
                "n_flags": 0 as Int64,
                "creator": "\(username)".utf8EncodedString(),
                "text": text
            ]
            
            stitchClient.callFunction(withName: "createComment", withArgs: [newCommentDoc]) { (result: StitchResult<String>) in
                switch result {
                case .failure(let error):
                    print("Stitch comment creation error: \(String(describing: error))")
                    if C.stitchAuthErrorDescriptions.contains(error.description) {
                        DispatchQueue.main.async {
                            logoutFlow(presentFrom: self)
                        }
                    }
                case .success(let commentId):
                    guard let newComment = Comment(
                        id: ObjectId(commentId)!,
                        creatorImageURL: URL(string: self.userImageURL)!,
                        creator: "\(self.username)".utf8DecodedString(),
                        creatorId: creatorId,
                        userIsCreator: true,
                        text:  text.utf8DecodedString()
                        ) else { return }
                    DispatchQueue.main.async {
                        Analytics.logEvent("create_comment", parameters: nil)
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.0) {
                            self.addCommentButton.isSelected = false
                        }
                        self.addCommentTextField.text = ""
                        self.comments.insert(newComment, at: 0)
                        self.commentsTableView.reloadData()
                    }
                }
            }
        }
    }
}
