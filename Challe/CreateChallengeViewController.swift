//
//  CreateChallengeViewController.swift
//  Challe
//
//  Created by Rob Dearborn on 2/25/19.
//  Copyright © 2019 Challe. All rights reserved.
//

import UIKit

import Firebase
import Presentr
import StitchCore


class CreateChallengeViewController: UIViewController, UITextFieldDelegate, UITextViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UITableViewDelegate, UITableViewDataSource {
    
    // MARK: Properties
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var creatorTextLabel: UILabel!
    @IBOutlet weak var startTextField: UITextField!
    @IBOutlet weak var endTextField: UITextField!
    @IBOutlet weak var visibilityTextField: UITextField!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var subChallengeInputTableView: UITableView!
    @IBOutlet weak var warningTextLabel: UILabel!
    @IBOutlet weak var submissionButton: UIButton!
    
    private let username = keychain.get(KeychainKeys.username.rawValue)!
    private let dateFormatter = DateFormatter()
    private var startDatePicker: UIDatePicker?
    private var endDatePicker: UIDatePicker?
    private var subChallenges = [SubChallenge]()
    private var cellHeights: [IndexPath : CGFloat] = [:]
    private var vSpinner: UIView?
    
    // MARK: Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Initialize title delegate
        titleTextField.delegate = self
        
        // Initialize byline
        creatorTextLabel.text = "by \(username)"
        
        // Date pickers
        dateFormatter.dateFormat = "MM/dd/yyyy"
        dateFormatter.timeZone = TimeZone.init(secondsFromGMT: 0)
        
        startDatePicker = UIDatePicker()
        startDatePicker?.timeZone = TimeZone.init(secondsFromGMT: 0)
        startDatePicker?.datePickerMode = .date
        startDatePicker?.addTarget(
            self,
            action: #selector(self.startDateChanged(datePicker:)),
            for: .valueChanged
        )
        startTextField.inputView = startDatePicker
        
        endDatePicker = UIDatePicker()
        endDatePicker?.timeZone = TimeZone.init(secondsFromGMT: 0)
        endDatePicker?.datePickerMode = .date
        endDatePicker?.addTarget(
            self,
            action: #selector(self.endDateChanged(datePicker:)),
            for: .valueChanged
        )
        endTextField.inputView = endDatePicker
        
        // Visibility selection
        let visibilityPicker = UIPickerView()
        visibilityTextField.inputView = visibilityPicker
        visibilityPicker.delegate = self
        
        // Initialize description delegate and borders
        descriptionTextView.delegate = self
        descriptionTextView.layer.borderColor = UIColor.lightGray.cgColor
        descriptionTextView.layer.borderWidth = 0.25
        descriptionTextView.layer.cornerRadius = 5.0
        
        // Initialize table view
        subChallengeInputTableView.delegate = self
        subChallengeInputTableView.dataSource = self
        subChallengeInputTableView.isEditing = false
        subChallengeInputTableView.showsVerticalScrollIndicator = false
        subChallengeInputTableView.showsHorizontalScrollIndicator = false
        addSubChallengeInput()
        
        // Initialize submission button borders
        submissionButton.layer.borderColor = UIColor(named: "ChalleRed")?.cgColor
        submissionButton.layer.borderWidth = 0.5
        submissionButton.layer.cornerRadius = 5.0
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        // constrain sCITV height once screen size determined
        // so that it can be pushed up by keyboard
        let initialHeight = subChallengeInputTableView.frame.height
        var sCITVHeightConstraint = subChallengeInputTableView.heightAnchor.constraint(equalToConstant: initialHeight)
        sCITVHeightConstraint.priority = UILayoutPriority(rawValue: 750)
        NSLayoutConstraint.activate([
            sCITVHeightConstraint
        ])
    }
    
    // MARK: Keyboard methods
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    // MARK: Date picker methods
    @objc private func startDateChanged(datePicker: UIDatePicker) {
        startTextField.text = dateFormatter.string(from: datePicker.date)
    }
    
    @objc private func endDateChanged(datePicker: UIDatePicker) {
        endTextField.text = dateFormatter.string(from: datePicker.date)
    }
    
    // MARK: Visibility picker methods
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView( _ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return C.challengeVisibilityOptions.count
    }
    
    func pickerView( _ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return C.challengeVisibilityOptions[row]
    }
    
    func pickerView( _ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        visibilityTextField.text = C.challengeVisibilityOptions[row]
    }
    
    // MARK: Table view methods
    // initialize # of rows
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return subChallenges.count
    }
    
    // form cells
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "CreateSubChallengeTableViewCell"
        
        // create cell
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? CreateSubChallengeTableViewCell else {
            fatalError("The dequeued cell is not an instance of CreateSubChallengeTableViewCell.")
        }
        cell.selectionStyle = .none
        
        // load cell data
        let subChallenge = subChallenges[indexPath.row]
        cell.subChallengeDateField.text = subChallenge.dateStr
        cell.subChallengeDescField.text = subChallenge.description
        
        // allow for persistant text editing
        cell.setSyncDateChange {[weak tableView, weak self] newDate in
            self?.subChallenges[indexPath.row].dateStr = newDate
            cell.subChallengeDateField.text = newDate

            DispatchQueue.main.async {
                tableView?.beginUpdates()
                tableView?.endUpdates()
            }
        }
        
        cell.setSyncDescChange {[weak tableView, weak self] newDesc in
            self?.subChallenges[indexPath.row].description = newDesc
            cell.subChallengeDescField.text = newDesc
            
            DispatchQueue.main.async {
                tableView?.beginUpdates()
                tableView?.endUpdates()
            }
        }
        
        return cell
    }
    
    // enable delete and reorder
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let movedObject = subChallenges[sourceIndexPath.row]
        subChallenges.remove(at: sourceIndexPath.row)
        subChallenges.insert(movedObject, at: destinationIndexPath.row)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            subChallenges.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            
        }
    }
    
    // MARK: Text limiting methods
    // title
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        
        return newString.length <= C.challengeTitleMaxChars
    }
    
    // description
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        let currentString: NSString = textView.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: text) as NSString
        
        return newString.length <= C.challengeDescMaxChars
    }
    
    
    @objc private func addSubChallengeInput(){
        guard let newSubChallenge = SubChallenge(is_complete: false, dateStr: "", description: "") else {
            fatalError("Unable to create new SubChallenge")
        }
        
        subChallenges += [newSubChallenge]
        
        subChallengeInputTableView.reloadData()
    }
    
    // MARK: Button Actions
    @IBAction func addSubChallengeButtonAction(_ sender: UIButton) {
        if subChallenges.count < 500 {
            addSubChallengeInput()
        }
    }
    
    @IBAction func reorderSubChallengesButtonAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        subChallengeInputTableView.isEditing = !subChallengeInputTableView.isEditing
    }
    
    @IBAction func dismissVC(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func submitChallengeButtonAction(_ sender: UIButton) {
        
        Analytics.logEvent("tap_submit_new_challenge_button", parameters: nil)
        
        var incompleteFields = false
        var dateError = false
        
        // check fields
        let title = titleTextField.text!
        incompleteFields = incompleteFields || (title.count == 0)

        let startDateStr = startTextField.text!
        incompleteFields = incompleteFields || (startDateStr.count == 0)
        let startDate: Date
        if startDateStr.count > 0 {
            startDate = dateFormatter.date(from: startDateStr)!
        } else {
            startDate = dateFormatter.date(from: "01/01/2000")!
        }

        let endDateStr = endTextField.text!
        incompleteFields = incompleteFields || (endDateStr.count == 0)
        let endDate: Date
        if endDateStr.count > 0 {
            endDate = dateFormatter.date(from: endDateStr)!
        } else {
            endDate = dateFormatter.date(from: "01/01/2000")!
        }
        dateError = dateError || (startDate > endDate)
        
        let visibility = visibilityTextField.text!
        incompleteFields = incompleteFields || (visibility.count == 0)

        let description = descriptionTextView.text!
        incompleteFields = incompleteFields || (description.count == 0)
        
        // check subChallenges
        var subChallengesDoc: Document = []
        var subChallengesProgressionDoc: Document = []
        var priorSubChallengeDate = dateFormatter.date(from: "01/01/2000")!
        incompleteFields = (incompleteFields || (subChallenges.count == 0))
        for (n, subChallenge) in subChallenges.enumerated() {
            
            incompleteFields = (
                incompleteFields
                || (subChallenge.dateStr.count == 0)
                || (subChallenge.description.count == 0)
            )
            
            if !incompleteFields {
                
                let subChallengeDate = dateFormatter.date(from: subChallenge.dateStr)!
                dateError = (
                    dateError
                        || (subChallengeDate < priorSubChallengeDate)
                        || (subChallengeDate < startDate)
                        || (subChallengeDate > endDate)
                )
                
                priorSubChallengeDate = subChallengeDate
                let subChallengeDoc: Document = [
                    "is_complete": subChallenge.is_complete,
                    "date": subChallengeDate,
                    "description": subChallenge.description.utf8EncodedString()
                ]
                subChallengesDoc[String(n)] = subChallengeDoc
                
                // for user_challenge record initialization
                let subChallengeProgressionDoc: Document = [
                    "is_complete": subChallenge.is_complete
                ]
                subChallengesProgressionDoc[String(n)] = subChallengeProgressionDoc
            }
            else {
                subChallengesDoc[String(n)] = nil
            }
        }
        
        // write to mdb if no errors
        if incompleteFields {
            warningTextLabel.text = "Please complete all fields 😊"
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3.0) {
                self.warningTextLabel.text = ""
            }
        }
        else if dateError {
            warningTextLabel.text = "Please correct dates errors 😊"
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3.0) {
                self.warningTextLabel.text = ""
            }
        }
        else {
            if vSpinner == nil || vSpinner?.superview == nil {
                vSpinner = showSpinner(onView: self.view)
            }
            
            let newChallenge: Document = [
                "creator_id": stitchClient.auth.currentUser!.id,
                "creator_image_url": keychain.get(KeychainKeys.userImageURL.rawValue)!,
                "created_at": Date(),
                "is_deleted": false,
                "n_flags": 0 as Int64,
                "visibility": visibility,
                "title": title.utf8EncodedString(),
                "creator": "\(username)".utf8EncodedString(),
                "start_date": startDate,
                "end_date": endDate,
                "description": description.utf8EncodedString(),
                "sub_challenges": subChallengesDoc,
                "n_followers": 0 as Int64
            ]
            
            stitchClient.callFunction(withName: "createChallenge", withArgs: [newChallenge]) { (result: StitchResult<String>) in
                switch result {
                case .failure(let error):
                    print("Stitch challenge creation error: \(String(describing: error))")
                    if self.vSpinner != nil { self.removeSpinner(vSpinner: self.vSpinner!) }
                    if C.stitchAuthErrorDescriptions.contains(error.description) {
                        DispatchQueue.main.async {
                            logoutFlow(presentFrom: self)
                        }
                    }
                case .success(let challengeId):
                    print("Stitch created challenge with ID: \(challengeId)")
                    let newUserChallenge: Document = [
                        "creator_id": stitchClient.auth.currentUser!.id,
                        "created_at": Date(),
                        "is_deleted": false,
                        "challenge_id": ObjectId(challengeId)!,
                        "user_progression": subChallengesProgressionDoc,
                        "n_subchallenges": Int64(exactly: subChallengesProgressionDoc.count)!,
                        "n_completed_subchallenges": 0 as Int64
                    ]
                    stitchClient.callFunction(withName: "createUserChallenge", withArgs: [newUserChallenge]) { (result: StitchResult<String>) in
                        switch result {
                        case .failure(let error):
                            print("Stitch user_challenge creation error: \(String(describing: error))")
                            if self.vSpinner != nil { self.removeSpinner(vSpinner: self.vSpinner!) }
                            if C.stitchAuthErrorDescriptions.contains(error.description) {
                                DispatchQueue.main.async {
                                    logoutFlow(presentFrom: self)
                                }
                            }
                        case .success(let userChallengeId):
                            print("Stitch created user challenge with ID: \(userChallengeId)")
                            
                            NotificationCenter.default.post(
                                name: .didCreateChallenge ,
                                object: nil,
                                userInfo: nil
                            )
                            
                            DispatchQueue.main.async {
                                weak var pvc = self.presentingViewController
                                if self.vSpinner != nil { self.removeSpinner(vSpinner: self.vSpinner!) }
                                self.dismiss(animated: true) {
                                    Analytics.logEvent("create_challenge", parameters: ["id": challengeId])
                                    
                                    let presenter = Presentr(presentationType: .fullScreen)
                                    
                                    if let pvc = pvc, let vc = pvc.storyboard?.instantiateViewController(withIdentifier: "challengeViewController") as? ChallengeViewController {
                                        vc.challengeId = ObjectId(challengeId)!
                                        pvc.customPresentViewController(presenter, viewController: vc, animated: true)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
