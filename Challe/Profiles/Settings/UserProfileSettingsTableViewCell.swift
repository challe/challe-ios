//
//  UserProfileSettingsTableViewCell.swift
//  Challe
//
//  Created by Rob Dearborn on 3/19/19.
//  Copyright © 2019 Challe. All rights reserved.
//

import UIKit


class UserProfileSettingsTableViewCell: UITableViewCell {
    
    // MARK: Properties
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var usernameTextLabel: UILabel!
    @IBOutlet weak var fullNameTextLabel: UILabel!

    // MARK: Overrides
    override func awakeFromNib() {
        super.awakeFromNib()
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(onUpdateOwnProfile(_:)),
            name: .didUpdateOwnProfile,
            object: nil
        )
        
        loadUserData()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: Data loading
    private func loadUserData() {
        if let userImageURLStr = keychain.get(KeychainKeys.userImageURL.rawValue), let userImageURL = URL(string: userImageURLStr) {
            userImageView.setImage(from: userImageURL)
        }
        
        if let username = keychain.get(KeychainKeys.username.rawValue) {
            usernameTextLabel.text = username
        }
        
        if let firstName = keychain.get(KeychainKeys.userFirstName.rawValue), let lastName = keychain.get(KeychainKeys.userLastName.rawValue) {
            fullNameTextLabel.text = "\(firstName.utf8DecodedString()) \(lastName.utf8DecodedString())"
        }
    }
    
    // MARK: listeners
    @objc private func onUpdateOwnProfile(_ notification:Notification) {
        DispatchQueue.main.async {
            self.loadUserData()
        }
    }

}
