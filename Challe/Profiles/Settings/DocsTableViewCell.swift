//
//  DocsTableViewCell.swift
//  Challe
//
//  Created by Rob Dearborn on 4/10/19.
//  Copyright © 2019 Challe. All rights reserved.
//

import UIKit

class DocsTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func communityGuidelinesButtonAction(_ sender: UIButton) {
        if let url = URL(string: "https://challe.co/community-guidelines") {
            UIApplication.shared.open(url)
        }
    }
    
    @IBAction func privacyPolicyButtonAction(_ sender: UIButton) {
        if let url = URL(string: "https://challe.co/privacy-policy/") {
            UIApplication.shared.open(url)
        }
    }
    
    @IBAction func termsOfUseButtonAction(_ sender: UIButton) {
        if let url = URL(string: "https://challe.co/terms/") {
            UIApplication.shared.open(url)
        }
    }

}
