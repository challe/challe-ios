//
//  NotificationsSettingsTableViewCell.swift
//  Challe
//
//  Created by Rob Dearborn on 3/19/19.
//  Copyright © 2019 Challe. All rights reserved.
//

import UIKit

import Firebase
import StitchCore


class NotificationsSettingsTableViewCell: UITableViewCell, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {

    // MARK: Properties
    @IBOutlet weak var dailyChallengeRemindersSwitch: UISwitch!
    @IBOutlet weak var dailyChallengeRemindersReceiveAtField: UITextField!
    @IBOutlet weak var newChallengeJoinerNotifsSwitch: UISwitch!
    @IBOutlet weak var newChallengeCommentNotifsSwitch: UISwitch!
    @IBOutlet weak var followedUserCommentedOnJoinedChallengeNotifsSwitch: UISwitch!
    @IBOutlet weak var followedUserCreatedChallengeNotifsSwitch: UISwitch!
    @IBOutlet weak var newProfileFollowerNotifsSwitch: UISwitch!
    @IBOutlet weak var profileFollowRequestApprovedNotifsSwitch: UISwitch!
    
    private var hourPicker = UIPickerView()
    private var userTimezone: String!
    
    // MARK: Overrides
    override func awakeFromNib() {
        super.awakeFromNib()
        dailyChallengeRemindersSwitch.isEnabled = false
        newChallengeJoinerNotifsSwitch.isEnabled = false
        newChallengeCommentNotifsSwitch.isEnabled = false
        followedUserCommentedOnJoinedChallengeNotifsSwitch.isEnabled = false
        followedUserCreatedChallengeNotifsSwitch.isEnabled = false
        newProfileFollowerNotifsSwitch.isEnabled = false
        profileFollowRequestApprovedNotifsSwitch.isEnabled = false
        
        dailyChallengeRemindersReceiveAtField.isEnabled = false
        dailyChallengeRemindersReceiveAtField.delegate = self
        dailyChallengeRemindersReceiveAtField.inputView = hourPicker
        hourPicker.delegate = self
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(onUpdateOwnProfile(_:)),
            name: .didUpdateOwnProfile,
            object: nil
        )
        
        loadUserData()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // exit keyboards / pickers
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    // MARK: Data loading
    private func loadUserData() {
        guard let ownUserId = stitchClient.auth.currentUser?.id else {
            print("UserNotificationSettingsTableViewCell error: user must be authenticated")
            return
        }
        
        // Load User Info
        stitchClient.callFunction(withName: "getUserNotificationSettings", withArgs: [ownUserId]) { (result: StitchResult<Document>) in
            switch result {
            case .failure(let error):
                print("Stitch user notification settings collection error: \(String(describing: error))")
            case .success(let unsDoc):
                
                if let receiveDailyChallengeReminders = unsDoc["receive_challenge_reminders_default"] as? Bool, let receiveDailyChallengeRemindersAtUTCHour = unsDoc["receive_challenge_reminders_at_utc_hour_default"] as? Int, let timezone = unsDoc["timezone"] as? String {
                    DispatchQueue.main.async {
                        self.dailyChallengeRemindersSwitch.isOn = receiveDailyChallengeReminders
                        self.dailyChallengeRemindersReceiveAtField.isEnabled = receiveDailyChallengeReminders
                        self.dailyChallengeRemindersSwitch.isEnabled = true
                        
                        self.userTimezone = timezone
                        
                        let receiveDailyChallengeRemindersAtLocalHour = hourTimezoneConversion(hourOfDay: Int(receiveDailyChallengeRemindersAtUTCHour), firstTimezone: TimeZone(abbreviation: "UTC")!, secondTimezone: TimeZone(identifier: self.userTimezone)!)
                        self.dailyChallengeRemindersReceiveAtField.text = C.hourOptions[receiveDailyChallengeRemindersAtLocalHour]
                        
                        let row = C.hourOptions.firstIndex(of: self.dailyChallengeRemindersReceiveAtField.text!)!
                        self.hourPicker.selectRow(row, inComponent: 0, animated: true)
                    }
                }
                
                if let receiveNewChallengeJoinerNotifs = unsDoc["receive_new_challenge_joiner_notifs"] as? Bool {
                    DispatchQueue.main.async {
                        self.newChallengeJoinerNotifsSwitch.isOn = receiveNewChallengeJoinerNotifs
                        self.newChallengeJoinerNotifsSwitch.isEnabled = true
                    }
                }
                
                if let receiveNewChallengeCommentNotifs = unsDoc["receive_new_challenge_comment_notifs"] as? Bool {
                    DispatchQueue.main.async {
                        self.newChallengeCommentNotifsSwitch.isOn = receiveNewChallengeCommentNotifs
                        self.newChallengeCommentNotifsSwitch.isEnabled = true
                    }
                }
                
                if let receiveFollowedUserCommentedOnJoinedChallengeNotifs = unsDoc["receive_followed_user_commented_on_joined_challenge_notifs"] as? Bool {
                    DispatchQueue.main.async {
                        self.followedUserCommentedOnJoinedChallengeNotifsSwitch.isOn = receiveFollowedUserCommentedOnJoinedChallengeNotifs
                        self.followedUserCommentedOnJoinedChallengeNotifsSwitch.isEnabled = true
                    }
                }
                
                if let receiveFollowedUserCreatedChallengeNotifs = unsDoc["receive_followed_user_created_challenge_notifs"] as? Bool {
                    DispatchQueue.main.async {
                        self.followedUserCreatedChallengeNotifsSwitch.isOn = receiveFollowedUserCreatedChallengeNotifs
                        self.followedUserCreatedChallengeNotifsSwitch.isEnabled = true
                    }
                }
                
                // NB: this also maps to receive_profile_follow_request_notifs
                if let receiveNewProfileFollowerNotifs = unsDoc["receive_new_profile_follower_notifs"] as? Bool {
                    DispatchQueue.main.async {
                        self.newProfileFollowerNotifsSwitch.isOn = receiveNewProfileFollowerNotifs
                        self.newProfileFollowerNotifsSwitch.isEnabled = true
                    }
                }
                
                
                if let receiveProfileFollowRequestApprovedNotifs = unsDoc["receive_profile_follow_request_approved_notifs"] as? Bool {
                    DispatchQueue.main.async {
                        self.profileFollowRequestApprovedNotifsSwitch.isOn = receiveProfileFollowRequestApprovedNotifs
                        self.profileFollowRequestApprovedNotifsSwitch.isEnabled = true
                    }
                }
                
            }
        }
    }
    
    // MARK: Picker methods
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView( _ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return C.hourOptions.count
    }
    
    func pickerView( _ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return C.hourOptions[row]
    }
    
    func pickerView( _ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        let hourOption = C.hourOptions[row]
        let receiveDailyChallengeRemindersAtLocalHour = C.hourOptions.firstIndex(of: hourOption)!
        
        let receiveDailyChallengeRemindersAtUTCHour = hourTimezoneConversion(hourOfDay: receiveDailyChallengeRemindersAtLocalHour, firstTimezone: TimeZone(identifier: self.userTimezone)!, secondTimezone: TimeZone(abbreviation: "UTC")!)
        
        // update user notification settings
        let updateDoc: Document = [
            "receive_challenge_reminders_at_utc_hour_default": Int64(exactly: receiveDailyChallengeRemindersAtUTCHour)!
        ]
        updateMongoUns(updateDoc: updateDoc, analyticsParameters: ["parameter": "receive_challenge_reminders_at_utc_hour_default", "value": receiveDailyChallengeRemindersAtUTCHour])
        
        return dailyChallengeRemindersReceiveAtField.text = hourOption
        
    }
    
    // MARK: listeners
    @objc private func onUpdateOwnProfile(_ notification:Notification) {
        DispatchQueue.main.async {
            self.loadUserData()
        }
    }
    
    // MARK: Button actions
    @IBAction func dailyChallengeRemindersSwitchAction(_ sender: UISwitch) {
        dailyChallengeRemindersReceiveAtField.isEnabled = dailyChallengeRemindersSwitch.isOn
        
        let updateDoc: Document = [
            "receive_challenge_reminders_default": dailyChallengeRemindersSwitch.isOn
        ]
        updateMongoUns(updateDoc: updateDoc, analyticsParameters: ["parameter": "receive_challenge_reminders_default", "value": self.dailyChallengeRemindersSwitch.isOn])
    }
    
    @IBAction func newChallengeJoinerNotifsSwitchAction(_ sender: UISwitch) {
        let updateDoc: Document = [
            "receive_new_challenge_joiner_notifs": newChallengeJoinerNotifsSwitch.isOn
        ]
        updateMongoUns(updateDoc: updateDoc, analyticsParameters: ["parameter": "receive_new_challenge_joiner_notifs", "value": self.newChallengeJoinerNotifsSwitch.isOn])
    }
    
    @IBAction func newChallengeCommentNotifsSwitchAction(_ sender: UISwitch) {
        let updateDoc: Document = [
            "receive_new_challenge_comment_notifs": newChallengeCommentNotifsSwitch.isOn
        ]
        updateMongoUns(updateDoc: updateDoc, analyticsParameters: ["parameter": "receive_new_challenge_comment_notifs", "value": self.newChallengeCommentNotifsSwitch.isOn])
    }
    
    @IBAction func followedUserCommentedOnJoinedChallengeNotifsSwitchAction(_ sender: UISwitch) {
        let updateDoc: Document = [
            "receive_followed_user_commented_on_joined_challenge_notifs": followedUserCommentedOnJoinedChallengeNotifsSwitch.isOn
        ]
        updateMongoUns(updateDoc: updateDoc, analyticsParameters: ["parameter": "receive_followed_user_commented_on_joined_challenge_notifs", "value": self.followedUserCommentedOnJoinedChallengeNotifsSwitch.isOn])
    }
    
    @IBAction func followedUserCreatedChallengeNotifsSwitchAction(_ sender: UISwitch) {
        let updateDoc: Document = [
            "receive_followed_user_created_challenge_notifs": followedUserCreatedChallengeNotifsSwitch.isOn
        ]
        updateMongoUns(updateDoc: updateDoc, analyticsParameters: ["parameter": "receive_followed_user_created_challenge_notifs", "value": self.followedUserCreatedChallengeNotifsSwitch.isOn])
    }
    
    @IBAction func newProfileFollowerNotifsSwitchAction(_ sender: UISwitch) {
        let updateDoc: Document = [
            "receive_new_profile_follower_notifs": newProfileFollowerNotifsSwitch.isOn,
            "receive_profile_follow_request_notifs": newProfileFollowerNotifsSwitch.isOn
        ]
        updateMongoUns(updateDoc: updateDoc, analyticsParameters: ["parameter": "receive_new_profile_follower_notifs", "value": self.newProfileFollowerNotifsSwitch.isOn])
    }
    
    @IBAction func profileFollowRequestApprovedNotifsSwitchAction(_ sender: UISwitch) {
        let updateDoc: Document = [
            "receive_profile_follow_request_approved_notifs": profileFollowRequestApprovedNotifsSwitch.isOn
        ]
        updateMongoUns(updateDoc: updateDoc, analyticsParameters: ["parameter": "receive_profile_follow_request_approved_notifs", "value": self.profileFollowRequestApprovedNotifsSwitch.isOn])
    }
    
    private func updateMongoUns(updateDoc: Document, analyticsParameters: [String: Any]?) {
        let userId = stitchClient.auth.currentUser!.id
        
        stitchClient.callFunction(withName: "updateUserNotificationSettings", withArgs: [userId, updateDoc]) {
            (result: StitchResult<String>) in
            switch result {
            case .failure(let error):
                print("Stitch update user notification settings error: \(error)")
            case .success(let user_id):
                DispatchQueue.main.async {
                    Analytics.logEvent("update_user_notification_settings", parameters: analyticsParameters)
                    print("Successfully updated user notification settings to Stitch: \(user_id)")
                }
            }
        }
    }
    
}
