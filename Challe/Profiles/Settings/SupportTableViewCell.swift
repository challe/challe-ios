//
//  SupportTableViewCell.swift
//  Challe
//
//  Created by Rob Dearborn on 4/10/19.
//  Copyright © 2019 Challe. All rights reserved.
//

import UIKit

class SupportTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func supportEmailButtonAction(_ sender: UIButton) {
        if let url = URL(string: "mailto:support@challe.co") {
            UIApplication.shared.open(url)
        }
    }
    
}
