//
//  UserProfileSettingsViewController.swift
//  Challe
//
//  Created by Rob Dearborn on 3/19/19.
//  Copyright © 2019 Challe. All rights reserved.
//

import UIKit

import AMPopTip
import AWSLambda
import Firebase
import Presentr
import StitchCore


class UserProfileSettingsViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate, UITextViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource {

    // MARK: Properties
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var editUserImageButton: UIButton!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var phoneNumberTextField: UITextField!
    @IBOutlet weak var bioTextView: UITextView!
    @IBOutlet weak var privacyTextField: UITextField!
    @IBOutlet weak var timezoneTextField: UITextField!
    @IBOutlet weak var privacyInfoButton: UIButton!
    @IBOutlet weak var messageTextView: UITextView!
    @IBOutlet weak var buttomButtonsView: UIView!
    
    @IBOutlet weak var deleteAccountButton: UIButton!
    
    
    private var challeUser: ChalleUser!
    private var imagePicker = UIImagePickerController()
    private var imageWasEdited = false
    private var originalUserEmail: String!
    private var emailWasEdited = false
    private var privacyPicker = UIPickerView()
    private var timezonePicker = UIPickerView()
    private var timezoneDeltaMinutes = 0
    private var getPhotoUploadURLRequest, updateAuth0UserRequest, updateMongoUserRequest: URLRequest!
    private var vSpinner: UIView?
    
    
    // MARK: Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let ownUserId = stitchClient.auth.currentUser?.id else {
            print("UserProfileSettingsViewController error: user must be authenticated")
            return
        }
        
        saveButton.isEnabled = false
        
        // fields config
        imagePicker.delegate = self
        
        emailTextField.delegate = self
        usernameTextField.delegate = self
        firstNameTextField.delegate = self
        lastNameTextField.delegate = self
        phoneNumberTextField.delegate = self
        bioTextView.delegate = self
        privacyTextField.delegate = self
        
        emailTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        usernameTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        firstNameTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        lastNameTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        phoneNumberTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        bioTextView.layer.borderColor = UIColor.lightGray.cgColor
        bioTextView.layer.borderWidth = 0.25
        bioTextView.layer.cornerRadius = 5.0
        
        privacyTextField.inputView = privacyPicker
        privacyPicker.delegate = self
        
        timezoneTextField.inputView = timezonePicker
        timezonePicker.delegate = self
        
        messageTextView.isHidden = true
        
        // URL config
        let getPhotoUploadURLURL = URL(string: "https://0r6psvjhfi.execute-api.us-east-1.amazonaws.com/prod/users/get-profile-photo-upload-url")!
        getPhotoUploadURLRequest = URLRequest(url: getPhotoUploadURLURL)
        getPhotoUploadURLRequest.httpMethod = "GET"
        
        let updateAuth0UserURL = URL(string: "https://0r6psvjhfi.execute-api.us-east-1.amazonaws.com/prod/users/update-auth0-user")!
        updateAuth0UserRequest = URLRequest(url: updateAuth0UserURL)
        updateAuth0UserRequest.httpMethod = "PATCH"
        
        let updateMongo0UserURL = URL(string: "https://0r6psvjhfi.execute-api.us-east-1.amazonaws.com/prod/users/update-mongo-user")!
        updateMongoUserRequest = URLRequest(url: updateMongo0UserURL)
        updateMongoUserRequest.httpMethod = "PATCH"

        // Populate user
        if vSpinner == nil || vSpinner?.superview == nil {
            vSpinner = showSpinner(onView: self.view)
        }
        
        stitchClient.callFunction(withName: "getUser", withArgs: [ownUserId, false]) { (result: StitchResult<Document>) in
            switch result {
            case .failure(let error):
                print("Stitch user collection error: \(String(describing: error))")
                if self.vSpinner != nil { self.removeSpinner(vSpinner: self.vSpinner!) }
                if C.stitchAuthErrorDescriptions.contains(error.description) {
                    DispatchQueue.main.async {
                        logoutFlow(presentFrom: self)
                    }
                } else {
                    self.dismiss(animated: true, completion: nil) // back up to avoid bad writes
                }
            case .success(let userDoc):
                DispatchQueue.main.async {
                    self.challeUser = ChalleUser(userDoc: userDoc)
                    
                    if let imageURL = URL(string: self.challeUser.imageURL) {
                        self.userImageView.setImage(from: imageURL)
                    }
                    if let email = userDoc["email"] as? String {
                        self.emailTextField.text = email
                        self.originalUserEmail = email
                    }
                    self.usernameTextField.text = self.challeUser.username
                    self.firstNameTextField.text = self.challeUser.firstName
                    self.lastNameTextField.text = self.challeUser.lastName
                    if let phoneNumberStr = userDoc["phone_number"] as? String {
                        self.phoneNumberTextField.text = phoneNumberStr
                    }
                    self.bioTextView.text = self.challeUser.bio
                    if let isPrivate = userDoc["is_private"] as? Bool {
                        
                        self.privacyTextField.text = C.userPrivacyMongoArgsToOptions[isPrivate]
                        let row = C.userPrivacyOptions.firstIndex(of: self.privacyTextField.text!)!
                        self.privacyPicker.selectRow(row, inComponent: 0, animated: true)
                    }
                    self.timezoneTextField.text = self.challeUser.timezone
                    let row = C.userTimezoneOptions.firstIndex(of: self.timezoneTextField.text!)!
                    self.timezonePicker.selectRow(row, inComponent: 0, animated: true)
                    if self.vSpinner != nil { self.removeSpinner(vSpinner: self.vSpinner!) }
                }
            }
        }
        
    }
    
    // exit keyboards / pickers
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    // MARK: Image input
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let chosenImage = info[UIImagePickerController.InfoKey.editedImage] as! UIImage
        userImageView.contentMode = .scaleAspectFill
        userImageView.image = chosenImage
        
        imageWasEdited = true
        saveButton.isEnabled = true
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    // MARK: text limiting methods
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        
        return newString.length <= C.userTextFieldsMaxChars
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        let currentString: NSString = textView.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: text) as NSString
        
        return newString.length <= C.userBioMaxChars
    }
    
    // MARK: methods to highlight, save changed fields
    @objc func textFieldDidChange(_ textField: UITextField) {
        textField.textColor = UIColor(named: "ChalleRed") ?? UIColor.red
        saveButton.isEnabled = true
        
        var requestHeader: String
        var updateVal = textField.text!
        switch textField {
        case emailTextField:
            requestHeader = "email"
            emailWasEdited = true
        case usernameTextField:
            requestHeader = "username"
            challeUser.username = updateVal
        case firstNameTextField:
            requestHeader = "first_name"
            challeUser.firstName = updateVal
            updateVal = updateVal.utf8EncodedString()
        case lastNameTextField:
            requestHeader = "last_name"
            challeUser.lastName = updateVal
            updateVal = updateVal.utf8EncodedString()
        case phoneNumberTextField:
            requestHeader = "phone_number"
        default:
            requestHeader = "unknown"
        }
        
        updateAuth0UserRequest.setValue(updateVal, forHTTPHeaderField: requestHeader)
        updateMongoUserRequest.setValue(updateVal, forHTTPHeaderField: requestHeader)
    }
    
    func textViewDidChange(_ textView: UITextView) {
        textView.textColor = UIColor(named: "ChalleRed") ?? UIColor.red
        saveButton.isEnabled = true
        
        let requestHeader = "bio"
        let updateVal = textView.text!
        
        challeUser.bio = updateVal
        updateAuth0UserRequest.setValue(updateVal.utf8EncodedString(), forHTTPHeaderField: requestHeader)
        updateMongoUserRequest.setValue(updateVal.utf8EncodedString(), forHTTPHeaderField: requestHeader)
    }
    
    // MARK: Picker methods
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView( _ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == privacyPicker {
            return C.userPrivacyOptions.count
        } else {
            return C.userTimezoneOptions.count
        }
    }
    
    func pickerView( _ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == privacyPicker {
            return C.userPrivacyOptions[row]
        } else {
            return C.userTimezoneOptions[row]
        }
    }
    
    func pickerView( _ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == privacyPicker {
            privacyTextField.textColor = UIColor(named: "ChalleRed") ?? UIColor.red
            saveButton.isEnabled = true
            
            let privacyVal = C.userPrivacyOptions[row]
            challeUser.isPrivate = C.userPrivacyOptionsToMongoArgs[privacyVal]!
            
            updateAuth0UserRequest.setValue(privacyVal, forHTTPHeaderField: "is_private")
            updateMongoUserRequest.setValue(privacyVal, forHTTPHeaderField: "is_private")
            
            return privacyTextField.text = privacyVal
        } else {
            timezoneTextField.textColor = UIColor(named: "ChalleRed") ?? UIColor.red
            saveButton.isEnabled = true
            
            let timezoneVal = C.userTimezoneOptions[row]
            
            // calculate tz delta by which to shift utc times
            let now = Date()
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            
            dateFormatter.timeZone = TimeZone(identifier: challeUser.timezone)
            let oldDateStr = dateFormatter.string(from: now)
            let oldDate = dateFormatter.date(from: oldDateStr)! // ensure no rounding errors
            
            dateFormatter.timeZone = TimeZone(identifier: timezoneVal)
            let newDate = dateFormatter.date(from: oldDateStr)! // same string different absolute date
            
            timezoneDeltaMinutes += newDate.minutes(from: oldDate)
            
            challeUser.timezone = timezoneVal
            
            updateAuth0UserRequest.setValue(timezoneVal, forHTTPHeaderField: "timezone")
            updateMongoUserRequest.setValue(timezoneVal, forHTTPHeaderField: "timezone")
            
            return timezoneTextField.text = timezoneVal
        }
    }
    
    // MARK: message display methods
    func displayConfirmationMessage(msg: String, displaySecs: Double = 2.0, disableNav: Bool = false, dismissView: Bool = false, completion: (() -> Void)? = nil) {
        DispatchQueue.main.async {
            if disableNav {
                self.cancelButton.isEnabled = false
                self.saveButton.isEnabled = false
            }
            self.messageTextView.isHidden = false
            self.messageTextView.textColor = UIColor(named: "ChalleGreen") ?? UIColor.green
            self.messageTextView.text = msg
            
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + displaySecs) {
                self.messageTextView.isHidden = true
                self.messageTextView.textColor = UIColor(named: "ChalleBlack") ?? UIColor.darkGray
                self.messageTextView.text = ""
                if disableNav {
                    self.cancelButton.isEnabled = true
                    self.saveButton.isEnabled = true
                }
                if let completion = completion {
                    completion()
                }
                if dismissView {
                    self.dismiss(animated: true, completion: nil)
                }
            }
        }
    }
    
    func displayErrorMessage(msg: String = "An error occurred while updating your profile. Please try saving again and contact support@challe.co if this message persists.", displaySecs: Double = 5.0, disableNav: Bool = false, dismissView: Bool = false, completion: (() -> Void)? = nil) {
        DispatchQueue.main.async {
            if disableNav {
                self.cancelButton.isEnabled = false
                self.saveButton.isEnabled = false
            }
            self.messageTextView.isHidden = false
            self.messageTextView.textColor = UIColor(named: "ChalleRed") ?? UIColor.red
            self.messageTextView.text = msg
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + displaySecs) {
                self.messageTextView.isHidden = true
                self.messageTextView.textColor = UIColor(named: "ChalleBlack") ?? UIColor.darkGray
                self.messageTextView.text = ""
                if disableNav {
                    self.cancelButton.isEnabled = true
                    self.saveButton.isEnabled = true
                }
                if let completion = completion {
                    completion()
                }
                if dismissView {
                    self.dismiss(animated: true, completion: nil)
                }
            }
        }
    }

    
    // MARK: Edit image button action and helpers
    @IBAction func editUserImageButton(_ sender: Any) {
        
        Analytics.logEvent("tap_edit_user_image_button", parameters: nil)
        
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Take Photo", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Choose from Gallery", style: .default, handler: { _ in
            self.openGallery()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func openCamera() {
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera)) {
            Analytics.logEvent("edit_user_image", parameters: ["with": "camera"])
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        } else {
            let alert = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallery() {
        Analytics.logEvent("edit_user_image", parameters: ["with": "gallery"])
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func privacyInfoButtonAction(_ sender: UIButton) {
        Analytics.logEvent("tap_privacy_info_button", parameters: nil)
            
        privacyInfoButton.isSelected = true
        
        let popTip = PopTip()
        let infoStr = "If private, profiles can only be viewed by followers and follow requests must be approved. Created challenges are still discoverable by non-followers if their visibility is set to 'Everyone'."
        popTip.show(text: infoStr, direction: .up, maxWidth: 200, in: view, from: privacyInfoButton.frame, duration: 10)
        popTip.backgroundColor = UIColor(named: "ChalleRed") ?? UIColor.red
        popTip.shouldDismissOnTap = true
        popTip.shouldDismissOnTapOutside = true

        popTip.dismissHandler = { popTip in
            self.privacyInfoButton.isSelected = false
        }
    }
    
    // MARK: Save button action and helpers
    @IBAction func saveButtonAction(_ sender: Any) {
        
        Analytics.logEvent("tap_save_profile_edits_button", parameters: nil)
        
        guard textIsValid() else { return }
        
        if emailWasEdited {
            displayErrorMessage(msg: "Please verify your current email and password to save changes.", displaySecs: 2.0, disableNav: true) {
                forceLogin(presentFrom: self, completion: self.updateUserFlow)
            }
        } else {
            updateUserFlow()
        }
        
    }
    
    func textIsValid() -> Bool {
        let popTip = PopTip()
        popTip.backgroundColor = UIColor(named: "ChalleRed") ?? UIColor.red
        popTip.shouldDismissOnTap = true
        popTip.shouldDismissOnTapOutside = true
        
        if !emailTextField.text!.isAlphanumeric(additionalChars: "@_+\\-.") {
            let infoStr = "Invalid character in email."
            popTip.show(text: infoStr, direction: .up, maxWidth: 200, in: view, from: emailTextField.frame, duration: 5)
            return false
        } else if !usernameTextField.text!.isAlphanumeric(additionalChars: " _+\\-.") {
            let infoStr = "Invalid character in username."
            popTip.show(text: infoStr, direction: .up, maxWidth: 200, in: view, from: usernameTextField.frame, duration: 5)
            return false
        } else if !firstNameTextField.text!.isLetter(additionalChars: " \\-’'") {
            let infoStr = "Invalid character in first name."
            popTip.show(text: infoStr, direction: .up, maxWidth: 200, in: view, from: firstNameTextField.frame, duration: 5)
            return false
        } else if !lastNameTextField.text!.isLetter(additionalChars: " \\-’'") {
            let infoStr = "Invalid character in last name."
            popTip.show(text: infoStr, direction: .up, maxWidth: 200, in: view, from: lastNameTextField.frame, duration: 5)
            return false
        } else if !phoneNumberTextField.text!.isNumeric(additionalChars: "+") {
            let infoStr = "Invalid character in phone number."
            popTip.show(text: infoStr, direction: .up, maxWidth: 200, in: view, from: phoneNumberTextField.frame, duration: 5)
            return false
        } else {
            return true
        }
    }
    
    func updateUserFlow() {
        print("saving...")
        let auth0UserId = self.challeUser.auth0Id
        let mongoUserId = self.challeUser.id
        
        if vSpinner == nil || vSpinner?.superview == nil {
            vSpinner = showSpinner(onView: self.view)
        }
        
        auth0CredentialsManager.credentials { error, credentials in
            DispatchQueue.main.async {
                guard error == nil, let credentials = credentials else {
                    print("Auth0 credentials error: \(String(describing: error))")
                    if self.vSpinner != nil { self.removeSpinner(vSpinner: self.vSpinner!) }
                    logoutFlow(presentFrom: self)
                    return
                }
                
                guard let token = credentials.accessToken else {
                    print("Auth0 token formation error")
                    if self.vSpinner != nil { self.removeSpinner(vSpinner: self.vSpinner!) }
                    self.displayErrorMessage()
                    return
                }
                
                self.getPhotoUploadURLRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
                self.getPhotoUploadURLRequest.setValue(self.challeUser.id, forHTTPHeaderField: "user_id")
                
                self.updateMongoUserRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
                self.updateMongoUserRequest.setValue(mongoUserId, forHTTPHeaderField: "user_id")
                
                self.updateAuth0UserRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
                self.updateAuth0UserRequest.setValue(auth0UserId, forHTTPHeaderField: "auth0_user_id")
                
                if self.timezoneDeltaMinutes != 0 {
                    let tokenDoc: Document = [
                        "timezone": self.challeUser.timezone,
                        "timezone_delta": Int64(exactly: self.timezoneDeltaMinutes/60)!
                        // TODO: handle half-hour-shifted timezones
                    ]
                    stitchClient.callFunction(withName: "updateUserNotificationSettings", withArgs: [mongoUserId, tokenDoc]) {
                        (result: StitchResult<String>) in
                        switch result {
                        case .failure(let error):
                            print("Stitch update user notification settings error: \(error)")
                        case .success(let user_id):
                            print("Successfully updated user notification settings to Stitch: \(user_id)")
                            self.timezoneDeltaMinutes = 0
                        }
                    }
                }
                
                if self.imageWasEdited {
                    print("issuing get photo upload address request...")
                    let getPhotoUploadURLTask = self.formGetPhotoUploadURLTask()
                    getPhotoUploadURLTask.resume()
                } else {
                    print("updating auth0 user data...")
                    let updateAuth0UserTask = self.formUpdateAuth0UserTask()
                    updateAuth0UserTask.resume()
                }
                
            }
        }
    }
    
    func formGetPhotoUploadURLTask() -> URLSessionDataTask {
        let getPhotoUploadURLTask = URLSession.shared.dataTask(with: getPhotoUploadURLRequest) {(data, response, error) in
            DispatchQueue.main.async {
                guard let data = data else {
                    print("get-profile-photo-upload-url error")
                    if self.vSpinner != nil { self.removeSpinner(vSpinner: self.vSpinner!) }
                    self.displayErrorMessage()
                    return
                }
                
                guard let jsonData = try? JSONSerialization.jsonObject(with: data, options: []) else {
                    print("error: get-profile-photo-upload-url data couldn't be deserialized")
                    if self.vSpinner != nil { self.removeSpinner(vSpinner: self.vSpinner!) }
                    self.displayErrorMessage()
                    return
                }
                
                guard let dataDict = jsonData as? [String: Any] else {
                    print("error: deserialized get-profile-photo-upload-url data couldn't be formed into dict")
                    if self.vSpinner != nil { self.removeSpinner(vSpinner: self.vSpinner!) }
                    self.displayErrorMessage()
                    return
                }
                
                guard let photoUploadURLStr = dataDict["upload_url"] as? String else {
                    print("error: bool upload_url not returned by get-profile-photo-upload-url")
                    if self.vSpinner != nil { self.removeSpinner(vSpinner: self.vSpinner!) }
                    self.displayErrorMessage()
                    return
                }
                
                guard let photoURLStr = dataDict["photo_url"] as? String else {
                    print("error: bool photo_url not returned by get-profile-photo-upload-url")
                    if self.vSpinner != nil { self.removeSpinner(vSpinner: self.vSpinner!) }
                    self.displayErrorMessage()
                    return
                }
                
                guard let imgData = self.userImageView.image?.resizeWithWidth(width: 250.0)?.jpegData(compressionQuality: 0.25) else {
                    print("error: photo conversion error")
                    if self.vSpinner != nil { self.removeSpinner(vSpinner: self.vSpinner!) }
                    self.displayErrorMessage()
                    return
                }
                
                print("uploading photo...")
                let photoUploadTask = self.getPhotoUploadTask(photoUploadURLStr: photoUploadURLStr, photoURLStr: photoURLStr, imgData: imgData)
                photoUploadTask.resume()
            }
        }
        
        return getPhotoUploadURLTask
    }
    
    func getPhotoUploadTask(photoUploadURLStr: String, photoURLStr: String, imgData: Data)  -> URLSessionDataTask {
        let photoUploadURL = URL(string: photoUploadURLStr)!
        var photoUploadRequest = URLRequest(url: photoUploadURL, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: 60)
        photoUploadRequest.httpMethod = "PUT"
        
        let photoUploadTask = URLSession.shared.uploadTask(with: photoUploadRequest, from: imgData) {(data, response, error) in
            DispatchQueue.main.async {
                guard let response = response as? HTTPURLResponse else {
                    print("error uploading photo to s3")
                    if self.vSpinner != nil { self.removeSpinner(vSpinner: self.vSpinner!) }
                    self.displayErrorMessage()
                    return
                }
                
                guard response.statusCode == 200 else {
                    print("error uploading photo to s3")
                    if self.vSpinner != nil { self.removeSpinner(vSpinner: self.vSpinner!) }
                    self.displayErrorMessage()
                    return
                }
                
                self.challeUser.imageURL = photoURLStr
                self.updateAuth0UserRequest.setValue(photoURLStr, forHTTPHeaderField: "image_url")
                self.updateMongoUserRequest.setValue(photoURLStr, forHTTPHeaderField: "image_url")
                
                Analytics.logEvent("save_user_image_edit", parameters: nil)
                
                print("updating auth0 user data...")
                let updateAuth0UserTask = self.formUpdateAuth0UserTask()
                updateAuth0UserTask.resume()
            }
        }
        
        return photoUploadTask
    }
    
    func formUpdateAuth0UserTask() -> URLSessionDataTask {
        let updateAuth0UserTask = URLSession.shared.dataTask(with: updateAuth0UserRequest) {(data, response, error) in
            DispatchQueue.main.async {
                guard let data = data else {
                    print("update-auth0-user error")
                    if self.vSpinner != nil { self.removeSpinner(vSpinner: self.vSpinner!) }
                    self.displayErrorMessage()
                    return
                }
                guard let jsonData = try? JSONSerialization.jsonObject(with: data, options: []) else {
                    print("error: update-auth0-user data couldn't be deserialized")
                    if self.vSpinner != nil { self.removeSpinner(vSpinner: self.vSpinner!) }
                    self.displayErrorMessage()
                    return
                }
                
                guard let dataDict = jsonData as? [String: Any] else {
                    print("error: deserialized update-auth0-user data couldn't be formed into dict")
                    if self.vSpinner != nil { self.removeSpinner(vSpinner: self.vSpinner!) }
                    self.displayErrorMessage()
                    return
                }
                
                guard let auth0UpdateComplete = dataDict["update_complete"] as? Bool else {
                    print("error: bool update_complete not returned by update-auth0-user")
                    if self.vSpinner != nil { self.removeSpinner(vSpinner: self.vSpinner!) }
                    self.displayErrorMessage()
                    return
                }
                
                if auth0UpdateComplete {
                    print("updating mongo user data...")
                    let updateMongoUserTask = self.formUpdateMongoUserTask()
                    updateMongoUserTask.resume()
                } else {
                    if self.vSpinner != nil { self.removeSpinner(vSpinner: self.vSpinner!) }
                    if let msg = dataDict["message"] as? String {
                        self.displayErrorMessage(msg: msg)
                    } else {
                        self.displayErrorMessage()
                    }
                }
            }
        }
        
        return updateAuth0UserTask
    }
    
    func formUpdateMongoUserTask() -> URLSessionDataTask {
        let updateMongoUserTask = URLSession.shared.dataTask(with: updateMongoUserRequest) {(data, response, error) in
            DispatchQueue.main.async {
                guard let data = data else {
                    print("update-mongo-user error")
                    if self.vSpinner != nil { self.removeSpinner(vSpinner: self.vSpinner!) }
                    self.displayErrorMessage()
                    return
                }
                
                guard let jsonData = try? JSONSerialization.jsonObject(with: data, options: []) else {
                    print("error: update-mongo-user data couldn't be deserialized")
                    if self.vSpinner != nil { self.removeSpinner(vSpinner: self.vSpinner!) }
                    self.displayErrorMessage()
                    return
                }
                
                guard let dataDict = jsonData as? [String: Any] else {
                    print("error: deserialized update-auth0-user data couldn't be formed into dict")
                    if self.vSpinner != nil { self.removeSpinner(vSpinner: self.vSpinner!) }
                    self.displayErrorMessage()
                    return
                }
                
                guard let mongoUpdateComplete = dataDict["update_complete"] as? Bool else {
                    print("error: bool update_complete not returned by update-mongo-user")
                    if self.vSpinner != nil { self.removeSpinner(vSpinner: self.vSpinner!) }
                    self.displayErrorMessage()
                    return
                }
                
                if mongoUpdateComplete {
                    self.afterSuccessfulSave()
                } else {
                    self.displayErrorMessage()
                }
            }
        }
        
        return updateMongoUserTask
    }
    
    func afterSuccessfulSave() {
        DispatchQueue.main.async {
            Analytics.logEvent("save_profile_edit", parameters: nil)
            
            // reset field colors
            self.emailTextField.textColor = UIColor(named: "ChalleBlack") ?? UIColor.darkGray
            self.usernameTextField.textColor = UIColor(named: "ChalleBlack") ?? UIColor.darkGray
            self.firstNameTextField.textColor = UIColor(named: "ChalleBlack") ?? UIColor.darkGray
            self.lastNameTextField.textColor = UIColor(named: "ChalleBlack") ?? UIColor.darkGray
            self.phoneNumberTextField.textColor = UIColor(named: "ChalleBlack") ?? UIColor.darkGray
            self.bioTextView.textColor = UIColor(named: "ChalleBlack") ?? UIColor.darkGray
            self.privacyTextField.textColor = UIColor(named: "ChalleBlack") ?? UIColor.darkGray
            self.timezoneTextField.textColor = UIColor(named: "ChalleBlack") ?? UIColor.darkGray
            
            // update keychain
            keychain.set(self.challeUser.username, forKey: KeychainKeys.username.rawValue)
            keychain.set(self.challeUser.firstName.utf8EncodedString(), forKey: KeychainKeys.userFirstName.rawValue)
            keychain.set(self.challeUser.lastName.utf8EncodedString(), forKey: KeychainKeys.userLastName.rawValue)
            keychain.set(self.challeUser.imageURL, forKey: KeychainKeys.userImageURL.rawValue)
            
            // own profile changed notif
            // keychain updates must happen first
            NotificationCenter.default.post(
                name: .didUpdateOwnProfile ,
                object: nil,
                userInfo: ["newChalleUser": self.challeUser]
            )
            
            // confirmation message
            if self.vSpinner != nil { self.removeSpinner(vSpinner: self.vSpinner!) }
            
            if self.emailWasEdited {
                // if email changed, log out, direct to verify
                Analytics.logEvent("start_email_changed_flow", parameters: nil)
                logout()
                let presenter = Presentr(presentationType: .fullScreen)
                if let vc = self.storyboard?.instantiateViewController(withIdentifier: "verifyAccountViewController") as? VerifyAccountViewController {
                    DispatchQueue.main.async {
                        vc.titleTextLabel.text = "Email successfully changed!"
                    }
                    self.customPresentViewController(presenter, viewController: vc, animated: false)
                }
            } else {
                self.displayConfirmationMessage(msg: "Updates saved!", disableNav: true, dismissView: true)
            }
        }
        
    }
    
    // MARK: Other button actions
    @IBAction func dismissVC(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func changePasswordButton(_ sender: UIButton) {
        if vSpinner == nil || vSpinner?.superview == nil {
            vSpinner = showSpinner(onView: self.view)
        }
        
        Analytics.logEvent("tap_change_password_button", parameters: nil)
        
        let changePasswordURL = URL(string: "https://0r6psvjhfi.execute-api.us-east-1.amazonaws.com/prod/users/change-user-password")!
        var changePasswordRequest = URLRequest(url: changePasswordURL)
        changePasswordRequest.httpMethod = "PATCH"
        
        auth0CredentialsManager.credentials { error, credentials in
            guard error == nil, let credentials = credentials else {
                print("Auth0 credentials error: \(String(describing: error))")
                DispatchQueue.main.async {
                    logoutFlow(presentFrom: self)
                }
                return
            }
            
            guard let token = credentials.accessToken else {
                print("Auth0 token formation error")
                self.displayErrorMessage()
                return
            }
            
            changePasswordRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            changePasswordRequest.setValue(self.originalUserEmail, forHTTPHeaderField: "user_email")
            
            let auth0Task = URLSession.shared.dataTask(with: changePasswordRequest) {(data, response, error) in
                guard let data = data else {
                    print("change-password error")
                    if self.vSpinner != nil { self.removeSpinner(vSpinner: self.vSpinner!) }
                    return
                }
                
                guard let jsonData = try? JSONSerialization.jsonObject(with: data, options: []) else {
                    print("error: change-password data couldn't be deserialized")
                    if self.vSpinner != nil { self.removeSpinner(vSpinner: self.vSpinner!) }
                    return
                }
                
                guard let dataDict = jsonData as? [String: Any] else {
                    print("error: deserialized change-password data couldn't be formed into dict")
                    if self.vSpinner != nil { self.removeSpinner(vSpinner: self.vSpinner!) }
                    return
                }
                
                guard let emailTriggered = dataDict["email_triggered"] as? Bool else {
                    print("error: bool email_triggered not returned by change-password")
                    if self.vSpinner != nil { self.removeSpinner(vSpinner: self.vSpinner!) }
                    return
                }
                
                if emailTriggered {
                    let msg = "Please check your email for instructions on resetting your password."
                    if self.vSpinner != nil { self.removeSpinner(vSpinner: self.vSpinner!) }
                    self.displayErrorMessage(msg: msg)
                }
                
            }
            print("issuing change password request...")
            auth0Task.resume()
        }
    }
    
    @IBAction func logOutButton(_ sender: Any) {
        Analytics.logEvent("tap_log_out_button", parameters: nil)
        logoutFlow(presentFrom: self)
    }
    
    @IBAction func deleteAccountButton(_ sender: Any) {
        Analytics.logEvent("tap_delete_account_button", parameters: nil)
        let popTip = PopTip()
        let infoStr = "To have your account deleted, please send an email to support@challe.co with the subject 'ACCOUNT DELETION REQUEST'."
        popTip.show(text: infoStr, direction: .up, maxWidth: 200, in: view, from: buttomButtonsView.frame, duration: 10)
        popTip.backgroundColor = UIColor(named: "ChalleRed") ?? UIColor.red
        popTip.shouldDismissOnTap = true
        popTip.shouldDismissOnTapOutside = true
    }
    
}
