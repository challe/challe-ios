//
//  SettingsViewController.swift
//  Challe
//
//  Created by Rob Dearborn on 3/19/19.
//  Copyright © 2019 Challe. All rights reserved.
//

import UIKit


class SettingsViewController: UIViewController {

    // MARK: Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Swipe to dismiss
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(dismissVC(_:)))
        swipeDown.direction = UISwipeGestureRecognizer.Direction.down
        self.view.addGestureRecognizer(swipeDown)
    }
    
    // MARK: Button Actions
    @IBAction func dismissVC(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

}
