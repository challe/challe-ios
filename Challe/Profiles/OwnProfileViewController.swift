//
//  OwnProfileViewController.swift
//  Challe
//
//  Created by Rob Dearborn on 3/18/19.
//  Copyright © 2019 Challe. All rights reserved.
//

import UIKit

import Presentr


class OwnProfileViewController: UIViewController {
    
    // MARK: Properties
    @IBOutlet weak var profileView: UIView!
    @IBOutlet weak var feedButton: UIButton!
    @IBOutlet weak var profileButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let ownUserId = stitchClient.auth.currentUser?.id else {
            print("OwnProfileViewController error: must be authenticated.")
            return
        }
        
        // Initialie user profile view
        if let vc = storyboard?.instantiateViewController(withIdentifier: "userProfileViewController") as? UserProfileViewController {
            vc.challeUserId = ownUserId
                vc.view.translatesAutoresizingMaskIntoConstraints = false
            
            addChild(vc)
            profileView.addSubview(vc.view)
            
            NSLayoutConstraint.activate([
                vc.view.leadingAnchor.constraint(equalTo: profileView.leadingAnchor),
                vc.view.trailingAnchor.constraint(equalTo: profileView.trailingAnchor),
                vc.view.topAnchor.constraint(equalTo: profileView.topAnchor),
                vc.view.bottomAnchor.constraint(equalTo: profileView.bottomAnchor)
                ])
            
            vc.didMove(toParent: self)
        }
    }
    
    @IBAction func feedButtonAction(_ sender: UIButton) {
        self.performSegue(withIdentifier: "ownProfileFeedSegue", sender: self)
    }

}
