//
//  User.swift
//  Challe
//
//  Created by Rob Dearborn on 3/19/19.
//  Copyright © 2019 Challe. All rights reserved.
//

import UIKit

import StitchCore


class ChalleUser: NSObject {
    
    var id: String
    var auth0Id: String
    var username: String
    var imageURL: String
    var firstName: String
    var lastName: String
    var userIsFollower: Bool
    var followRequestPending: Bool
    var isPrivate: Bool
    var bio: String
    var timezone: String
    var nFollowers: Int64
    var nFollowing: Int64
    
    init?(userDoc: Document){
        if let objectId = userDoc["_id"] as? ObjectId {
            self.id = "\(objectId)"
        } else if let id = userDoc["_id"] as? String {
            self.id = id
        } else {
            self.id = "000000000000000000000000"
        }
        
        self.auth0Id = userDoc["auth0_id"] as? String ?? "auth0|000000000000000000000000"
        self.username = userDoc["username"] as? String ?? "username"
        self.imageURL = userDoc["image_url"] as? String ?? "https://s3.amazonaws.com/challe-auth0/challe_logo_light_sq+_150.png"
        let encodedFirstName = userDoc["first_name"] as? String ?? "firstname"
        self.firstName = encodedFirstName.utf8DecodedString()
        let encodedLastName = userDoc["last_name"] as? String ?? "lastname"
        self.lastName = encodedLastName.utf8DecodedString()
        self.userIsFollower = userDoc["user_is_follower"] as? Bool ?? false
        self.followRequestPending = userDoc["follow_request_pending"] as? Bool ?? false
        self.isPrivate = userDoc["is_private"] as? Bool ?? true
        let encodedBio = userDoc["bio"] as? String ?? ""
        self.bio = encodedBio.utf8DecodedString()
        self.timezone = userDoc["timezone"] as? String ?? "America/New_York"
        
        if let nFollowers = userDoc["n_followers"] as? Int {
            self.nFollowers = Int64(exactly: nFollowers)!
        } else if let nFollowers = userDoc["n_followers"] as? Int64 {
            self.nFollowers = nFollowers
        } else {
            self.nFollowers = Int64(exactly: 0)!
        }
        
        if let nFollowing = userDoc["n_following"] as? Int {
            self.nFollowing = Int64(exactly: nFollowing)!
        } else if let nFollowing = userDoc["n_following"] as? Int64 {
            self.nFollowing = nFollowing
        } else {
            self.nFollowing = Int64(exactly: 0)!
        }
        
    }
    
    func getFollowersStr() -> String {
        return toFriendlyStr(int: self.nFollowers)
    }
    func getFollowingStr() -> String {
        return toFriendlyStr(int: self.nFollowing)
    }

}
