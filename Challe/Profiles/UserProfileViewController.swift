//
//  UserProfileViewController.swift
//  Challe
//
//  Created by Rob Dearborn on 3/18/19.
//  Copyright © 2019 Challe. All rights reserved.
//

import UIKit

import Firebase
import Presentr
import StitchCore


class UserProfileViewController: UIViewController {
    
    // MARK: Properties
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var followButton: UIButton!
    @IBOutlet weak var composeButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var moreButton: UIButton!
    @IBOutlet weak var settingsButton: UIButton!
    @IBOutlet weak var usernameTextLabel: UILabel!
    @IBOutlet weak var fullNameTextLabel: UILabel!
    @IBOutlet weak var followersTextButton: UIButton!
    @IBOutlet weak var followingTextButton: UIButton!
    @IBOutlet weak var requestsTextButton: UIButton!
    @IBOutlet weak var bioTextView: UITextView!
    @IBOutlet weak var challengesTextLabel: UILabel!
    @IBOutlet weak var challengesSegmentedControl: UISegmentedControl!
    @IBOutlet weak var challengesView: UIView!
    @IBOutlet weak var privateProfileTextLabel: UILabel!
    @IBOutlet weak var privateProfileTextView: UITextView!
    @IBOutlet weak var privateProfileView: UIView!
    
    
    var challeUserId: String?
    private var challeUser: ChalleUser?
    private var userViewingSelf: Bool!
    private var nFollowRequests: Int64 = 0
    var challengesTableViewController: ChallengesTableViewController!
    private var vSpinner: UIView?
    
    // MARK: Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let challeUserId = challeUserId else {
            print("UserProfileViewController error: challeUserId not set")
            return
        }
        
        guard let ownUserId = stitchClient.auth.currentUser?.id else {
            print("UserProfileViewController error: user not authenticated")
            return
        }
        
        userViewingSelf = (challeUserId == ownUserId)
        
        initialHides()
        
        // Load User Info
        stitchClient.callFunction(withName: "getUser", withArgs: [challeUserId]) { (result: StitchResult<Document>) in
            switch result {
            case .failure(let error):
                print("Stitch user collection error: \(String(describing: error))")
                if C.stitchAuthErrorDescriptions.contains(error.description) {
                    DispatchQueue.main.async {
                        logoutFlow(presentFrom: self)
                    }
                }
            case .success(let userDoc):
                DispatchQueue.main.async {
                    Analytics.logEvent("view_profile", parameters: ["id": self.challeUserId])
                    guard let cu = ChalleUser(userDoc: userDoc) else { return }
                    self.loadUserData(challeUser: cu)
                }
            }
        }
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(onUpdateOwnProfile(_:)),
            name: .didUpdateOwnProfile,
            object: nil
        )
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(onUpdateOwnFollowers(_:)),
            name: .didUpdateOwnFollowers,
            object: nil
        )
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(onUpdateOwnFollowing(_:)),
            name: .didUpdateOwnFollowing,
            object: nil
        )
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(onHandleFollowRequest(_:)),
            name: .didHandleFollowRequest,
            object: nil
        )
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(onChallengeCreation(_:)),
            name: .didCreateChallenge,
            object: nil
        )
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(onChallengeDeletion(_:)),
            name: .didDeleteChallenge,
            object: nil
        )
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(onChallengeUpdate(_:)),
            name: .didUpdateChallenge,
            object: nil
        )
        
        // Populate challenge stats in hidden state
        self.challengesSegmentedControl.setTitleTextAttributes([NSAttributedString.Key.font : UIFont(name: "HelveticaNeue-Thin", size: 13.0)], for: .normal)
        
        loadUserChallengeCounts()
        
        // Initialize challenges tableview
        challengesTableViewController = storyboard!.instantiateViewController(withIdentifier: "challengesTableViewController") as! ChallengesTableViewController
        challengesTableViewController.view.translatesAutoresizingMaskIntoConstraints = false
        
        addChild(challengesTableViewController)
        challengesView.addSubview(challengesTableViewController.view)
        
        NSLayoutConstraint.activate([
            challengesTableViewController.view.leadingAnchor.constraint(equalTo: challengesView.leadingAnchor),
            challengesTableViewController.view.trailingAnchor.constraint(equalTo: challengesView.trailingAnchor),
            challengesTableViewController.view.topAnchor.constraint(equalTo: challengesView.topAnchor),
            challengesTableViewController.view.bottomAnchor.constraint(equalTo: challengesView.bottomAnchor)
            ])
        
        challengesTableViewController.didMove(toParent: self)
        
        loadUserChallenges()
    
    }
    
    // MARK: user data loading
    private func initialHides() {
        // populate top level info
        if userViewingSelf {
            moreButton.isHidden = true
            loadFollowRequestsCount()
        } else {
            composeButton.isHidden = true
            settingsButton.isHidden = true
        }
        requestsTextButton.isHidden = true
        
        // hide all content initially until profile privacy is determined
        // but load in parallel irrespective of visibility
        followButton.isHidden = true
        followersTextButton.isEnabled = false
        followingTextButton.isEnabled = false
        bioTextView.isHidden = true
        challengesTextLabel.isHidden = true
        challengesSegmentedControl.isHidden = true
        challengesView.isHidden = true
        privateProfileTextLabel.isHidden = true
        privateProfileTextView.isHidden = true
        privateProfileView.isHidden = true
        
    }
    
    private func loadUserData(challeUser: ChalleUser, skipImage: Bool=false){
        self.challeUser = challeUser
        
        if !skipImage, let imageURL = URL(string: challeUser.imageURL) {
            userImageView.setImage(from: imageURL)
        }
        
        usernameTextLabel.text = challeUser.username
        fullNameTextLabel.text = "\(challeUser.firstName) \(challeUser.lastName)"
        var followersStr = ""
        if challeUser.nFollowers == 1 {
            followersStr = "1 follower"
        } else {
            followersStr = "\(challeUser.getFollowersStr()) followers"
        }
        followersTextButton.setTitle(followersStr, for: .normal)
        followingTextButton.setTitle("\(challeUser.getFollowingStr()) following", for: .normal)
        followButton.isEnabled = !challeUser.followRequestPending
        followButton.isSelected = challeUser.userIsFollower
        followButton.isHidden = userViewingSelf
        
        if !challeUser.isPrivate || challeUser.userIsFollower {
            followersTextButton.isEnabled = true
            followingTextButton.isEnabled = true
            
            bioTextView.text = challeUser.bio
            bioTextView.isHidden = false
            
            challengesTextLabel.isHidden = false
            challengesSegmentedControl.isHidden = false
            challengesView.isHidden = false
        } else {
            privateProfileView.isHidden = false
            
            if challeUser.followRequestPending {
                privateProfileTextLabel.text = "\(challeUser.firstName)'s profile is private 😎"
                privateProfileTextView.text = "Your request to follow \(challeUser.firstName) is pending approval."
            } else {
                privateProfileTextLabel.text = "\(challeUser.firstName)'s profile is private 😎"
                privateProfileTextView.text = "Follow \(challeUser.firstName) to see their challenges."                
            }
            
            privateProfileTextLabel.isHidden = false
            privateProfileTextView.isHidden = false
        }
        
    }
    
    private func loadUserChallengeCounts() {
        guard let challeUserId = challeUserId else {
            print("UserProfileViewController error: challeUserId not set")
            return
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        dateFormatter.timeZone = TimeZone.init(secondsFromGMT: 0)
        let minStartDate = dateFormatter.date(from: "01/01/2000")!
        let maxStartDate = dateFormatter.date(from: "01/01/3000")!
        
        var argsDoc: Document = [
            "maxCreatedAt": Date(),
            "minStartDate": minStartDate,
            "maxStartDate": maxStartDate,
            "creator": challeUserId
        ]
        stitchClient.callFunction(withName: "getChallengesCount", withArgs: [argsDoc]) { (result: StitchResult<Int64>) in
            switch result {
            case .failure(let error):
                print("Stitch count challenges error: \(String(describing: error))")
                if C.stitchAuthErrorDescriptions.contains(error.description) {
                    DispatchQueue.main.async {
                        logoutFlow(presentFrom: self)
                    }
                }
            case .success(let ct):
                DispatchQueue.main.async {
                    let friendlyCt = toFriendlyStr(int: ct)
                    let titleStr = "Created (\(friendlyCt))"
                    self.challengesSegmentedControl.setTitle(titleStr, forSegmentAt: 0)
                }
            }
        }
        
        argsDoc["creator"] = "any"
        argsDoc["follower"] = challeUserId
        argsDoc["completed"] = false
        stitchClient.callFunction(withName: "getChallengesCount", withArgs: [argsDoc]) { (result: StitchResult<Int64>) in
            switch result {
            case .failure(let error):
                print("Stitch count challenges error: \(String(describing: error))")
                if C.stitchAuthErrorDescriptions.contains(error.description) {
                    DispatchQueue.main.async {
                        logoutFlow(presentFrom: self)
                    }
                }
            case .success(let ct):
                DispatchQueue.main.async {
                    let friendlyCt = toFriendlyStr(int: ct)
                    let titleStr = "Started (\(friendlyCt))"
                    self.challengesSegmentedControl.setTitle(titleStr, forSegmentAt: 1)
                }
            }
        }
        
        argsDoc["completed"] = true
        stitchClient.callFunction(withName: "getChallengesCount", withArgs: [argsDoc]) { (result: StitchResult<Int64>) in
            switch result {
            case .failure(let error):
                print("Stitch count challenges error: \(String(describing: error))")
                if C.stitchAuthErrorDescriptions.contains(error.description) {
                    DispatchQueue.main.async {
                        logoutFlow(presentFrom: self)
                    }
                }
            case .success(let ct):
                DispatchQueue.main.async {
                    let friendlyCt = toFriendlyStr(int: ct)
                    let titleStr = "Done (\(friendlyCt))"
                    self.challengesSegmentedControl.setTitle(titleStr, forSegmentAt: 2)
                }
            }
        }
    }
    
    private func loadUserChallenges() {
        
        guard let challeUserId = challeUserId else {
            print("UserProfileViewController error: challeUserId not set")
            return
        }
        
        DispatchQueue.main.async {
            let selectedIndex = self.challengesSegmentedControl.selectedSegmentIndex
            
            var creatorQueryArg: String
            var followerQueryArg: String?
            var completedQueryArg: Bool?
            
            switch selectedIndex {
            case 0:
                creatorQueryArg = challeUserId
                followerQueryArg = nil
                completedQueryArg = nil
            case 1:
                creatorQueryArg = "any"
                followerQueryArg = challeUserId
                completedQueryArg = false
            case 2:
                creatorQueryArg = "any"
                followerQueryArg = challeUserId
                completedQueryArg = true
            default:
                creatorQueryArg = challeUserId
                followerQueryArg = nil
                completedQueryArg = nil
            }
            
            self.challengesTableViewController.updateFiltsAndReload(
                creatorQueryArg: creatorQueryArg,
                sortByQueryArg: "start_date",
                sortByQueryOrder: -1,
                startingFilt: "All",
                queryString: "",
                followerQueryArg: followerQueryArg,
                completedQueryArg: completedQueryArg
            )
        }
    }
    
    private func loadFollowRequestsCount() {
        
        stitchClient.callFunction(withName: "getFollowRequestsCount", withArgs: []) { (result: StitchResult<Int64>) in
            switch result {
            case .failure(let error):
                print("Stitch user collection error: \(String(describing: error))")
                if C.stitchAuthErrorDescriptions.contains(error.description) {
                    DispatchQueue.main.async {
                        logoutFlow(presentFrom: self)
                    }
                }
            case .success(let ct):
                self.nFollowRequests = ct
                self.setFollowRequestsStr()
            }
        }
        
    }
    
    private func setFollowRequestsStr() {
        DispatchQueue.main.async {
            let friendlyCt = toFriendlyStr(int: self.nFollowRequests)
            var titleStr = ""
            if self.nFollowRequests < 1 {
                self.requestsTextButton.isHidden = true
            } else {
                if self.nFollowRequests == 1 {
                    titleStr = "1 request"
                } else {
                    titleStr = "\(friendlyCt) requests"
                }
                self.requestsTextButton.setTitle(titleStr, for: .normal)
                self.requestsTextButton.isHidden = false
            }
        }
    }
    
    // MARK: listeners
    @objc private func onUpdateOwnProfile(_ notification:Notification) {
        DispatchQueue.main.async {
            guard let userInfo = notification.userInfo as? [String: ChalleUser], let newChalleUser = userInfo["newChalleUser"] else { return }
            
            self.loadUserData(challeUser: newChalleUser)
        }
    }
    
    @objc private func onUpdateOwnFollowers(_ notification:Notification) {
        DispatchQueue.main.async {
            guard let challeUser = self.challeUser else {
                print("UserProfileViewControllerError: couldn't update own followers since challeUser is not set.")
                return
            }
            
            guard let userInfo = notification.userInfo as? [String: Int64], let byAmt = userInfo["byAmt"] else { return }
            
            if self.userViewingSelf {
                self.challeUser!.nFollowers += byAmt
                self.followersTextButton.setTitle("\(self.challeUser!.getFollowersStr()) followers", for: .normal)
            }
        }
    }
    
    @objc private func onUpdateOwnFollowing(_ notification:Notification) {
        DispatchQueue.main.async {
            guard let challeUser = self.challeUser else {
                print("UserProfileViewControllerError: couldn't update own following since challeUser is not set.")
                return
            }
            
            guard let userInfo = notification.userInfo as? [String: Int64], let byAmt = userInfo["byAmt"] else { return }
            
            if self.userViewingSelf {
                self.challeUser!.nFollowing += byAmt
                self.followingTextButton.setTitle("\(self.challeUser!.getFollowingStr()) following", for: .normal)
            }
        }
    }
    
    @objc private func onHandleFollowRequest(_ notification:Notification) {
        DispatchQueue.main.async {
            self.nFollowRequests -= 1
            self.setFollowRequestsStr()
        }
    }
    
    @objc private func onChallengeCreation(_ notification:Notification) {
        if userViewingSelf {
            loadUserChallengeCounts()
            loadUserChallenges()
        }
        // otherwise, no need to worry about moving challenges between segmented controls
        // and updates can be handled by challenges tables
    }
    
    @objc private func onChallengeDeletion(_ notification:Notification) {
        if userViewingSelf {
            loadUserChallengeCounts()
        }
        // otherwise, no need to worry about moving challenges between segmented controls
        // and updates can be handled by challenges tables
        // in this case challenges tables handle removal of deleted cell without querying mongo
        // so only updating counts is necessary
    }
    
    @objc private func onChallengeUpdate(_ notification:Notification) {
        if userViewingSelf {
            loadUserChallengeCounts()
            loadUserChallenges()
        }
        // otherwise, no need to worry about moving challenges between segmented controls
        // and updates can be handled by challenges tables
    }
    
    // MARK: button actions
    @IBAction func followButtonAction(_ sender: UIButton) {
        guard let challeUserId = challeUserId else {
            print("UserProfileViewController error: challeUserId not set")
            return
        }
        
        guard let challeUser = challeUser else {
            print("UserProfileViewController error: challeUser not set")
            return
        }
        
        if challeUser.userIsFollower {
            let alertController = UIAlertController(
                title: "Unfollow?",
                message: nil,
                preferredStyle: .alert
            )
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
                print("Follow deletion canceled.")
            }
            alertController.addAction(cancelAction)
            
            let destroyAction = UIAlertAction(title: "Unfollow", style: .destructive) { (action) in
                
                stitchClient.callFunction(withName: "deleteFollow", withArgs: [challeUserId, stitchClient.auth.currentUser!.id]) { (result: StitchResult<String>) in
                    switch result {
                    case .failure(let error):
                        print("Stitch follow deletion error: \(String(describing: error))")
                        if C.stitchAuthErrorDescriptions.contains(error.description) {
                            DispatchQueue.main.async {
                                logoutFlow(presentFrom: self)
                            }
                        }
                    case .success(_):
                        DispatchQueue.main.async {
                            Analytics.logEvent("profile_unfollow", parameters: ["id": self.challeUserId])
                            challeUser.userIsFollower = false
                            challeUser.nFollowers -= 1
                            self.initialHides()
                            self.loadUserData(challeUser: challeUser, skipImage: true)
                            NotificationCenter.default.post(
                                name: .didUpdateUser,
                                object: nil,
                                userInfo: ["challeUser": challeUser]
                            )
                            NotificationCenter.default.post(
                                name: .didUpdateOwnFollowing ,
                                object: nil,
                                userInfo: ["byAmt": -1 as Int64]
                            )
                        }
                    }
                }
            }
            alertController.addAction(destroyAction)
            
            self.present(alertController, animated: true) {
                // ...
            }
        } else {
            if vSpinner == nil || vSpinner?.superview == nil {
                vSpinner = showSpinner(onView: self.view)
            }
            
            let newFollow: Document = [
                "creator_id": stitchClient.auth.currentUser!.id,
                "created_at": Date(),
                "is_deleted": false,
                "following_id": challeUserId,
                "is_approved": !challeUser.isPrivate
            ]
            stitchClient.callFunction(withName: "createFollow", withArgs: [newFollow]) { (result: StitchResult<String>) in
                switch result {
                case .failure(let error):
                    print("Stitch follow creation error: \(String(describing: error))")
                    if self.vSpinner != nil { self.removeSpinner(vSpinner: self.vSpinner!) }
                    if C.stitchAuthErrorDescriptions.contains(error.description) {
                        DispatchQueue.main.async {
                            logoutFlow(presentFrom: self)
                        }
                    }
                case .success(let followId):
                    print("Stitch created follow with ID: \(followId)")
                    DispatchQueue.main.async {
                        if self.vSpinner != nil { self.removeSpinner(vSpinner: self.vSpinner!) }
                        
                        if challeUser.isPrivate {
                            challeUser.followRequestPending = true
                        } else {
                            challeUser.userIsFollower = true
                            challeUser.nFollowers += 1
                            NotificationCenter.default.post(
                                name: .didUpdateOwnFollowing ,
                                object: nil,
                                userInfo: ["byAmt": 1 as Int64]
                            )
                        }
                        
                        Analytics.logEvent("profile_follow_request", parameters: ["id": self.challeUserId, "is_private": challeUser.isPrivate])
                        
                        self.loadUserData(challeUser: challeUser, skipImage: true)
                        NotificationCenter.default.post(
                            name: .didUpdateUser,
                            object: nil,
                            userInfo: ["challeUser": challeUser]
                        )
                    }
                }
            }
        }
    }
    
    @IBAction func composeButtonAction(_ sender: UIButton) {
        let presenter = Presentr(presentationType: .fullScreen)
        
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "createChallengeViewController") as? CreateChallengeViewController {
            customPresentViewController(presenter, viewController: vc, animated: true)
        }
    }
    
    @IBAction func shareButtonAction(_ sender: UIButton) {
        guard let challeUserId = challeUserId else {
            print("error: UserProfileViewController created w/o setting challeUserId")
            return
        }
        
        Analytics.logEvent("share_profile", parameters: ["id": self.challeUserId])
        
        var text = "https://challe.co/profile/\(challeUserId)"
        
        // set up activity view controller
        let textToShare = [text]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    @IBAction func moreButtonAction(_ sender: UIButton) {
        moreButton.isSelected = true
        let alertController = UIAlertController(
            title: nil,
            message: nil,
            preferredStyle: .actionSheet
        )
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            print("Profile flagging canceled.")
            self.moreButton.isSelected = false
        }
        alertController.addAction(cancelAction)
        
        let destroyAction = setDestroyAction()
        alertController.addAction(destroyAction)
        
        self.present(alertController, animated: true) {
            // ...
        }
    }
    
    private func setDestroyAction() -> UIAlertAction {
        
        var destroyAction: UIAlertAction
        destroyAction = UIAlertAction(title: "Report as Inappropriate", style: .destructive) { (action) in
            let alertController = UIAlertController(
                title: "Report this profile?",
                message: "Reported profiles may be removed if inappropriate.",
                preferredStyle: .alert
            )
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
                print("Profile flagging canceled.")
                self.moreButton.isSelected = false
            }
            alertController.addAction(cancelAction)
            
            let destroyAction = UIAlertAction(title: "Report", style: .destructive) { (action) in
                let flagDoc: Document = [
                    "creator_id": stitchClient.auth.currentUser!.id,
                    "created_at": Date(),
                    "target_type": "user",
                    "target_id": ObjectId(self.challeUserId!)!
                ]
                print(flagDoc)
                stitchClient.callFunction(withName: "createFlag", withArgs: [flagDoc]) { (result: StitchResult<String>) in
                    switch result {
                    case .failure(let error):
                        print("Stitch flag creation error: \(String(describing: error))")
                        if C.stitchAuthErrorDescriptions.contains(error.description) {
                            DispatchQueue.main.async {
                                logoutFlow(presentFrom: self)
                            }
                        }
                    case .success(let successMessage):
                        Analytics.logEvent("flag_profile", parameters: ["id": self.challeUserId])
                        print(successMessage)
                    }
                }
                self.moreButton.isSelected = false
            }
            alertController.addAction(destroyAction)
            
            self.present(alertController, animated: true) {
                // ...
            }
        }
        
        return destroyAction
    }
    
    @IBAction func settingsButtonAction(_ sender: UIButton) {
        let presenter = Presentr(presentationType: .fullScreen)
        
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "settingsViewController") as? SettingsViewController {
            self.customPresentViewController(presenter, viewController: vc, animated: true)
        }
    }
    
    @IBAction func followersTextButtonAction(_ sender: UIButton) {
        
        let presenter = Presentr(presentationType: .fullScreen)
        
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "usersFeedViewController") as? UsersFeedViewController {
            Analytics.logEvent("view_profile_followers", parameters: ["id": self.challeUserId])
            vc.queryType = .userFollowers
            vc.queryString = challeUserId
            self.customPresentViewController(presenter, viewController: vc, animated: true)
        }
    }
    
    @IBAction func followingTextButtonAction(_ sender: UIButton) {
        
        let presenter = Presentr(presentationType: .fullScreen)
        
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "usersFeedViewController") as? UsersFeedViewController {
            Analytics.logEvent("view_profile_following", parameters: ["id": self.challeUserId])
            vc.queryType = .userFollowing
            vc.queryString = challeUserId
            self.customPresentViewController(presenter, viewController: vc, animated: true)
        }
    }
    
    @IBAction func requestsTextButtonAction(_ sender: UIButton) {
        
        let presenter = Presentr(presentationType: .fullScreen)
        
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "usersFeedViewController") as? UsersFeedViewController {
            Analytics.logEvent("view_profile_follower_requests", parameters: ["id": self.challeUserId])
            vc.queryType = .userFollowRequests
            vc.queryString = challeUserId
            self.customPresentViewController(presenter, viewController: vc, animated: true)
        }
    }
    
    
    @IBAction func challengesSegmentedControlAction(_ sender: UISegmentedControl) {
        loadUserChallenges()
        
        let selectedIndex = self.challengesSegmentedControl.selectedSegmentIndex
        let feedType = self.challengesSegmentedControl.titleForSegment(at: selectedIndex)
        Analytics.logEvent("toggle_profile_feed", parameters: ["type": feedType])
    }
}
