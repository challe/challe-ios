//
//  UsersTableViewCell.swift
//  Challe
//
//  Created by Rob Dearborn on 3/25/19.
//  Copyright © 2019 Challe. All rights reserved.
//

import UIKit


protocol UsersTableViewCellDelegator {
    func viewUserProfile(challeUserId: String)
    func handleFollowRequest(requestingUserId: String, approve: Bool, requestingUserIdx: Int)
}


class UsersTableViewCell: UITableViewCell {
    
    // MARK: Properties
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var usernameTextButton: UIButton!
    @IBOutlet weak var fullNameTextButton: UIButton!
    @IBOutlet weak var approveButton: UIButton!
    @IBOutlet weak var rejectButton: UIButton!
    @IBOutlet weak var followButton: UIButton!
    
    var delegate: UsersTableViewController!
    var challeUser: ChalleUser!
    var challeUserIdx: Int?

    // MARK: Overrides
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: Button actions
    @IBAction func usernameTextButtonAction(_ sender: Any) {
        self.delegate.viewUserProfile(challeUserId: challeUser.id)
    }
    
    @IBAction func approveButtonAction(_ sender: Any) {
        guard let challeUserIdx = challeUserIdx else {
            print("UsersTableViewCell error: cannot approve follow request; challeUserIdx not set.")
            return
        }
            
        self.delegate.handleFollowRequest(requestingUserId: challeUser.id, approve: true, requestingUserIdx: challeUserIdx)
    }
    
    @IBAction func rejectButtonAction(_ sender: Any) {
        guard let challeUserIdx = challeUserIdx else {
            print("UsersTableViewCell error: cannot approve follow request; challeUserIdx not set.")
            return
        }
        self.delegate.handleFollowRequest(requestingUserId: challeUser.id, approve: false, requestingUserIdx: challeUserIdx)
    }
    
    @IBAction func fullNameTextButtonAction(_ sender: Any) {
        self.delegate.viewUserProfile(challeUserId: challeUser.id)
    }
    
}
