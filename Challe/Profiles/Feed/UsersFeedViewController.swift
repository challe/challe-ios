//
//  UsersFeedViewController.swift
//  Challe
//
//  Created by Rob Dearborn on 3/25/19.
//  Copyright © 2019 Challe. All rights reserved.
//

import UIKit


class UsersFeedViewController: UIViewController {

    // MARK: Properties
    @IBOutlet weak var profileView: UIView!
    
    var queryType: UsersQueryTypes?
    var queryString: String?
    
    // MARK: Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Initialize user profile view
        if let usersTableViewController = storyboard?.instantiateViewController(withIdentifier: "usersTableViewController") as? UsersTableViewController {
            usersTableViewController.view.translatesAutoresizingMaskIntoConstraints = false
            
            guard let queryType = queryType else {
                print("UsersFeedViewController error: no querytype set")
                return
            }
            
            guard let queryString = queryString else {
                print("UsersFeedViewController error: no connectionId set")
                return
            }
            
            usersTableViewController.updateRequestAndReload(queryType: queryType, queryString: queryString)
            
            addChild(usersTableViewController)
            profileView.addSubview(usersTableViewController.view)
            
            NSLayoutConstraint.activate([
                usersTableViewController.view.leadingAnchor.constraint(equalTo: profileView.leadingAnchor),
                usersTableViewController.view.trailingAnchor.constraint(equalTo: profileView.trailingAnchor),
                usersTableViewController.view.topAnchor.constraint(equalTo: profileView.topAnchor),
                usersTableViewController.view.bottomAnchor.constraint(equalTo: profileView.bottomAnchor)
                ])
            
            usersTableViewController.didMove(toParent: self)
        }
        
        // swipe to dismiss
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(dismissVC(_:)))
        swipeDown.direction = UISwipeGestureRecognizer.Direction.down
        self.view.addGestureRecognizer(swipeDown)
    }
    
    // MARK: Actions
    @IBAction func dismissVC(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

}
