//
//  UsersTableViewController.swift
//  Challe
//
//  Created by Rob Dearborn on 3/25/19.
//  Copyright © 2019 Challe. All rights reserved.
//

import UIKit

import Firebase
import Presentr
import StitchCore


enum UsersQueryTypes {
    
    case userFollowers
    case userFollowing
    case userFollowRequests
    case challengeFollowers
    case search
    
}


class UsersTableViewController: UITableViewController, UsersTableViewCellDelegator {
    
    // MARK: Properties
    var challeUsers = [ChalleUser]()
    
    private var queryType: UsersQueryTypes?
    private var queryString: String?
    private let tableQueue = DispatchQueue(label: "tableQueue")
    private var pauseLoading = true // flipped after initial load
    private var maxCreatedAt = Date()
    private var usersPerPage = C.usersPerPage
    private var onPage = 1
    private var vSpinner: UIView?
    
    // MARK: Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(onUserUpdate(_:)),
            name: .didUpdateUser,
            object: nil
        )
    }
    
    // initialize # of rows
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return challeUsers.count
    }
    
    // form cells
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "UsersTableViewCell"
        
        // create cell
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? UsersTableViewCell else {
            fatalError("The dequeued cell is not an instance of UsersTableViewCell.")
        }
        cell.delegate = self
        cell.selectionStyle = .none
        
        // load cell data
        let challeUser = challeUsers[indexPath.row]
        cell.challeUser = challeUser
        cell.challeUserIdx = indexPath.row
        
        cell.usernameTextButton.setTitle(challeUser.username, for: .normal)
        cell.fullNameTextButton.setTitle("\(challeUser.firstName) \(challeUser.lastName)", for: .normal)
        if let url = URL(string: challeUser.imageURL) {
            cell.userImageView.setImage(from: url)
        }
        
        switch queryType! {
        case .userFollowRequests:
            cell.followButton.isHidden = true
        default:
            cell.approveButton.isHidden = true
            cell.rejectButton.isHidden = true
            
            cell.followButton.isHidden = !(challeUser.userIsFollower || challeUser.followRequestPending)
            cell.followButton.isSelected = challeUser.userIsFollower
            cell.followButton.isEnabled = !challeUser.followRequestPending
        }
        
        return cell
    }
    
    // reload and pagination
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let frameHeight = scrollView.frame.size.height
        let contentYoffset = scrollView.contentOffset.y
        let rowsHeight = scrollView.contentSize.height
        let distanceFromBottom = rowsHeight - contentYoffset
        
        // Reload when pulled down
        if !pauseLoading && contentYoffset < -50 {
            pauseLoading = true
            loadUsers(reload: true)
        }
        
        // Load more when bottom of table reached
        if !pauseLoading && (rowsHeight > frameHeight) && (distanceFromBottom < frameHeight) {
            pauseLoading = true
            loadUsers()
        }
    }
    
    // Selection
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewUserProfile(challeUserId: challeUsers[indexPath.row].id)
    }
    
    func updateRequestAndReload(queryType: UsersQueryTypes, queryString: String) {
        
        self.queryType = queryType
        self.queryString = queryString
        
        loadUsers(reload: true)
    }
    
    // MARK: Data loading
    private func loadUsers(reload: Bool = false) {
        if vSpinner == nil || vSpinner?.superview == nil {
            vSpinner = showSpinner(onView: self.view)
        }
        
        guard let queryType = self.queryType else {
            print("UsersTableViewController error: querytype not set")
            if self.vSpinner != nil { self.removeSpinner(vSpinner: self.vSpinner!) }
            return
        }
        
        guard let queryString = self.queryString else {
            print("UsersTableViewController error: queryStr not set")
            if self.vSpinner != nil { self.removeSpinner(vSpinner: self.vSpinner!) }
            return
        }
        
        if reload {
            self.onPage = 1
            self.maxCreatedAt = Date()
        }
        
        var getUsersURL: URL
        
        if queryType == .search {
            getUsersURL = URL(string: "https://0r6psvjhfi.execute-api.us-east-1.amazonaws.com/prod/users/get-users")!
        } else {
            getUsersURL = URL(string: "https://0r6psvjhfi.execute-api.us-east-1.amazonaws.com/prod/users/get-connected-users")!
        }
        var getUsersRequest = URLRequest(url: getUsersURL)
        getUsersRequest.httpMethod = "GET"
        
        auth0CredentialsManager.credentials { error, credentials in
            guard error == nil, let credentials = credentials else {
                print("Auth0 credentials error: \(String(describing: error))")
                if self.vSpinner != nil { self.removeSpinner(vSpinner: self.vSpinner!) }
                DispatchQueue.main.async {
                    logoutFlow(presentFrom: self)
                }
                return
            }
            
            guard let token = credentials.accessToken else {
                print("Auth0 token formation error")
                if self.vSpinner != nil { self.removeSpinner(vSpinner: self.vSpinner!) }
                return
            }
            
            getUsersRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            
            let currentUserId = stitchClient.auth.currentUser!.id
            getUsersRequest.setValue(currentUserId, forHTTPHeaderField: "current_user_id")
            
            getUsersRequest.setValue("\(self.usersPerPage)", forHTTPHeaderField: "n_per_page")
            getUsersRequest.setValue("\(self.onPage)", forHTTPHeaderField: "page_number")
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            dateFormatter.timeZone = TimeZone.init(secondsFromGMT: 0)
            getUsersRequest.setValue(dateFormatter.string(from: self.maxCreatedAt), forHTTPHeaderField: "max_created_at")
            
            getUsersRequest.setValue(queryString, forHTTPHeaderField: "query_string")
            
            if queryType != .search {
                var connectionCollection, connectionField, userField, connectionApproval: String!
                
                switch queryType {
                case .userFollowers:
                    connectionCollection = "follows"
                    connectionField = "following_id"
                    connectionApproval = "true"
                    userField = "creator_id"
                case .userFollowing:
                    connectionCollection = "follows"
                    connectionField = "creator_id"
                    connectionApproval = "true"
                    userField = "following_id"
                case .userFollowRequests:
                    connectionCollection = "follows"
                    connectionField = "following_id"
                    connectionApproval = "false"
                    userField = "creator_id"
                case .challengeFollowers:
                    connectionCollection = "user_challenges"
                    connectionField = "challenge_id"
                    connectionApproval = ""
                    userField = "creator_id"
                case .search:
                    connectionCollection = ""
                    connectionField = ""
                    connectionApproval = ""
                    userField = ""
                }
                
                getUsersRequest.setValue(connectionCollection, forHTTPHeaderField: "connection_collection")
                getUsersRequest.setValue(connectionField, forHTTPHeaderField: "connection_field")
                getUsersRequest.setValue(connectionApproval, forHTTPHeaderField: "connection_approval")
                getUsersRequest.setValue(userField, forHTTPHeaderField: "user_field")
            } else {
                if queryString.count < 1 {
                    if self.vSpinner != nil { self.removeSpinner(vSpinner: self.vSpinner!) }
                    return
                }
            }
                
            let getUsersTask = URLSession.shared.dataTask(with: getUsersRequest) {(data, response, error) in
                guard let data = data else {
                    print("get-connected-users / get-users error")
                    if self.vSpinner != nil { self.removeSpinner(vSpinner: self.vSpinner!) }
                    return
                }
                
                guard let userBSONs = try? Document(fromJSON: data) else {
                    print("error: get-connected-users / get-users data couldn't be deserialized")
                    if self.vSpinner != nil { self.removeSpinner(vSpinner: self.vSpinner!) }
                    return
                }
                
                DispatchQueue.main.async {
                    let serialQueue = DispatchQueue(label: "serialQueue")
                    serialQueue.sync {
                        if reload {
                            self.challeUsers.removeAll()
                        }
                        for userBSON in userBSONs {
                            guard let userDoc = try? Document(fromJSON: "\(userBSON.value)") else {
                                print("error: get-connected-users / get-users data couldn't be formed into document")
                                continue
                            }
                            guard let challeUser = ChalleUser(userDoc: userDoc) else {
                                ("error: get-connected-users / get-users document couldn't be formed into ChalleUser")
                                continue
                            }
                            self.challeUsers.append(challeUser)
                        }
                        self.tableView.reloadData()
                        if self.challeUsers.count == 0 {
                            self.tableView.setEmptyView(title: "No users to show 😢", message: "(yet!)")
                        }
                        else {
                            self.tableView.restore()
                        }
                        Analytics.logEvent("load_users", parameters: ["reload": reload, "on_page": self.onPage])
                        self.onPage += 1
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2.0) {
                            self.pauseLoading = false
                        }
                        if self.vSpinner != nil { self.removeSpinner(vSpinner: self.vSpinner!) }
                    }
                }

            }
            getUsersTask.resume()
        }
        
    }
    
    func viewUserProfile(challeUserId: String) {
        if !pauseLoading {
            let presenter = Presentr(presentationType: .fullScreen)
            
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "otherProfileViewController") as? OtherProfileViewController {
                vc.challeUserId = challeUserId
                customPresentViewController(presenter, viewController: vc, animated: true)
            }
        }
    }
    
    func handleFollowRequest(requestingUserId: String, approve: Bool, requestingUserIdx: Int) {
        
        guard let thisUserId = stitchClient.auth.currentUser?.id else {
            print("UsersTableViewController error: must be authenticated to handle follow request.")
            return
        }
        
        if vSpinner == nil || vSpinner?.superview == nil {
            vSpinner = showSpinner(onView: self.view)
        }
        
        stitchClient.callFunction(withName: "handleFollowRequest", withArgs: [thisUserId, requestingUserId, approve]) { (result: StitchResult<String>) in
            switch result {
            case .failure(let error):
                print("Stitch handle follow request error: \(String(describing: error))")
                if self.vSpinner != nil { self.removeSpinner(vSpinner: self.vSpinner!) }
                if C.stitchAuthErrorDescriptions.contains(error.description) {
                    DispatchQueue.main.async {
                        logoutFlow(presentFrom: self)
                    }
                }
            case .success(_):
                DispatchQueue.main.async {
                    self.challeUsers.remove(at: requestingUserIdx)
                    let indexPath = IndexPath(row: requestingUserIdx, section: 0)
                    self.tableView.deleteRows(at: [indexPath], with: .fade)
                    
                    NotificationCenter.default.post(
                        name: .didHandleFollowRequest ,
                        object: nil,
                        userInfo: nil
                    )
                    
                    if approve {
                        NotificationCenter.default.post(
                            name: .didUpdateOwnFollowers ,
                            object: nil,
                            userInfo: ["byAmt": 1 as Int64]
                        )
                    }
                    Analytics.logEvent("handle_profile_follow_request", parameters: ["approve": approve])
                    if self.vSpinner != nil { self.removeSpinner(vSpinner: self.vSpinner!) }
                }
            }
        }
    }
    
    // MARK: Listeners
    @objc private func onUserUpdate(_ notification:Notification) {
        DispatchQueue.main.async {
            guard let userInfo = notification.userInfo as? [String: ChalleUser],
                  let newChalleUser = userInfo["challeUser"]
            else {
                    return
            }
            
            let idToEdit = newChalleUser.id
            
            for (idx, challeUser) in self.challeUsers.enumerated() {
                if challeUser.id == idToEdit {
                    self.challeUsers[idx] = newChalleUser
                    let indexPath = IndexPath(row: idx, section: 0)
                    self.tableView.reloadRows(at: [indexPath], with: UITableView.RowAnimation.none)
                }
            }
        }
    }

}
