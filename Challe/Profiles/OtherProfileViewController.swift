//
//  OtherProfileViewController.swift
//  Challe
//
//  Created by Rob Dearborn on 3/18/19.
//  Copyright © 2019 Challe. All rights reserved.
//

import UIKit

class OtherProfileViewController: UIViewController {
    
    // MARK: Properties
    @IBOutlet weak var profileView: UIView!
    
    var challeUserId: String?
    
    // MARK: Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let challeUserId = challeUserId else {
            print("OtherProfileViewController error: challeUserId not set.")
            return
        }
        
        // Initialize user profile view
        if let vc = storyboard?.instantiateViewController(withIdentifier: "userProfileViewController") as? UserProfileViewController {
            vc.challeUserId = challeUserId
            vc.view.translatesAutoresizingMaskIntoConstraints = false
            
            addChild(vc)
            profileView.addSubview(vc.view)
            
            NSLayoutConstraint.activate([
                vc.view.leadingAnchor.constraint(equalTo: profileView.leadingAnchor),
                vc.view.trailingAnchor.constraint(equalTo: profileView.trailingAnchor),
                vc.view.topAnchor.constraint(equalTo: profileView.topAnchor),
                vc.view.bottomAnchor.constraint(equalTo: profileView.bottomAnchor)
                ])
            
            vc.didMove(toParent: self)
        }
        
        // Swipe to dismiss
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(dismissVC(_:)))
        swipeDown.direction = UISwipeGestureRecognizer.Direction.down
        self.view.addGestureRecognizer(swipeDown)
    }
    
    // MARK: Actions
    @IBAction func dismissVC(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

}
